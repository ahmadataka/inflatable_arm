#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Int32.h>
#include <iostream>
using namespace std;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
geometry_msgs::PoseArray obs_pose;
unsigned int obs_length;
unsigned int obs_index, frame_index;
geometry_msgs::Vector3 sens_to_obs;
geometry_msgs::Vector3 sens_pose;
geometry_msgs::Vector3 obs_sent_cand;
std_msgs::Int32 parts;
int segment_num, sensor_num;

void callback(const PointCloud::ConstPtr& msg)
{
  obs_length = (msg->width)*(msg->height);
  obs_pose.poses.clear();
  BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
  {
    geometry_msgs::Pose obs;
    obs.position.x = pt.x;
    obs.position.y = pt.y;
    obs.position.z = pt.z;
    obs_pose.poses.push_back(obs);
  }

}

float magni(geometry_msgs::Pose obs_, geometry_msgs::Vector3 rob_)
{
  return pow(pow(obs_.position.x - rob_.x,2)+pow(obs_.position.y - rob_.y,2)+pow(obs_.position.z - rob_.z,2),0.5);
}

float dist_calculate(geometry_msgs::Vector3 vect)
{
  return pow(pow(vect.x,2)+pow(vect.y,2)+pow(vect.z,2),0.5);
}

geometry_msgs::Vector3 closest_obs(geometry_msgs::Vector3 active_pose)
{
  geometry_msgs::Vector3 obs_return;
  float minim = 100;
  float avg_x = 0;
  float avg_y = 0;
  float avg_z = 0;
  for(unsigned int i=0; i<obs_length; i++)
  {
    avg_x = avg_x + obs_pose.poses[i].position.x;
    avg_y = avg_y + obs_pose.poses[i].position.y;
    avg_z = avg_z + obs_pose.poses[i].position.z;
    if(magni(obs_pose.poses[i],active_pose)<minim)
    {

      minim = magni(obs_pose.poses[i],active_pose);
      obs_index = i;

    }
    // cout << "dist:" << magni(obs_pose.poses[i],robot_pose) << endl;
    // cout << "obs" << i << ": " << obs_pose.poses[i].position << endl;
    // cout << "rob:" << robot_pose << endl;
  }

  // cout << "obs:" << obs_index << endl;
  // cout << "obs_length:" << obs_length << endl;
  // cout << robot_pose << endl;
  // cout << "dist_close:" << magni(obs_pose.poses[obs_index],robot_pose) << endl;
  obs_return.x = obs_pose.poses[obs_index].position.x - active_pose.x;
  obs_return.y = obs_pose.poses[obs_index].position.y - active_pose.y;
  obs_return.z = obs_pose.poses[obs_index].position.z - active_pose.z;

  return obs_return;
}

float find_magnitude(geometry_msgs::Vector3 vect)
{
  return pow(pow(vect.x,2)+pow(vect.y,2)+pow(vect.z,2),0.5);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "sub_sens");
  ros::NodeHandle nh;

  nh.param("segment_num", segment_num, segment_num);
  nh.param("sensor_num", sensor_num, sensor_num);

  char flag[segment_num*sensor_num];
  geometry_msgs::Vector3 sensor_pose[segment_num*sensor_num];
  geometry_msgs::Vector3 obs_sensor_sent[segment_num*sensor_num];

  for(unsigned char i=0; i<(segment_num*sensor_num-1); i++)
  {
    flag[i] = 0;
  }
  // nh.param("robot", turtle_name, turtle_name);

  ros::Subscriber sub = nh.subscribe<PointCloud>("obstacle_point", 10, callback);

  ros::Publisher pub_sens = nh.advertise<geometry_msgs::Vector3> ("agent/sensor_to_obs", 10);
  ros::Publisher pub_parts = nh.advertise<std_msgs::Int32> ("agent/sensor_index", 10);
  ros::Publisher pub_sens_pose = nh.advertise<geometry_msgs::Vector3> ("/agent/body/current", 10);
  tf::TransformListener listener[segment_num*sensor_num];
  // ros::Rate loop_rate(40);
  while (nh.ok())
  {
    tf::StampedTransform transform[segment_num*sensor_num];
    for(unsigned char i=0; i<(segment_num*sensor_num-1); i++)
    {
      stringstream ss;
      ss << int(i);
      string tip_name = ss.str();
        try
        {
          listener[i].lookupTransform("/world", "tip"+tip_name,
                                   ros::Time(0), transform[i]);
          flag[i] = 1;
        }
        catch (tf::TransformException ex)
        {
          ROS_ERROR("%s",ex.what());
          ros::Duration(1.0).sleep();
        }
    }

    char flag_mult=1;
    for(unsigned char i=0; i<(segment_num*sensor_num-1); i++)
    {
      flag_mult = flag_mult*flag[i];
    }

      float minim = 100;
      float distance;

      if(flag_mult == 1 )
      {
        for(unsigned char i=0; i<(segment_num*sensor_num-1); i++)
        {
          sensor_pose[i].x = transform[i].getOrigin().x();
          sensor_pose[i].y = transform[i].getOrigin().y();
          sensor_pose[i].z = transform[i].getOrigin().z();
          obs_sensor_sent[i] = closest_obs(sensor_pose[i]);
          distance = find_magnitude(obs_sensor_sent[i]);
          if(distance<minim)
          {
            minim = distance;
            frame_index = i;
          }
        }
        sens_to_obs.x = obs_sensor_sent[frame_index].x;
        sens_to_obs.y = obs_sensor_sent[frame_index].y;
        sens_to_obs.z = obs_sensor_sent[frame_index].z;
        parts.data = frame_index;
        sens_pose.x = sensor_pose[frame_index].x;
        sens_pose.y = sensor_pose[frame_index].y;
        sens_pose.z = sensor_pose[frame_index].z;
        }

        // cout << parts << endl;
        // closest_part();
        // cout << "tesing" << endl;
      pub_parts.publish(parts);
      pub_sens.publish(sens_to_obs);
      pub_sens_pose.publish(sens_pose);
    ros::spinOnce();
    // loop_rate.sleep ();
  }

}
