#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <iostream>
#include <math.h>
#include <armadillo>
using namespace std;
using namespace arma;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
PointCloud::Ptr msg (new PointCloud);

mat AngletoR(mat ang, mat pos)
{
  mat T_arma;
  T_arma << cos(ang[0,0])*cos(ang[0,1]) << cos(ang[0,0])*sin(ang[0,1])*sin(ang[0,2])-sin(ang[0,0])*cos(ang[0,2]) << cos(ang[0,0])*sin(ang[0,1])*cos(ang[0,2])+sin(ang[0,0])*sin(ang[0,2]) << pos(0,0) << endr
	 << sin(ang[0,0])*cos(ang[0,1]) << sin(ang[0,0])*sin(ang[0,1])*sin(ang[0,2])+cos(ang[0,0])*cos(ang[0,2]) << sin(ang[0,0])*sin(ang[0,1])*cos(ang[0,2])-cos(ang[0,0])*sin(ang[0,2]) << pos(1,0) << endr
	 << -sin(ang[0,1]) << cos(ang[0,1])*sin(ang[0,2]) << cos(ang[0,1])*cos(ang[0,2]) << pos(2,0) << endr
   << 0 << 0 << 0 << 1 << endr;

  return T_arma;
}

void square_obs()
{
  float x_obs[4] = {6.4, 2.6, 6, 9.9};
  float y_obs[4] = {4.6, 8.5, 12.4, 8.1};
  unsigned char point_num = 10;
  float grad_x, grad_y;

  msg->width = msg->width + 4*point_num;
  // msg->width = 1;

  for(unsigned char i = 0; i < 4; i++)
  {

    if(i!=3)
    {
      grad_x = (x_obs[i+1] - x_obs[i])/ ((float) (point_num));
      grad_y = (y_obs[i+1] - y_obs[i])/ ((float) (point_num));
    }
    else
    {
      grad_x = (x_obs[0] - x_obs[i])/ ((float) (point_num));
      grad_y = (y_obs[0] - y_obs[i])/ ((float) (point_num));
    }

    for(unsigned char j = 0; j < point_num; j++)
    {
      msg->points.push_back (pcl::PointXYZ(x_obs[i]+grad_x*j, y_obs[i]+grad_y*j, 0.0));
    }
  }
}

void plane_obs(float x_cen, float y_cen, float z_cen, float plane_length, float alpha, float beta, float gamma)
{
  float x_obs[4];
  float y_obs[4];
  float z_obs[4];
  unsigned char corner=0;
  char signs_i, signs_j;
  mat obs_vec, obs_rot, obs_new;

  for(unsigned char i=0; i<2; i++)
  {
    for(unsigned char j=0; j<2; j++)
    {
      if(i%2==0) signs_i = -1;
      else signs_i = 1;

      if(j%2==0) signs_j = -1;
      else signs_j = 1;

      x_obs[corner] = plane_length/2*(signs_i);
      y_obs[corner] = 0;
      z_obs[corner] = plane_length/2*(signs_j);

      corner++;
    }
  }

  unsigned char point_num = 10;
  float grad_x, grad_y, grad_z;
  msg->width = msg->width + pow(point_num,2);
  // msg->width = 1;
  grad_x = grad_z = plane_length/((float)(point_num));
  mat angles, positions;
  for(unsigned char i = 0; i < point_num; i++)
  {
    for(unsigned char j = 0; j < point_num; j++)
    {
      obs_vec << x_obs[0]+grad_x*j << endr
              << y_obs[0] << endr
              << z_obs[0]+grad_z*i << endr
              << 1 << endr;

      angles << alpha << endr
            << beta << endr
            << gamma << endr;
      positions << x_cen << endr
                << y_cen << endr
                << z_cen << endr;
      obs_rot = AngletoR(angles, positions);
      // obs_rot << cos(theta) << sin(theta) << 0 << x_cen << endr
      //         << -sin(theta) << cos(theta) << 0 << y_cen << endr
      //         << 0 << 0 << 1 << z_cen << endr
      //         << 0 << 0 << 0 << 1 << endr;
      obs_new = obs_rot*obs_vec;
      msg->points.push_back (pcl::PointXYZ(obs_new(0,0), obs_new(1,0), obs_new(2,0)));
    }
  }
}

void cube_obs(float x_cen, float y_cen, float z_cen, float plane_length, float theta)
{
  char signs_i, signs_j;
  float y_obs[2];
  for(unsigned char j=0; j<2; j++)
  {
    if(j%2==0) signs_j = -1;
    else signs_j = 1;

    y_obs[j] = y_cen+plane_length/2*(signs_j);
    plane_obs(x_cen, y_obs[j], z_cen, plane_length, theta, 0, 0);
    plane_obs(x_cen, y_obs[j], z_cen, plane_length, theta, 0, 0);
    // plane_obs(x_cen+plane_length/2*(signs_j), y_obs[j]+plane_length/2, z_cen, plane_length, M_PI/2+theta, 0, 0);
  }

}

void circle_obs()
{
  float x_cen = 6;
  float y_cen = 6;
  float radius = 2;
  unsigned char point_num = 40;

  msg->width = msg->width + point_num;
  // msg->width = 1;

  for(unsigned char i = 0; i < point_num; i++)
  {
    msg->points.push_back (pcl::PointXYZ(x_cen+radius*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*i), 0.0));
  }
}

void spherical_obs(float x_cen, float y_cen, float z_cen, float radius)
{
  unsigned char point_num = 40;

  msg->width = msg->width + pow(point_num,2);
  // msg->width = 1;

  for(unsigned char i = 0; i < point_num; i++)
  {
    for(unsigned char j = 0; j< point_num; j++)
    {
      msg->points.push_back (pcl::PointXYZ(x_cen+radius*sin(2*M_PI/((float)(point_num))*j)*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*j)*sin(2*M_PI/((float)(point_num))*i), z_cen+radius*cos(2*M_PI/((float)(point_num))*j)));
    }
  }
}

void double_spherical_obs()
{
  float x_cen = 6;
  float y_cen = 6;
  float z_cen = 0;
  float radius = 2;
  unsigned char point_num = 40;

  msg->width = msg->width + 2*pow(point_num,2);
  // msg->width = 1;

  for(unsigned char i = 0; i < point_num; i++)
  {
    for(unsigned char j = 0; j< point_num; j++)
    {
      msg->points.push_back (pcl::PointXYZ(x_cen+radius*sin(2*M_PI/((float)(point_num))*j)*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*j)*sin(2*M_PI/((float)(point_num))*i), z_cen+radius*cos(2*M_PI/((float)(point_num))*j)));
    }
  }

  x_cen = 6;
  y_cen = 6;
  z_cen = 2;
  radius = 2;

  for(unsigned char i = 0; i < point_num; i++)
  {
    for(unsigned char j = 0; j< point_num; j++)
    {
      msg->points.push_back (pcl::PointXYZ(x_cen+radius*sin(2*M_PI/((float)(point_num))*j)*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*j)*sin(2*M_PI/((float)(point_num))*i), z_cen+radius*cos(2*M_PI/((float)(point_num))*j)));
    }
  }

}

void environment_exp()
{
  // Env1
  plane_obs(0.2,0.0,0.06,0.03,M_PI/2,0,0);
  spherical_obs(0.3, 0.06, 0.05, 0.015);
  spherical_obs(0.3, -0.06, 0.05, 0.015);
  plane_obs(0.4,0.02,0.05,0.02,M_PI/2,0,0);
  plane_obs(0.5,-0.04,0.05,0.02,M_PI/2,0,0);
  spherical_obs(0.5, 0.08, 0.05, 0.015);
  spherical_obs(0.6, 0.02, 0.06, 0.025);

  // Env2
  spherical_obs(0.3, -0.05, -0.025, 0.015);
  plane_obs(0.40,0.05,-0.04,0.03,M_PI/2,0,0);
  spherical_obs(0.4, -0.05, -0.04, 0.015);
  plane_obs(0.5,-0.045,-0.045,0.02,M_PI/2,0,0);
  plane_obs(0.5,0.045,-0.035,0.02,M_PI/2,0,0);
  spherical_obs(0.6, -0.05, -0.025, 0.015);
}
int main(int argc, char** argv)
{
  int real_baxter;
  ros::init (argc, argv, "pub_pcl");
  ros::NodeHandle nh;
  ros::Publisher pub = nh.advertise<PointCloud> ("obstacle_point", 10);
  msg->header.frame_id = "world";
  msg->height = 1;

  // square_obs();
  // circle_obs();
  // spherical_obs(0.15, 0.0, 0.38, 0.05);
  // double_spherical_obs();
  // real_baxter = 1;

  // environment_exp();
    // plane_obs(0.2,0.0,0.15,0.05,M_PI/2,0,0);

    // For a static base:
    // plane_obs(0.085,0.0,0.0,0.005,0,0,0);
    // cube_obs(0.07,0.0,0.5,0.05,0);
    // cube_obs(5.07,0.0,0.5,0.05,0);
    // spherical_obs(0.2, 0.02, 0.05, 0.01);
    // plane_obs(0.2,0.02,0.05,0.02,M_PI/2,0,0);
  // Exp1
  // plane_obs(0.0,0.0,0.1,0.005,M_PI/2,0,0);
  // Exp2
  // plane_obs(0.0,0.0,0.095,0.01,M_PI/2,0,0);
  plane_obs(0.0, 1.0, 0.25, 0.5, 0.0, 0.0, 0.0);
  ros::Rate loop_rate(10);
  while (nh.ok())
  {
    // msg->header.stamp = ros::Time::now().toNSec();
    pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
    pub.publish (msg);
    ros::spinOnce ();
    loop_rate.sleep ();
  }
}
