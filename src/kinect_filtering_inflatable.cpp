#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_ros/transforms.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Transform.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/conversions.h>
#include <std_msgs/Header.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
PointCloud::Ptr new_msg (new PointCloud);
sensor_msgs::PointCloud2 cloud_in, cloud_out;
geometry_msgs::TransformStamped transform;

int flag_scaling;
double bound_filter = 2.0;
int filter_ind;
double magni(double point1, double point2, double point3)
{
  return sqrt(pow(point1,2)+pow(point2,2)+pow(point3,2));
}

void callback(const PointCloud::ConstPtr& msg)
{
  filter_ind = 0;
  new_msg->points.clear();
  BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
  {
    if(magni(pt.x, pt.y, pt.z)< bound_filter)
    {
      new_msg->points.push_back (pcl::PointXYZ(pt.x, pt.y, pt.z));
      filter_ind++;
    }
  }
  new_msg->width = 1;
  new_msg->height = filter_ind;

  flag_scaling = 1;
}

// void callback_pcd2(const sensor_msgs::PointCloud2::ConstPtr& msg)
// {
//   cloud_in = *msg;
//   // flag_cloud = 1;
// }

int main(int argc, char** argv)
{
  ros::init(argc, argv, "kinect_filter");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe<PointCloud>("/kinect2/qhd/points", 10, callback);
  // ros::Subscriber sub_transform = nh.subscribe<sensor_msgs::PointCloud2>("/openni/depth/points", 10, callback_pcd2);
  ros::Publisher pub = nh.advertise<PointCloud> ("obstacle_point_kinect", 10);
  // ros::Publisher pub_transform = nh.advertise<sensor_msgs::PointCloud2> ("obstacle_point", 10);
  ros::Rate loop_rate(100);
  new_msg->header.frame_id = "kinect2_rgb_optical_frame";

  // new_msg->header.frame_id = "torso";

  // // transform to Torso
  // transform.transform.translation.x = 1.9;
  // transform.transform.translation.y = 0.0;
  // transform.transform.translation.z = 0.0;
  // transform.transform.rotation.x = 0.0;
  // transform.transform.rotation.y = 0.0;
  // transform.transform.rotation.z = 1.0;
  // transform.transform.rotation.w = 0.0;
  // transform.header.frame_id = "torso";
  while (nh.ok())
  {

      if(flag_scaling == 1)
      {
        pcl_conversions::toPCL(ros::Time::now(), new_msg->header.stamp);
        // tf2::doTransform (cloud_in, cloud_out, transform);
        pub.publish(new_msg);
        // pub_transform.publish(cloud_out);
      }
    ros::spinOnce();
    loop_rate.sleep ();
  }

}
