#include "ros/ros.h"
#include <iostream>
#include <armadillo>
#include <math.h>
#include "sensor_msgs/JointState.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int32.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Vector3.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Twist.h"

using namespace std;
using namespace arma;

mat current_pose, force, tip_pose, target_pose, sens_pose;
geometry_msgs::Vector3 dist_to_obs, vel_obs, sens_to_obs, vel_body, vel_tip, central_obs, vel_obs_add, vel_body_add;
geometry_msgs::Twist sp, sp_body;
std_msgs::Float64MultiArray theta_d_obs;
int field_type, joint_num, sens_in, goal_relax, whole_body_avoid, field_add, field_body_type, mobile;
float dt;
std_msgs::Float32 distance_sca;
double redundancy_weight;
std_msgs::Int32 magnet_flag;
mat tip_current;
int flag_init;
double init_data;
std_msgs::Float32 stiffness_sent;
geometry_msgs::Vector3Stamped vel_obs_stamped;
ros::Time twist_time;
int flag_obs;
std_msgs::Float32 redundancy_sent;
int segment_num = 3;

void get_distobs(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 var;
  var = *msg;
  dist_to_obs.x = var.x;
  dist_to_obs.y = var.y;
  dist_to_obs.z = var.z;
  flag_obs = 1;
}

void get_obscentral(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 var;
  var = *msg;
  central_obs.x = var.x;
  central_obs.y = var.y;
  central_obs.z = var.z;
}

void get_sensobs(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 var;
  var = *msg;
  sens_to_obs.x = var.x;
  sens_to_obs.y = var.y;
  sens_to_obs.z = var.z;
}

void get_sensin(const std_msgs::Int32::ConstPtr& msg)
{
  std_msgs::Int32 var;
  var = *msg;
  sens_in = var.data;
}

void get_twist(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  geometry_msgs::Vector3Stamped var;
  var = *msg;
  sp.linear.x = var.vector.x;
  sp.linear.y = var.vector.y;
  sp.linear.z = var.vector.z;
  twist_time = var.header.stamp;
}

void get_wrist_twist(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  geometry_msgs::Vector3Stamped var;
  var = *msg;
  sp_body.linear.x = var.vector.x;
  sp_body.linear.y = var.vector.y;
  sp_body.linear.z = var.vector.z;
}

void get_tip(const geometry_msgs::PoseArray::ConstPtr& msg)
{
  geometry_msgs::PoseArray var;
  var = *msg;
  tip_pose << var.poses[segment_num-1].position.x << endr
           << var.poses[segment_num-1].position.y << endr
           << var.poses[segment_num-1].position.z << endr;
}

void get_body(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 var;
  var = *msg;
  sens_pose << var.x << endr
           << var.y << endr
           << var.z << endr;
}

void get_target(const geometry_msgs::Pose::ConstPtr& msg)
{
  geometry_msgs::Pose var;
  var = *msg;
  target_pose << var.position.x << endr
              << var.position.y << endr
              << var.position.z << endr;
  if(flag_init == 0)
  {
    init_data = norm(target_pose - tip_pose);
    flag_init = 1;
  }
}

double find_magnitude(double input1, double input2, double input3)
{
  double magnitude = sqrt(pow(input1,2) + pow(input2,2)+ pow(input3,2));
  return magnitude;
}

void collision_avoid_tip()
{
  double dist_const = 0.0;

  // Sim
  double bound = 0.04;
  double bound_add = 0.015;
  double bound_elec = 0.1;
  double constant = 0.0002;

  // Exp
  // double bound_elec = 0.02;
  // double constant = 0.00002;


  double constant2 = 1.5;
  double constant_add = 0.008;
  double distance, speed, mag;
  mat agent_cur, dist_from_obs, obs_cur, lc_cross, B, Force, la_cross, tip_base;
  mat b_vect, m_x_vect, d_vect, n_cross, d_cross;
  mat agent_vel, vect_to_obs, u_t, w_vect, u_g_vect, damper;
  mat Force_add;
  double v_scalar, sigma, sigmoid, energy;
  distance = find_magnitude(dist_to_obs.x, dist_to_obs.y, dist_to_obs.z) - dist_const;
  speed = find_magnitude(sp.linear.x, sp.linear.y, sp.linear.z);
  if(field_type == 0)
  {
    if(distance > bound_elec)
    {
      vel_obs.x = 0;
      vel_obs.y = 0;
      vel_obs.z = 0;
    }

    else
    {
      if(distance !=0)
      {
        vel_obs.y = -constant*(1/distance - 1/bound_elec)*(dist_to_obs.y)/(pow(distance,3));
        vel_obs.x = -constant*(1/distance - 1/bound_elec)*(dist_to_obs.x)/(pow(distance,3));
        vel_obs.z = -constant*(1/distance - 1/bound_elec)*(dist_to_obs.z)/(pow(distance,3));
      }
    }
  }

  else if(field_type == 1)
  {
    if((dist_to_obs.x!=0) && (dist_to_obs.y!=0) && (dist_to_obs.z!=0))
    {
      if(distance > (bound))
      {
        vel_obs.x = 0;
        vel_obs.y = 0;
        vel_obs.z = 0;
        magnet_flag.data = 0;
      }
      else
      {
        magnet_flag.data = 1;
        // cout << "obs" << endl;
        agent_cur << sp.linear.x << endr
                  << sp.linear.y << endr
                  << sp.linear.z << endr;

        if(speed!=0) agent_cur = agent_cur/(speed);
        dist_from_obs << -1*dist_to_obs.x << endr
                      << -1*dist_to_obs.y << endr
                      << -1*dist_to_obs.z << endr;
        obs_cur = agent_cur - (dot(agent_cur, dist_from_obs)/norm(dist_from_obs))*(dist_from_obs/norm(dist_from_obs));
        tip_current = obs_cur;

        // For exp
        if(mobile == 1)
        {
          if(obs_cur(2,0)>0.0)
          {
            obs_cur(2,0) = 0.0;
          }
        }
        else
        {
          if(obs_cur(0,0)>0)
          {
            obs_cur(0,0) = 0.0;
          }
        }
        mag = pow((pow(obs_cur(0,0),2)+pow(obs_cur(1,0),2)+pow(obs_cur(2,0),2)),0.5);
        // obs_cur = obs_cur/mag;
        // cout << mag << endl;
        if(mag <= 0.05)
        {
          // cout << "90" << endl;
          // tip_base = -1*tip_pose;
          // obs_cur = tip_base - (dot(tip_base, dist_from_obs)/norm(dist_from_obs))*(dist_from_obs/norm(dist_from_obs));
          // mag = pow((pow(obs_cur(0,0),2)+pow(obs_cur(1,0),2)+pow(obs_cur(2,0),2)),0.5);
          obs_cur = obs_cur/mag;
        }
        lc_cross << 0 << -obs_cur(2,0) << obs_cur(1,0) << endr
                 << obs_cur(2,0) << 0 << -obs_cur(0,0) << endr
                 << -obs_cur(1,0) << obs_cur(0,0) << 0 << endr;
        B << sp.linear.x << endr
          << sp.linear.y << endr
          << sp.linear.z << endr;
        B = lc_cross*B/distance;
        la_cross << 0 << -agent_cur(2,0) << agent_cur(1,0) << endr
                 << agent_cur(2,0) << 0 << -agent_cur(0,0) << endr
                 << -agent_cur(1,0) << agent_cur(0,0) << 0 << endr;
        Force = constant2*la_cross*B;

        if(distance > bound_add)
        {
          vel_obs_add.x = 0;
          vel_obs_add.y = 0;
          vel_obs_add.z = 0;
        }
        else
        {
          if(distance !=0)
          {
            if(field_add == 1)
            {
              vel_obs_add.y = -constant_add*(1/distance - 1/bound_add)*(dist_to_obs.y)/(distance);
              vel_obs_add.x = -constant_add*(1/distance - 1/bound_add)*(dist_to_obs.x)/(distance);
              vel_obs_add.z = -constant_add*(1/distance - 1/bound_add)*(dist_to_obs.z)/(distance);
            }
            // else
            // {
            //   vel_obs_add.y = -constant2*speed*(dist_to_obs.y)/(distance);
            //   vel_obs_add.x = -constant2*speed*(dist_to_obs.x)/(distance);
            //   vel_obs_add.z = -constant2*speed*(dist_to_obs.z)/(distance);
            // }
          }
          else
          {
            cout << "Colliding...\n" << endl;
          }
        }

        // cout << "dot product in obs: " << dot(Force, agent_cur) << endl;
        // Force.print("F=");
        // agent_cur.print("v=");
        Force_add << vel_obs_add.x << endr
                  << vel_obs_add.y << endr
                  << vel_obs_add.z << endr;
        Force_add = Force_add - dot(Force_add, agent_cur)*agent_cur;

        // Force_add.print("Force_add: ");
        // vel_obs.x = sp.linear.x + Force(0,0)*dt;
        // vel_obs.y = sp.linear.y + Force(1,0)*dt;
        // vel_obs.z = sp.linear.z + Force(2,0)*dt;
        if(field_add != 0)
        {
          vel_obs.x = Force(0,0) + Force_add(0,0);
          vel_obs.y = Force(1,0) + Force_add(1,0);
          vel_obs.z = Force(2,0) + Force_add(2,0);
        }
        else
        {
          vel_obs.x = Force(0,0);
          vel_obs.y = Force(1,0);
          vel_obs.z = Force(2,0);
          // vel_obs.x = sp.linear.x + Force(0,0)*dt;
          // vel_obs.y = sp.linear.y + Force(1,0)*dt;
          // vel_obs.z = sp.linear.z + Force(2,0)*dt;
        }

      }
    }
  }
  else if(field_type==3)
  {
    // Haddadin
    if((dist_to_obs.x!=0) && (dist_to_obs.y!=0) && (dist_to_obs.z!=0))
    {
      if(distance > (bound) || flag_init==0)
      {
        vel_obs.x = 0;
        vel_obs.y = 0;
        vel_obs.z = 0;
        magnet_flag.data = 0;
      }
      else
      {
        magnet_flag.data = 1;
        // cout << "obs" << endl;
        agent_cur << sp.linear.x << endr
                  << sp.linear.y << endr
                  << sp.linear.z << endr;
        if(speed!=0) agent_cur = agent_cur/(speed);

        b_vect = target_pose - tip_pose;
        m_x_vect << central_obs.x << endr
                 << central_obs.y << endr
                 << central_obs.z << endr;
        d_vect = m_x_vect - dot(m_x_vect,b_vect)*b_vect/norm(b_vect);
        dist_from_obs << -1*dist_to_obs.x << endr
                      << -1*dist_to_obs.y << endr
                      << -1*dist_to_obs.z << endr;
        n_cross << 0 << -dist_from_obs(2,0) << dist_from_obs(1,0) << endr
                << dist_from_obs(2,0) << 0 << -dist_from_obs(0,0) << endr
                << -dist_from_obs(1,0) << dist_from_obs(0,0) << 0 << endr;
        d_cross << 0 << -d_vect(2,0) << d_vect(1,0) << endr
                << d_vect(2,0) << 0 << -d_vect(0,0) << endr
                << -d_vect(1,0) << d_vect(0,0) << 0 << endr;
        obs_cur = (n_cross/distance)*(d_cross*b_vect/norm(d_cross*b_vect));

        lc_cross << 0 << -obs_cur(2,0) << obs_cur(1,0) << endr
                 << obs_cur(2,0) << 0 << -obs_cur(0,0) << endr
                 << -obs_cur(1,0) << obs_cur(0,0) << 0 << endr;
        B << sp.linear.x << endr
          << sp.linear.y << endr
          << sp.linear.z << endr;
        B = lc_cross*B/(speed*pow(distance,2));
        la_cross << 0 << -agent_cur(2,0) << agent_cur(1,0) << endr
                 << agent_cur(2,0) << 0 << -agent_cur(0,0) << endr
                 << -agent_cur(1,0) << agent_cur(0,0) << 0 << endr;
        Force = 0.1*constant2*la_cross*B;
        // Force.print("force:");
        vel_obs.x = Force(0,0);
        vel_obs.y = Force(1,0);
        vel_obs.z = Force(2,0);
      }
    }
  }

  else if(field_type==4)
  {
    // Sabattini
    if((dist_to_obs.x!=0) && (dist_to_obs.y!=0) && (dist_to_obs.z!=0))
    {
      if(distance > (bound) || flag_init == 0)
      {
        vel_obs.x = 0;
        vel_obs.y = 0;
        vel_obs.z = 0;
        magnet_flag.data = 0;
      }
      else
      {
        magnet_flag.data = 1;

        agent_vel << sp.linear.x << endr
                  << sp.linear.y << endr
                  << sp.linear.z << endr;
        vect_to_obs << dist_to_obs.x << endr
                    << dist_to_obs.y << endr
                    << dist_to_obs.z << endr;
        if(speed!=0)
        {
          agent_cur = agent_vel;
        }
        else
        {
          agent_cur = vect_to_obs;
        }

        u_t = target_pose - tip_pose;
        u_t = u_t*0.1;
        w_vect = u_t - dot(u_t,agent_cur)*agent_cur/pow(norm(agent_cur),2);
        u_g_vect = -constant2*w_vect/norm(w_vect);
        v_scalar = dot(agent_vel,vect_to_obs);
        if(v_scalar>=0)
        {
          sigma = 1;
          sigmoid = 1;
        }
        else
        {
          sigma = 0;
          sigmoid = -1;
        }
        energy = 0.1*pow(init_data,2);
        damper = -0.05*sigmoid*energy/norm(vect_to_obs)*(abs(v_scalar)+exp(-abs(v_scalar)))*vect_to_obs/norm(vect_to_obs);

        Force = sigma*(u_g_vect+damper);

        vel_obs.x = Force(0,0);
        vel_obs.y = Force(1,0);
        vel_obs.z = Force(2,0);
      }
    }

  }

  force << vel_obs.x << endr
        << vel_obs.y << endr
        << vel_obs.z << endr;
  vel_obs_stamped.vector.x = vel_obs.x;
  vel_obs_stamped.vector.y = vel_obs.y;
  vel_obs_stamped.vector.z = vel_obs.z;

  // vel_obs_stamped.header.stamp = ros::Time::now();
  vel_obs_stamped.header.stamp = twist_time;
  distance_sca.data = pow((pow(dist_to_obs.x,2)+pow(dist_to_obs.y,2)+pow(dist_to_obs.z,2)),0.5);
}

void collision_avoid_body()
{
  // double bound = 0.05;
  double bound = 0.08;
  double bound_add = 0.015;
  double bound_elec = 0.1;
  double constant = 0.0002;

  double distance;

  // double constant = 0.01;
  double constant2 = 0.001;
  double constant_add = 0.008;
  double dist_const = 0.0;
  double speed, mag, weight_body;
  mat agent_cur, dist_from_obs, obs_cur, lc_cross, B, Force, Force_add, la_cross, tip_base;
  distance = find_magnitude(sens_to_obs.x, sens_to_obs.y, sens_to_obs.z) - dist_const;
  if(distance == 0)
  {
    distance = 1000;
  }
  speed = find_magnitude(sp_body.linear.x, sp_body.linear.y, sp_body.linear.z);
  mat vect_goal, vect_obs, tip_to_goal;
  double cos_angle;
  double weight_vector, weight_current;
  int flag_weight, flag_arrive;
  // cout << distance << endl;
  if(field_body_type == 0)
  {
    if(distance > bound)
    {
      vel_body.x = 0;
      vel_body.y = 0;
      vel_body.z = 0;
    }

    else
    {
      // cout << "body" << endl;
      if(distance !=0)
      {
        vel_body.y = -constant2*(1/distance - 1/bound)*(sens_to_obs.y)/(pow(distance,2));
        vel_body.x = -constant2*(1/distance - 1/bound)*(sens_to_obs.x)/(pow(distance,2));
        vel_body.z = -constant2*(1/distance - 1/bound)*(sens_to_obs.z)/(pow(distance,2));
        // vel_body.y = -2*(1 - pow(sin(M_PI*distance/(2*bound)),0.25))*(sens_to_obs.y)/(distance);
        // vel_body.x = -2*(1 - pow(sin(M_PI*distance/(2*bound)),0.25))*(sens_to_obs.x)/(distance);
      }
    }
  }

  else if(field_body_type == 1)
  {
    if((sens_to_obs.x!=0) && (sens_to_obs.y!=0) && (sens_to_obs.z!=0))
    {
      if(distance > (bound))
      {
        vel_body.x = 0;
        vel_body.y = 0;
        vel_body.z = 0;
        magnet_flag.data = 0;
      }
      else
      {
        magnet_flag.data = 1;
        // cout << "obs" << endl;
        agent_cur << sp_body.linear.x << endr
                  << sp_body.linear.y << endr
                  << sp_body.linear.z << endr;
        if(speed!=0) agent_cur = agent_cur/(speed);
        dist_from_obs << -1*sens_to_obs.x << endr
                      << -1*sens_to_obs.y << endr
                      << -1*sens_to_obs.z << endr;
        obs_cur = agent_cur - (dot(agent_cur, dist_from_obs)/norm(dist_from_obs))*(dist_from_obs/norm(dist_from_obs));
        // if(obs_cur(2,0)>0.0)
        {
          obs_cur(2,0) = 0.0;
        }

        // mat dot_cur = (obs_cur.t()*tip_current);
        // if(dot_cur(0,0) < 0)
        // {
        //   obs_cur = -1*obs_cur;
        // }
        mag = pow((pow(obs_cur(0,0),2)+pow(obs_cur(1,0),2)+pow(obs_cur(2,0),2)),0.5);
        // obs_cur = obs_cur/mag;
        // cout << mag << endl;
        if(mag <= 0.05)
        {
          // cout << "90" << endl;
          // tip_base = -1*tip_pose;
          // obs_cur = tip_base - (dot(tip_base, dist_from_obs)/norm(dist_from_obs))*(dist_from_obs/norm(dist_from_obs));
          // mag = pow((pow(obs_cur(0,0),2)+pow(obs_cur(1,0),2)+pow(obs_cur(2,0),2)),0.5);
          obs_cur = obs_cur/mag;
        }
        lc_cross << 0 << -obs_cur(2,0) << obs_cur(1,0) << endr
                 << obs_cur(2,0) << 0 << -obs_cur(0,0) << endr
                 << -obs_cur(1,0) << obs_cur(0,0) << 0 << endr;
        B << sp_body.linear.x << endr
          << sp_body.linear.y << endr
          << sp_body.linear.z << endr;
        B = lc_cross*B/distance;
        la_cross << 0 << -agent_cur(2,0) << agent_cur(1,0) << endr
                 << agent_cur(2,0) << 0 << -agent_cur(0,0) << endr
                 << -agent_cur(1,0) << agent_cur(0,0) << 0 << endr;
        Force = constant2*la_cross*B;

        if(distance > bound_add)
        {
          vel_body_add.x = 0;
          vel_body_add.y = 0;
          vel_body_add.z = 0;
        }
        else
        {
          if(distance !=0)
          {
            if(field_add == 1)
            {
              vel_body_add.y = -constant_add*(1/distance - 1/bound_add)*(sens_to_obs.y)/(distance);
              vel_body_add.x = -constant_add*(1/distance - 1/bound_add)*(sens_to_obs.x)/(distance);
              vel_body_add.z = -constant_add*(1/distance - 1/bound_add)*(sens_to_obs.z)/(distance);
            }
            // else
            // {
            //   vel_obs_add.y = -constant2*speed*(dist_to_obs.y)/(distance);
            //   vel_obs_add.x = -constant2*speed*(dist_to_obs.x)/(distance);
            //   vel_obs_add.z = -constant2*speed*(dist_to_obs.z)/(distance);
            // }
          }
          else
          {
            cout << "Colliding...\n" << endl;
          }
        }

        Force_add << vel_body_add.x << endr
                  << vel_body_add.y << endr
                  << vel_body_add.z << endr;
        // Force_add = Force_add - dot(Force_add, agent_cur)*agent_cur;

        // Force_add.print("Force_add: ");
        // vel_obs.x = sp.linear.x + Force(0,0)*dt;
        // vel_obs.y = sp.linear.y + Force(1,0)*dt;
        // vel_obs.z = sp.linear.z + Force(2,0)*dt;

        if(flag_init == 1 && flag_obs == 1)
        {
          // vect_goal = target_pose - tip_pose;
          tip_to_goal = target_pose - tip_pose;
          vect_goal = target_pose - sens_pose;
          vect_obs << sens_to_obs.x << endr
                   << sens_to_obs.y << endr
                   << sens_to_obs.z << endr;

          if((vect_obs(0,0)+vect_obs(1,0)+vect_obs(2,0)) != 0 && (vect_goal(0,0)+vect_goal(1,0)+vect_goal(2,0)) != 0)
          {

            cos_angle = (dot(vect_goal, vect_obs))/(norm(vect_goal)*norm(vect_obs));
            // weight_body = 1+cos_angle;
          }
          else
          {
            // weight_body = 1;
            cos_angle = 0;
          }
          if(cos_angle>0) weight_body = 1;
          else weight_body = 0;
        }

        else
        {
          weight_body = 1;
        }

        weight_vector = dot(vect_goal, agent_cur)/norm(vect_goal);
        weight_current = dot(vect_goal, obs_cur)/norm(vect_goal);
        if(norm(tip_to_goal)<bound) flag_arrive = 1;
        else flag_arrive = 0;
        tip_to_goal.print("vect_goal:");
        cout << "weight_body " << weight_body << endl;
        // cout << "weight_vector " << weight_vector << endl;
        cout << "weight_current " << weight_current << endl;
        cout << "flag_arrive" << flag_arrive << endl;
        if((weight_body == 0 && weight_current<0) || (weight_body == 0 && flag_arrive==1) || (weight_current<0 && flag_arrive==1)) flag_weight = 1;
        else flag_weight = 0;
        cout << "flag_weight" << flag_weight << endl;

        if(field_add != 0)
        {
          // vel_body.x = weight_body*Force(0,0) + Force_add(0,0);
          // vel_body.y = weight_body*Force(1,0) + Force_add(1,0);
          // vel_body.z = weight_body*Force(2,0) + Force_add(2,0);
          vel_body.x = (1-flag_weight)*Force(0,0) + Force_add(0,0);
          vel_body.y = (1-flag_weight)*Force(1,0) + Force_add(1,0);
          vel_body.z = (1-flag_weight)*Force(2,0) + Force_add(2,0);
        }
        else
        {
          vel_body.x = Force(0,0);
          vel_body.y = Force(1,0);
          vel_body.z = Force(2,0);
          // vel_obs.x = sp.linear.x + Force(0,0)*dt;
          // vel_obs.y = sp.linear.y + Force(1,0)*dt;
          // vel_obs.z = sp.linear.z + Force(2,0)*dt;
        }
        // Force.print("F_bod:");
        // Force_add.print("F_bod_rep:");
        // vel_body.x = sp_body.linear.x + Force(0,0)*dt;
        // vel_body.y = sp_body.linear.y + Force(1,0)*dt;
        // vel_body.z = sp_body.linear.z + Force(2,0)*dt;
        // vel_body.x = Force(0,0);
        // vel_body.y = Force(1,0);
        // vel_body.z = Force(2,0);
      }
    }
  }

  // cout << "weight" << weight_body << endl;
  // sens_pose.print("sens:");

  if(distance>=1.1*bound)
  {
    redundancy_weight = 0;
    // cout << "2" << endl;
  }
  else if(distance>bound && distance<1.1*bound)
  {
    redundancy_weight = pow(cos((M_PI/2)*((distance-bound)/(0.1*bound))),2);
    // cout << "1" << endl;
  }
  else if(distance < bound)
  {
    redundancy_weight = 1;
    // cout << "0" << endl;
  }
  redundancy_sent.data = redundancy_weight;
}

void stiffness_calculate()
{
  double dist_obs;
  dist_obs = pow((pow(dist_to_obs.x,2)+pow(dist_to_obs.y,2)+pow(dist_to_obs.z,2)),0.5);
  mat vect_goal, vect_obs;

  double bound = 0.04;
  double cos_angle, weight, new_weight;

  if(dist_obs<bound && flag_init == 1 && flag_obs == 1)
  {
    vect_goal = target_pose - tip_pose;
    vect_obs << dist_to_obs.x << endr
             << dist_to_obs.y << endr
             << dist_to_obs.z << endr;

    if(dist_obs<bound && (vect_obs(0,0)+vect_obs(1,0)+vect_obs(2,0)) != 0 && (vect_goal(0,0)+vect_goal(1,0)+vect_goal(2,0)) != 0)
    {

      cos_angle = (dot(vect_goal, vect_obs))/(norm(vect_goal)*norm(vect_obs));
      weight = 1-cos_angle;
      if(norm(vect_goal)>init_data)
      {
        new_weight = exp(-(norm(vect_goal)-init_data)/0.1);
      }
      else
      {
        new_weight = 1;
      }
    }
    else
    {
      weight = 1;
      new_weight = 1;
    }
  }

  else
  {
    weight = 1;
    new_weight = 1;
  }
  // cout << "dist_obs" << weight << endl;
  // cout << "weight" << weight << endl;
  // cout << "new_weight" << new_weight << endl;
  new_weight = 1;
  stiffness_sent.data = (1.0-exp(-(dist_obs)/(0.5*bound)))*weight*new_weight;
}

int
main(int argc, char** argv)
{
  ros::init(argc,argv, "obstacle_avoidance");
  ros::NodeHandle n;
  n.getParam("bend_number", segment_num);
  n.param("field_type", field_type, field_type);
  // n.param("field_body_type", field_body_type, field_body_type);
  n.param("field_add", field_add, field_add);
  n.param("goal_relaxation", goal_relax, goal_relax);
  // n.param("whole_body_avoid", whole_body_avoid, whole_body_avoid);
  // n.param("mobile_base", mobile, mobile);
  flag_obs = 0;
  ros::Publisher pub_force = n.advertise<geometry_msgs::Vector3>("/force_obs",10);
  ros::Publisher pub_force_stamp = n.advertise<geometry_msgs::Vector3Stamped>("/force_obs_stamped",10);
  ros::Publisher forcebody_pub = n.advertise<geometry_msgs::Vector3>("force_body_obs",10);
  ros::Publisher magnetic_pub = n.advertise<std_msgs::Int32>("/magnetic_flag",10);
  ros::Publisher stiffness_pub = n.advertise<std_msgs::Float32>("/stiffness_scalar",10);
  ros::Publisher redundancy_pub = n.advertise<std_msgs::Float32>("redundancy_weight",10);

  ros::Subscriber sub_distobs = n.subscribe("/agent/dist_to_obs", 10, get_distobs);
  ros::Subscriber sub_distsens = n.subscribe("/agent/sensor_to_obs", 10, get_sensobs);
  ros::Subscriber sub_twist = n.subscribe("/tip_twist", 10, get_twist);
  ros::Subscriber sub_body_twist = n.subscribe("/agent/body/twist", 10, get_wrist_twist);
  ros::Subscriber sub_tip = n.subscribe("/tip_pose", 10, get_tip);
  ros::Subscriber sub_sens = n.subscribe("/agent/body/current", 10, get_body);
  ros::Subscriber sub_target = n.subscribe("/goal_pose", 10, get_target);
  ros::Subscriber sub_central = n.subscribe("/obs_central", 10, get_obscentral);

  tip_current.set_size(3,1);
  tip_current.zeros();

  float freq = 50.0;
  dt = 1.0/freq;
  ros::Rate loop_rate(freq);
  force.set_size(2, 1);
  force.zeros();
  flag_init = 0;
  dist_to_obs.x = 100;
  dist_to_obs.y = 100;
  dist_to_obs.z = 100;

  while(ros::ok())
  {
    n.getParam("bend_number", segment_num);
    collision_avoid_tip();
    if(whole_body_avoid == 1)
    {
      collision_avoid_body();
    }

    if(goal_relax == 1)
    {
      stiffness_calculate();
      stiffness_pub.publish(stiffness_sent);
    }

    pub_force.publish(vel_obs);
    pub_force_stamp.publish(vel_obs_stamped);
    redundancy_pub.publish(redundancy_sent);
    forcebody_pub.publish(vel_body);
    magnetic_pub.publish(magnet_flag);
    ros::spinOnce();
    loop_rate.sleep();
  }

}
