#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Int32.h>
#include <iostream>
#include <string>
using namespace std;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
geometry_msgs::PoseArray obs_pose;
unsigned int obs_length;
unsigned int obs_index, frame_index;
geometry_msgs::Vector3 obs_sent;
geometry_msgs::Vector3 robot_pose;
geometry_msgs::Vector3 obs_avg_sent;
geometry_msgs::Vector3 obs_sent_cand;
unsigned int flag_obs_callback;
int segment_num, sensor_num;
void callback(const PointCloud::ConstPtr& msg)
{
  // if(flag_obs_callback == 0)
  {
    obs_length = (msg->width)*(msg->height);
    obs_pose.poses.clear();
    BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
    {
      geometry_msgs::Pose obs;
      obs.position.x = pt.x;
      obs.position.y = pt.y;
      obs.position.z = pt.z;
      obs_pose.poses.push_back(obs);
    }
    flag_obs_callback = 1;
  }
}

float magni(geometry_msgs::Pose obs_, geometry_msgs::Vector3 rob_)
{
  return pow(pow(obs_.position.x - rob_.x,2)+pow(obs_.position.y - rob_.y,2)+pow(obs_.position.z - rob_.z,2),0.5);
}

float dist_calculate(geometry_msgs::Vector3 vect)
{
  return pow(pow(vect.x,2)+pow(vect.y,2)+pow(vect.z,2),0.5);
}

geometry_msgs::Vector3 closest_obs(geometry_msgs::Vector3 active_pose)
{
  geometry_msgs::Vector3 obs_return;
  float minim = 100;
  float avg_x = 0;
  float avg_y = 0;
  float avg_z = 0;
  for(unsigned int i=0; i<obs_length; i++)
  {
    avg_x = avg_x + obs_pose.poses[i].position.x;
    avg_y = avg_y + obs_pose.poses[i].position.y;
    avg_z = avg_z + obs_pose.poses[i].position.z;
    if(magni(obs_pose.poses[i],active_pose)<minim)
    {

      minim = magni(obs_pose.poses[i],active_pose);
      obs_index = i;

    }
    // cout << "dist:" << magni(obs_pose.poses[i],robot_pose) << endl;
    // cout << "obs" << i << ": " << obs_pose.poses[i].position << endl;
    // cout << "rob:" << robot_pose << endl;
  }
  avg_x = avg_x / obs_length;
  avg_y = avg_y / obs_length;
  avg_z = avg_z / obs_length;
  obs_avg_sent.x = avg_x - robot_pose.x;
  obs_avg_sent.y = avg_y - robot_pose.y;
  obs_avg_sent.z = avg_z - robot_pose.z;
  // cout << "obs:" << obs_index << endl;
  // cout << "obs_length:" << obs_length << endl;
  // cout << robot_pose << endl;
  // cout << "dist_close:" << magni(obs_pose.poses[obs_index],robot_pose) << endl;
  obs_return.x = obs_pose.poses[obs_index].position.x - active_pose.x;
  obs_return.y = obs_pose.poses[obs_index].position.y - active_pose.y;
  obs_return.z = obs_pose.poses[obs_index].position.z - active_pose.z;

  return obs_return;
}

geometry_msgs::Vector3 closest_obs_averaging(geometry_msgs::Vector3 active_pose)
{
  geometry_msgs::Vector3 obs_return;
  // float bound = 0.6;
  float bound = 0.04;
  float avg_x = 0;
  float avg_y = 0;
  float avg_z = 0;
  int obs_close=0;
  for(unsigned int i=0; i<obs_length; i++)
  {
    if(magni(obs_pose.poses[i],active_pose)<bound)
    {
      avg_x = avg_x + obs_pose.poses[i].position.x;
      avg_y = avg_y + obs_pose.poses[i].position.y;
      avg_z = avg_z + obs_pose.poses[i].position.z;
      obs_close++;
    }
  }
  if(obs_close!=0)
  {
    avg_x = avg_x / obs_close - active_pose.x;
    avg_y = avg_y / obs_close - active_pose.y;
    avg_z = avg_z / obs_close - active_pose.z;
  }
  else
  {
    avg_x = 100;
    avg_y = 100;
    avg_z = 100;
  }

  obs_return.x = avg_x;
  obs_return.y = avg_y;
  obs_return.z = avg_z;

  return obs_return;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "sub_pcl");
  ros::NodeHandle nh;
  nh.param("segment_num", segment_num, segment_num);
  nh.param("sensor_num", sensor_num, sensor_num);

  char flag = 0;
  flag_obs_callback = 0;
  ros::Subscriber sub = nh.subscribe<PointCloud>("obstacle_point", 10, callback);
  ros::Publisher pub_dist = nh.advertise<geometry_msgs::Vector3> ("agent/dist_to_obs", 10);
  ros::Publisher pub_obs = nh.advertise<geometry_msgs::Vector3> ("obs_central", 10);
  tf::TransformListener listener, listen_wrist, listen_upel, listen_lowel, listen_upfore, listen_lowfore;
  ros::Rate loop_rate(50);

  while (nh.ok())
  {
    tf::StampedTransform transform, transform_wrist, transform_upel, transform_lowel, transform_upfore, transform_lowfore;
    stringstream ss;
    nh.getParam("bend_number", segment_num);
    // ss << int(sensor_num*segment_num-1);
    ss << int(segment_num-1);
    string tip_name = ss.str();
    cout << tip_name << endl;
      try
      {
        listener.lookupTransform("/world", "tip"+tip_name,
                                 ros::Time(0), transform);
        flag = 1;
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
      }
      if(flag == 1 && flag_obs_callback == 1)
      {
        robot_pose.x = transform.getOrigin().x();
        robot_pose.y = transform.getOrigin().y();
        robot_pose.z = transform.getOrigin().z();
        if(obs_length!=0)
        {
          obs_sent = closest_obs(robot_pose);
          obs_sent_cand = closest_obs_averaging(robot_pose);
          // cout << "testing2" << endl;
          if(dist_calculate(obs_sent)>dist_calculate(obs_sent_cand))
            {
              obs_sent.x = obs_sent_cand.x;
              obs_sent.y = obs_sent_cand.y;
              obs_sent.z = obs_sent_cand.z;
            }
            static tf::TransformBroadcaster br;
            tf::Transform tr;
            tr.setOrigin( tf::Vector3(obs_sent.x+robot_pose.x, obs_sent.y+robot_pose.y, obs_sent.z+robot_pose.z) );
            tf::Quaternion q_new;
            q_new.setRPY(0, 0, 0);
            tr.setRotation(q_new);
            br.sendTransform(tf::StampedTransform(tr, ros::Time::now(), "world", "avg_new"));
        }
      }

      pub_dist.publish(obs_sent);
      pub_obs.publish(obs_avg_sent);

    ros::spinOnce();
    loop_rate.sleep ();
  }

}
