#include "ros/ros.h"
#include <iostream>
#include <armadillo>
#include <math.h>
#include "geometry_msgs/Pose.h"
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"
#include <tf/transform_broadcaster.h>
#include <string>
#include "sensor_msgs/Joy.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/Vector3.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Int32.h"
#include <tf/transform_datatypes.h>

using namespace std;
using namespace arma;

// For sim
double L0=0.09;
// For exp
// double L0=0.10;
// double L0=0.047;

double tension_min = 0.0;
double tension_max = 5.0;
// For sim
double radi=0.015;
double diam = 2*radi;
double dt;
mat len_vec, len_vec_dot;
mat goal_pose, twist_tip, tip_field_dum, tip_field, twist_body, goal_active;
int segment_num, flag_obs_data, sensor_num, mobile_base, controller, task_on, learning_on, obs_active;
visualization_msgs::MarkerArray markerArray;
ros::Time times_, begin_;
sensor_msgs::Joy joy_msg;
sensor_msgs::JointState joint_sent;
geometry_msgs::Pose goal_sent, tip_sent;
geometry_msgs::Vector3Stamped twist_sent, twist_body_sent;
ros::Time time_sent, time_rec, time_task, time_record;
std_msgs::Float32 stiff;
int sens_in;
mat p_mat;
std_msgs::Float64MultiArray pressure_sent;
mat body_field;
double red_weight;
char flag_move;
mat r_mob, r_dot_mob, dot_vector;
int flag_obs_active;
mat tension, dot_tension;
double pressure, dot_pressure;
double pressure_0 = 100000;
double Young = 60000;
double Area_w = 0.25*M_PI*(pow(diam,2));
double inertia = M_PI*0.5*(pow(diam/2.0,4));
double shear = 1;
double torsion_const = 1;
double kappa, length_final, phi;
int task_completed, task_tracking;
int const total_task = 5;
mat goal_task[total_task];
int dur_task = 3;
// std_msgs::Int32 index_learning;

int index_act, counting;
int inc_num =10;
double increment[6] = {0.000, 0.000, 0.2, 0.2, 0.2, 0.0};
ros::Time begin;
void get_joy(const sensor_msgs::Joy::ConstPtr& msg)
{
  // sim
  float ds = 0.005;
  // exp
  // float ds = 0.005;
  joy_msg = *msg;
  if(task_on == 0)
  {
    goal_pose(0,0) = goal_pose(0,0) + joy_msg.axes[0]*ds;
    goal_pose(1,0) = goal_pose(1,0) + joy_msg.axes[1]*ds;
    goal_pose(2,0) = goal_pose(2,0) + joy_msg.axes[3]*ds;
    if(joy_msg.axes[2]==1) flag_move = 1;
    else if(joy_msg.axes[2]==-1) flag_move = 0;
    if(joy_msg.buttons[1]==1) flag_obs_active = 1;
    else if(joy_msg.buttons[3]==1) flag_obs_active = 0;
  }

  // pressure = pressure + 10000*joy_msg.axes[2];
  // tension(0,0) = tension(0,0) + 0.2*joy_msg.axes[0];
  // tension(1,0) = tension(1,0) + 0.2*joy_msg.axes[1];
  // tension(2,0) = tension(2,0) + 0.2*joy_msg.axes[3];
}

void get_stiffness(const std_msgs::Float32::ConstPtr& msg)
{
  stiff = *msg;
}

void get_force_tip(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 variable;
  variable = *msg;
  tip_field_dum << variable.x << endr
            << variable.y << endr
            << variable.z << endr;
}

void get_body(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 variable;
  variable = *msg;
  body_field << variable.x << endr
            << variable.y << endr
            << variable.z << endr;
}

void get_force_tip_stamp(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{

  geometry_msgs::Vector3Stamped variable;

  variable = *msg;
  time_rec = variable.header.stamp;

  if(time_sent == time_rec)
  {
    // For exp
    if(flag_obs_active == 1)
    {
      tip_field << variable.vector.x << endr
                << variable.vector.y << endr
                << variable.vector.z << endr;
    }
    else
    {
      tip_field.zeros();
    }

        flag_obs_data = 1;
  }

}

void get_sensin(const std_msgs::Int32::ConstPtr& msg)
{
  std_msgs::Int32 var;
  var = *msg;
  sens_in = var.data;
}

void red_callback(const std_msgs::Float32::ConstPtr& msg)
{
  std_msgs::Float32 var;
  var = *msg;
  red_weight = var.data;
}

mat vector_cross(mat vector_3)
{
  mat la_cross;
  la_cross << 0 << -vector_3(2,0) << vector_3(1,0) << endr
                 << vector_3(2,0) << 0 << -vector_3(0,0) << endr
                 << -vector_3(1,0) << vector_3(0,0) << 0 << endr;
  return la_cross;
}

mat modal_transform(mat len_act, double len_ind)
{
  double A1, A2, A3, A4;
  A1 = pow(len_act(0,0),2)+pow(len_act(1,0),2)+pow(len_act(2,0),2)-len_act(0,0)*len_act(1,0)-len_act(0,0)*len_act(2,0)-len_act(1,0)*len_act(2,0);
  A2 = 2*len_act(0,0)-len_act(1,0)-len_act(2,0);
  A3 = len_act(1,0)-len_act(2,0);
  A4 = 3*L0+len_act(0,0)+len_act(1,0)+len_act(2,0);
  mat T;
  T.set_size(4,4);

  T(0,0) = 1 - (pow(A2,2)*pow(A1,4)*pow(len_ind,10))/(837019575*pow(radi,10)) + (pow(A2,2)*pow(A1,3)*pow(len_ind,8))/(4133430*pow(radi,8)) - (pow(A2,2)*pow(A1,2)*pow(len_ind,6))/(32805*pow(radi,6)) + (A1*pow(A2,2)*pow(len_ind,4))/(486*pow(radi,4)) - (pow(A2,2)*pow(len_ind,2))/(18*pow(radi,2));
  T(0,1) = (sqrt(3)*A2*A3*pow(A1,4)*pow(len_ind,10))/(837019575*pow(radi,10)) + (sqrt(3)*A2*A3*pow(A1,3)*pow(len_ind,8))/(4133430*pow(radi,8)) - (sqrt(3)*A2*A3*pow(A1,2)*pow(len_ind,6))/(32805*pow(radi,6)) + (sqrt(3)*A2*A3*A1*pow(len_ind,4))/(486*pow(radi,4)) - (sqrt(3)*A2*A3*pow(len_ind,2))/(18*pow(radi,2));
  T(0,2) = - (2*A2*pow(A1,4)*pow(len_ind,9))/(55801305*pow(radi,9)) + (4*A2*pow(A1,3)*pow(len_ind,7))/(688905*pow(radi,7)) - (2*A2*pow(A1,2)*pow(len_ind,5))/(3645*pow(radi,5)) + (2*A2*A1*pow(len_ind,3))/(81*pow(radi,3)) - (A2*len_ind)/(3*radi);
  T(1,0) = T(0,1);
  T(1,1) = 1 - (pow(A3,2)*pow(A1,4)*pow(len_ind,10))/(279006525*pow(radi,10)) + (pow(A3,2)*pow(A1,3)*pow(len_ind,8))/(1377810*pow(radi,8)) - (pow(A3,2)*pow(A1,2)*pow(len_ind,6))/(10935*pow(radi,6)) + (pow(A3,2)*A1*pow(len_ind,4))/(162*pow(radi,4)) - (pow(A3,2)*pow(len_ind,2))/(6*pow(radi,2));
  T(1,2) = - (2*sqrt(3)*A3*pow(A1,4)*pow(len_ind,9))/(55801305*pow(radi,9)) + (4*sqrt(3)*A3*pow(A1,3)*pow(len_ind,7))/(688905*pow(radi,7)) - (2*sqrt(3)*A3*pow(A1,2)*pow(len_ind,5))/(3645*pow(radi,5)) + (2*sqrt(3)*A3*A1*pow(len_ind,3))/(81*pow(radi,3)) - (sqrt(3)*A3*len_ind)/(3*radi);
  T(2,0) = -T(0,2);
  T(2,1) = -T(1,2);
  T(2,2) = 1 - (2*pow(len_ind,2)*A1)/(9*pow(radi,2)) + (2*pow(len_ind,4)*pow(A1,2))/(243*pow(radi,4)) - (4*pow(len_ind,6)*pow(A1,3))/(32805*pow(radi,6)) + (2*pow(len_ind,8)*pow(A1,4))/(2066715*pow(radi,8)) - (4*pow(len_ind,10)*pow(A1,5))/(837019575*pow(radi,10));
  T(0,3) = - (A2*pow(A1,4)*A4*pow(len_ind,10))/(837019575*pow(radi,9)) + (A2*pow(A1,3)*A4*pow(len_ind,8))/(4133430*pow(radi,7)) - (A2*pow(A1,2)*A4*pow(len_ind,6))/(32805*pow(radi,5)) + (A2*A1*A4*pow(len_ind,4))/(486*pow(radi,3)) - (A2*A4*pow(len_ind,2))/(18*radi);
  T(1,3) = - (sqrt(3)*A4*A3*pow(A1,4)*pow(len_ind,10))/(837019575*pow(radi,9)) + (sqrt(3)*A4*A3*pow(A1,3)*pow(len_ind,8))/(4133430*pow(radi,7)) - (sqrt(3)*A4*A3*pow(A1,2)*pow(len_ind,6))/(32805*pow(radi,5)) + (sqrt(3)*A4*A1*A3*pow(len_ind,4))/(486*pow(radi,3)) - (sqrt(3)*A4*A3*pow(len_ind,2))/(18*radi);
  T(2,3) = (2*pow(A1,4)*A4*pow(len_ind,9))/(55801305*pow(radi,8)) - (4*pow(A1,3)*A4*pow(len_ind,7))/(688905*pow(radi,6)) + (2*pow(A1,2)*A4*pow(len_ind,5))/(3645*pow(radi,4)) - (2*A1*A4*pow(len_ind,3))/(81*pow(radi,2)) + (A4*len_ind)/(3);
  T(3,0) = T(3,1) = T(3,2) = 0;
  T(3,3) = 1;

  return T;
}

mat timoshenko_transform(mat tendon, double press, double len_ind)
{
  double delta_l, tot_force;
  double phi_const;
  mat T, tot_moment;
  // mat stiff_matrix;
  T.set_size(4,4);
  // stiff_matrix.set_size(6,6);
  // stiff_matrix.zeros();
  // phi_const = (12*Young*inertia)/(shear*Area_w*pow(len_ind*L0,2));
  // stiff_matrix(0,0) = stiff_matrix(1,1) = (12*Young*inertia)/((1+phi_const)*pow(len_ind*L0,3));
  // stiff_matrix(2,2) = (Young*Area_w)/(len_ind*L0);
  // stiff_matrix(1,3) = stiff_matrix(3,1) = -(6*Young*inertia)/((1+phi_const)*pow(len_ind*L0,2));
  // stiff_matrix(0,4) = stiff_matrix(4,0) = (6*Young*inertia)/((1+phi_const)*pow(len_ind*L0,2));
  // stiff_matrix(3,3) = stiff_matrix(4,4) = (((4+phi_const))*Young*inertia)/((1+phi_const)*len_ind*L0);
  // stiff_matrix(5,5) = (shear*torsion_const)/(len_ind*L0);

  tot_moment.set_size(3,1);
  tot_moment.zeros();

  mat rad_mat;
  mat tendon_vect;
  tot_force = (press-pressure_0)*Area_w;
  for(unsigned char i=0; i<3; i++)
  {
    rad_mat << radi*cos(i*2*M_PI/3) << endr
               << radi*sin(i*2*M_PI/3) << endr
               << 0 << endr;
    tendon_vect << 0.0 << endr
                   << 0.0 << endr
                   << -tendon(i,0) << endr;
    tot_moment = tot_moment + vector_cross(rad_mat)*tendon_vect;
    // rad_mat.print("rad_mat:");
    // tendon_vect.print("tendon_vect:");

    tot_force = tot_force - tendon(i,0);
  }
  // tot_moment.print("moment:");
  // mat pose_angle, force_torque;
  // force_torque.set_size(3,1);
  // force_torque.zeros();
  // force_torque.insert_rows(3,tot_moment);
  // force_torque.print("force_torque:");
  // pose_angle = inv(stiff_matrix)*force_torque;
  // cout << inv(stiff_matrix) << endl;
  // pose_angle.print("pose_angle:");
  // len_ind*L0/(Young*Area_w)*press*Area_w;

  kappa = norm(tot_moment)/(Young*inertia);
  length_final = len_ind*L0*(1+tot_force/(Young*Area_w));
  phi = atan2(-tot_moment(0,0),tot_moment(1,0));
  // cout << "phi" << phi << endl;

  if(kappa!=0)
  {
    T(0,3) = cos(phi)/kappa*(1-cos(kappa*length_final));
    T(1,3) = sin(phi)/kappa*(1-cos(kappa*length_final));
    T(2,3) = sin(kappa*length_final)/kappa;
  }
  else
  {
    T(0,3) = 0;
    T(1,3) = 0;
    T(2,3) = length_final;
  }

  T(3,3) = 1.0;

  // tf::Matrix3x3 m_;
  // m_.setEulerYPR(pose_angle(3,0), pose_angle(4,0), pose_angle(5,0));
  // tf::Vector3 Vecx, Vecy, Vecz;
  // Vecx = m_.getRow(0);
  // Vecy = m_.getRow(1);
  // Vecz = m_.getRow(2);

  // T(0,0) = Vecx.getX(); T(0,1) = Vecx.getY(); T(0,2) = Vecx.getZ();
  // T(1,0) = Vecy.getX(); T(1,1) = Vecy.getY(); T(1,2) = Vecy.getZ();
  // T(2,0) = Vecz.getX(); T(2,1) = Vecz.getY(); T(2,2) = Vecz.getZ();
  T(0,0) = cos(phi)*cos(kappa*length_final);
  T(1,0) = sin(phi)*cos(kappa*length_final);
  T(2,0) = -sin(kappa*length_final);
  T(3,0) = T(3,1) = T(3,2) = T(2,1) = 0;
  T(0,1) = -sin(phi);
  T(1,1) = cos(phi);
  T(0,2) = cos(phi)*sin(kappa*length_final);
  T(1,2) = sin(phi)*sin(kappa*length_final);
  T(2,2) = cos(kappa*length_final);

  return T;
}

mat forward(unsigned char segment_ind, double len_ind)
{
  mat T_tot, T_base, transform, tension_act;
  double len_input;

  T_tot = eye<mat>(4,4);
  tf::Matrix3x3 m_;
  m_.setEulerYPR(0.0, 3.14/2.0, 0.0);
  tf::Vector3 Vecx, Vecy, Vecz;
  Vecx = m_.getRow(0);
  Vecy = m_.getRow(1);
  Vecz = m_.getRow(2);
  T_tot.set_size(4,4);

  T_tot(0,0) = Vecx.getX(); T_tot(0,1) = Vecx.getY(); T_tot(0,2) = Vecx.getZ();
  T_tot(1,0) = Vecy.getX(); T_tot(1,1) = Vecy.getY(); T_tot(1,2) = Vecy.getZ();
  T_tot(2,0) = Vecz.getX(); T_tot(2,1) = Vecz.getY(); T_tot(2,2) = Vecz.getZ();
  T_tot(3,0) = T_tot(3,1) = T_tot(3,2) = 0.0; T_tot(3,3) = 1.0;
  // T_tot.print("T_tot:");
  if(mobile_base == 1)
  {

    T_base << 1.0 << 0.0 << 0.0 << r_mob(0,0) << endr
          << 0.0 << 1.0 << 0.0 << r_mob(1,0) << endr
          << 0.0 << 0.0 << 1.0 << 0.0 << endr
          << 0.0 << 0.0 << 0.0 << 1.0 << endr;
    T_tot = T_base*T_tot;
  }
  tension_act.set_size(3,1);
  for(unsigned char i=0; i<segment_ind; i++)
  {
    if(i==segment_ind-1) len_input = len_ind;
    else len_input = 1;
    tension_act(0,0) = tension(i*3);
    tension_act(1,0) = tension(i*3+1);
    tension_act(2,0) = tension(i*3+2);
    // transform = modal_transform(len_act, len_input);
    transform = timoshenko_transform(tension_act, pressure, len_input);
    T_tot = T_tot*transform;
  }

  return T_tot;
}

mat forward_kinematics(unsigned char segment_ind, double len_ind)
{
  mat T_out, pose;
  T_out = forward(segment_ind, len_ind);
  pose << T_out(0,3) << endr
       << T_out(1,3) << endr
       << T_out(2,3) << endr;
  return pose;
}

mat RtoQuat(mat T_matrix)
{
  mat T_arma;

//   T_arma << 0,0 << 0,3 << 0,6 << endr
// 	 << 0,1 << 0,4 << 0,7 << endr
// 	 << 0,2 << 0,5 << 0,8 << endr;

//   double w = 0.5*pow((1+T_matrix[0,0]+T_matrix[0,4]+T_matrix[0,8]),0.5);
//   double x = (T_matrix[0,5]-T_matrix[0,7])/(4*w);
//   double y = (T_matrix[0,6]-T_matrix[0,2])/(4*w);
//   double z = (T_matrix[0,1]-T_matrix[0,3])/(4*w);

  double w, x, y, z;

  float trace = T_matrix(0,0) + T_matrix(1,1) + T_matrix(2,2); // I removed + 1.0f; see discussion with Ethan
  if( trace > 0 ) {// I changed M_EPSILON to 0
    float s = 0.5f / sqrtf(trace+ 1.0f);
    w = 0.25f / s;
    x = ( T_matrix(2,1) - T_matrix(1,2) ) * s;
    y = ( T_matrix(0,2) - T_matrix(2,0) ) * s;
    z = ( T_matrix(1,0) - T_matrix(0,1) ) * s;
  } else {
    if ( T_matrix(0,0) > T_matrix(1,1) && T_matrix(0,0) > T_matrix(2,2) ) {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(0,0) - T_matrix(1,1) - T_matrix(2,2));
      w = (T_matrix(2,1) - T_matrix(1,2) ) / s;
      x = 0.25f * s;
      y = (T_matrix(0,1) + T_matrix(1,0) ) / s;
      z = (T_matrix(0,2) + T_matrix(2,0) ) / s;
    } else if (T_matrix(1,1) > T_matrix(2,2)) {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(1,1) - T_matrix(0,0) - T_matrix(2,2));
      w = (T_matrix(0,2) - T_matrix(2,0) ) / s;
      x = (T_matrix(0,1) + T_matrix(1,0) ) / s;
      y = 0.25f * s;
      z = (T_matrix(1,2) + T_matrix(2,1) ) / s;
    } else {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(2,2) - T_matrix(0,0) - T_matrix(1,1) );
      w = (T_matrix(1,0) - T_matrix(0,1) ) / s;
      x = (T_matrix(0,2) + T_matrix(2,0) ) / s;
      y = (T_matrix(1,2) + T_matrix(2,1) ) / s;
      z = 0.25f * s;
    }
  }

  T_arma << x << endr
	 << y << endr
	 << z << endr
	 << w << endr;
  return T_arma;

}

geometry_msgs::Pose homo_to_pose(mat T)
{
  geometry_msgs::Pose pose;
  pose.position.x = T(0,3);
  pose.position.y = T(1,3);
  pose.position.z = T(2,3);
  mat get_quat = RtoQuat(T);
  pose.orientation.w = get_quat(3,0);
  pose.orientation.x = get_quat(0,0);
  pose.orientation.y = get_quat(1,0);
  pose.orientation.z = get_quat(2,0);

  return pose;
}

mat jacobian_update(unsigned char segment_ind, double len_ind)
{
  mat T_final, Jacobi, T_final_new, len_act_new;
  unsigned char starter;
  T_final = forward(segment_ind, len_ind);
  double delta_pose = 0.00001;
  double delta_tension = 0.001;
  double delta_pressure = 1000;
  len_act_new.set_size(3*segment_num+1, 1);
  len_act_new.zeros();

  // for(unsigned char i=0; i<3*segment_num; i++)
  // {
  //   len_act_new(i,0) = len_vec(i,0);
  // }
  if(mobile_base == 0) starter = 0;
  else starter = 2;
  Jacobi.set_size(3,3*segment_num+starter+1);
  Jacobi.zeros();

  if(mobile_base == 1)
  {
    for(unsigned char i=0; i<starter; i++)
    {
      // len_act_new(i,0) = len_act_new(i,0)+delta;
      r_mob(i,0) = r_mob(i,0)+delta_pose;
      T_final_new = forward(segment_ind, len_ind);
      for(unsigned char j=0; j<3; j++)
      {
          Jacobi(j,i) = (T_final_new(j,3)-T_final(j,3))/delta_pose;
      }
      r_mob(i,0) = r_mob(i,0)-delta_pose;
    }
  }
  // cout << "a" << endl;

  for(unsigned char i=starter; i<3*segment_num+starter; i++)
  {
    // len_act_new(i,0) = len_act_new(i,0)+delta;
    tension(i-starter,0) = tension(i-starter,0)+delta_tension;
    T_final_new = forward(segment_ind, len_ind);
    for(unsigned char j=0; j<3; j++)
    {
        Jacobi(j,i) = (T_final_new(j,3)-T_final(j,3))/delta_tension;
    }
    tension(i-starter,0) = tension(i-starter,0)-delta_tension;
  }
  // cout << "b" << endl;
  // For pressure
  pressure = pressure+delta_pressure;
  T_final_new = forward(segment_ind, len_ind);
  for(unsigned char j=0; j<3; j++)
  {
      Jacobi(j,3*segment_num+starter) = (T_final_new(j,3)-T_final(j,3))/delta_pressure;
  }
  pressure = pressure-delta_pressure;


  return Jacobi;
}

void broadcaster()
{
  static tf::TransformBroadcaster br;
  tf::Transform transform;
  mat base_matrix;
  geometry_msgs::Pose tip_pose;
  base_matrix = eye<mat>(4,4);
  int sensor_index = 0;
  for(unsigned char i=0; i<segment_num; i++)
  {
    for(float j=0; j<1; j=j+(1/(float)(sensor_num)))
    {
      base_matrix = forward(i+1,j+(1/(float)(sensor_num)));
      tip_pose = homo_to_pose(base_matrix);
      transform.setOrigin( tf::Vector3(tip_pose.position.x, tip_pose.position.y, tip_pose.position.z) );
      tf::Quaternion q(tip_pose.orientation.x, tip_pose.orientation.y, tip_pose.orientation.z, tip_pose.orientation.w);
      transform.setRotation(q);
      stringstream ss;
      ss << int(sensor_index);
      string tip_name = ss.str();
      br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "tip"+tip_name));
      sensor_index++;
    }
  }
  if(mobile_base==1)
  {
    transform.setOrigin( tf::Vector3(r_mob(0,0), r_mob(1,0), 0.0) );
    tf::Quaternion q(0.0, 0.0, 0.0, 1.0);
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "base"));
  }
  // goal broadcast
  static tf::TransformBroadcaster br_goal;
  transform.setOrigin( tf::Vector3(goal_pose(0,0), goal_pose(1,0), goal_pose(2,0)) );
  tf::Quaternion q(0.0, 0.0, 0.0, 1.0);
  transform.setRotation(q);
  br_goal.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "goal"));
}

void marker_pub()
{
  visualization_msgs::Marker marker;
  int index_marker=0;
  markerArray.markers.clear();
  mat base_matrix;
  marker.header.frame_id = "/world";
  marker.header.stamp = ros::Time::now();
  marker.ns = "soft_backbone";
	marker.action = marker.ADD;
	// marker.type = marker.SPHERE;
  marker.type = marker.CYLINDER;
  marker.scale.x = radi*2.0;
  marker.scale.y = radi*2.0;
  marker.scale.z = radi*2.0;
  base_matrix = eye<mat>(4,4);
  for(unsigned char i=0; i<segment_num; i++)
  {
    switch (i) {
      case 0:
        marker.color.a = 1.0;
     	  marker.color.r = 1.0;
     	  marker.color.g = 0.0;
     	  marker.color.b = 0.0;
      break;
      case 1:
        marker.color.a = 1.0;
     	  marker.color.r = 0.0;
     	  marker.color.g = 1.0;
     	  marker.color.b = 0.0;
      break;
      case 2:
        marker.color.a = 1.0;
     	  marker.color.r = 0.0;
     	  marker.color.g = 0.0;
     	  marker.color.b = 1.0;
      break;
    }
    for(float j=0; j<1; j=j+0.2)
    {
      // cout << index_marker << endl;
      base_matrix = forward(i+1,j);
      // base_matrix.print();
      marker.pose = homo_to_pose(base_matrix);
      // cout << marker.pose << endl;
      markerArray.markers.push_back(marker);
      markerArray.markers[index_marker].id = index_marker;
      index_marker++;
    }
  }
}

mat skew_symm(mat mat_in)
{
  mat mat_out;
  mat_out << 0.0 << -mat_in(2,0) << mat_in(1,0) << endr
          << mat_in(2,0) << 0.0 << -mat_in(0,0) << endr
          << -mat_in(1,0) << mat_in(0,0) << 0.0 << endr;
  return mat_out;
}

void inv_kin()
{
  mat J_tip;
  mat tip_force;
  // For magnetic
  // double KP = 1.0;
  // For APF
  // double KP = 0.2;
  // For Inflatable Arm
  double KP = 1.0;
  double KD = 4.0;
  mat tip;
  mat twist_tip_update;
  mat twist_body_update;
  mat sensor_jacobi, length_jacobi;
  int sens_segment;
  float sens_len_in;
  // double Young = 100000;
  mat eye_mat;
  mat obs_term;
  mat J_tilda;
  double cons = 1;
  mat smid_mat;
  double s_mid = 0.5*(tension_min+tension_max);
  smid_mat = s_mid*ones<mat>(3*segment_num,1);
  mat grad_final, red_term, final_term;
  double gain = 1;
  unsigned char starter;
  if(mobile_base == 0) starter = 0;
  else starter = 2;
  mat grad_mob, grad_pressure, final_term_2, dot_pressure_mat;

  //For speed controller
  // double magni_bound = 0.5;
  // For exp
  // double magni_bound = 0.2;
  double magni_bound = 0.05;
  // double speed_limit = 1.5;
  // FOr experiment
  // double speed_limit = 0.2;
  double speed_limit = 0.025;
  // double speed_limit = 3.5;
  // double KP_speed = 1.0;
  // double KP_speed = 5.0;
  // For exp
  // double KP_speed = 10.0;
  double KP_speed = 50.0;
  double KD_speed = 1.5;
  double KP_omega = 10.0;
  double KP_geom = KP_omega;
  mat rg, agent_cur, s_, vel_attr, agent_target, base_vector, omega_skew, R_1, R_2, R_mat, omega_so3, out, omega_3_bef, omega_3, Force_add, Force_combined;
  double magni, u, speed_mag, cos_theta, sin_theta, theta;
  double total_tension, pressure_target, length_target;

  if(flag_obs_data == 1)
  {
    sens_segment = 1+int(floor(sens_in /(sensor_num)));
    sens_len_in = 1/(float)(sensor_num)*(1+(sens_in % sensor_num));
    // cout << "a" << endl;
    // cout << floor(sens_in /(segment_num)) << endl;
    // cout << (1+(sens_in % segment_num)) << endl;
    // cout << sens_in << endl;
    // cout << sens_segment << endl;
    // cout << sens_len_in << endl;
    tip = forward_kinematics(segment_num, 1);
    // tip_force = -KP*(tip - goal_pose);
    // cout << flag_move << endl;
    if(flag_move == 1)
    {
      goal_active = goal_pose;
    }


    // tip_force.print("tip:");
    J_tip = jacobian_update(segment_num,1);

    sensor_jacobi = jacobian_update(sens_segment,sens_len_in);
    // twist_body = sensor_jacobi*dot_vector;
    J_tip.print("J_tip:");

    // sensor_jacobi.print("J_sens:");


    if(learning_on==0)
    {
      if(controller == 0)
      {
        tip_force = stiff.data*(-KP*(tip - goal_active) - KD*twist_tip);
      }
      else
      {
        // cout << "start" << endl;
        // Speed controller
        rg = goal_active - tip;
        // cout << "fin" << endl;
        // agent_cur = xdot_vec;
        if(mobile_base == 0)
        {
          agent_cur = J_tip*dot_vector;
        }
        else
        {
          agent_cur = J_tip*dot_vector;
        }
        // cout << "fin2" << endl;
        // agent_cur.print("agent_cur:");

        magni = norm(rg);

        if(magni>magni_bound)
        {
          // cout << "speed: " << norm(agent_cur) << endl;
          if(norm(agent_cur)<=0.05)
          {
            // cout << "minus" << endl;
            s_ = rg/norm(rg);
          }
          else
          {
            // cout << "plus" << endl;
            s_ = agent_cur/norm(agent_cur);
          }

          u = -KP_speed*(norm(agent_cur) - speed_limit);
          vel_attr = u*s_;
          // cout << "far " << endl;
        }

        else
        {
          vel_attr = KP*rg - KD*agent_cur;
          // cout << "close " << endl;
        }
        // cout << "fin2" << endl;
        // Try the geometric control for 3D
        if(magni>magni_bound)
        {
          // agent_cur = xdot_vec;
          if(mobile_base == 0)
          {
            agent_cur = J_tip*dot_vector;
          }
          else
          {
            agent_cur = J_tip*dot_vector;
          }

          // agent_cur << 1.0 << endr
          //           << 0.0 << endr
          //           << 0.0 << endr;
          speed_mag = norm(agent_cur);
          agent_target = rg;
          // agent_target << 0.0 << endr
          //              << 1.0 << endr
          //              << 0.0 << endr;
          // agent_cur.print("agent_cur:");
          // agent_target.print("agent_target");
          if(norm(agent_cur)!=0)
          {
            agent_cur = agent_cur/(norm(agent_cur));
            base_vector << 1.0 << endr
                        << 0.0 << endr
                        << 0.0 << endr;;
            omega_skew = skew_symm(skew_symm(base_vector)*agent_cur);
            // omega_skew.print("omega_skew1:");
            cos_theta = dot(agent_cur, base_vector);
            sin_theta = norm(skew_symm(base_vector)*agent_cur);
            if(cos_theta!=-1)
            {
              R_1 = eye<mat>(3,3) + omega_skew + omega_skew*omega_skew*1/(1+cos_theta);
            }
            else
            {
              R_1 << -1.0 << 0.0 << 0.0 << endr
                  << 0.0 << -1.0 << 0.0 << endr
                  << 0.0 << 0.0 << 1.0 << endr;
            }

            if(norm(agent_target)!=0)
            {
              agent_target = agent_target/(norm(agent_target));
            }
            base_vector << 1.0 << endr
                        << 0.0 << endr
                        << 0.0 << endr;
            omega_skew = skew_symm(skew_symm(base_vector)*agent_target);
            // omega_skew.print("omega_skew2:");
            cos_theta = dot(agent_target, base_vector);
            sin_theta = norm(skew_symm(base_vector)*agent_target);
            if(cos_theta!=-1)
            {
              R_2 = eye<mat>(3,3) + omega_skew + omega_skew*omega_skew*1/(1+cos_theta);
            }
            else
            {
              R_2 << -1.0 << 0.0 << 0.0 << endr
                  << 0.0 << -1.0 << 0.0 << endr
                  << 0.0 << 0.0 << 1.0 << endr;
            }

          }

          else
          {
            R_1 = eye<mat>(3,3);
            R_2 = eye<mat>(3,3);
          }
          // R_1.print("R_1:");
          // R_2.print("R_2:");
          // This R_mat is in frame R_2, so the omega needs to be transformed back to world frame
          R_mat = R_2.t()*R_1;
          // R_mat.print("R_mat");
          cos_theta = (0.5*((trace(R_mat))-1));

          if(cos_theta>0.9999999)
          {
            cos_theta = 0.9999999;
          }
          else if(cos_theta<-0.9999999)
          {
            cos_theta = -0.9999999;
          }

          theta = acos(cos_theta);
          // cout << "theta: " << theta << endl;
          if(sin(theta)!=0)
          {
            out = (theta/sin(theta))*(R_mat - R_mat.t());
          }
          else
          {
            out = (R_mat - R_mat.t());
          }

          omega_so3 = -KP_geom*stiff.data*out;
          omega_3_bef << omega_so3(2,1) << endr
                      << omega_so3(0,2) << endr
                      << omega_so3(1,0) << endr;
          omega_3 = R_2*omega_3_bef;
          // agent_cur = xdot_vec;
          if(mobile_base == 0)
          {
            agent_cur = J_tip*dot_vector;
          }
          else
          {
            agent_cur = J_tip*dot_vector;
          }
          // agent_cur << 1.0 << endr
          //           << 0.0 << endr
          //           << 0.0 << endr;
          Force_add = skew_symm(omega_3)*agent_cur;
        }
        else
        {
          Force_add.zeros(3,1);
        }

        tip_force = Force_add + vel_attr;
        // Force_combined = vel_attr;
      }

      // tip_force.print("tip:");
      twist_tip_update = twist_tip + (tip_force + tip_field)*dt;
      // twist_tip_update = twist_tip + (tip_force)*dt;

      twist_body_update = twist_body + body_field*dt;


      //
      // // Obstacle redndanty 2nd priority
      // obs_term = red_weight*pinv(sensor_jacobi*(eye<mat>(3*segment_num+starter+1,3*segment_num+starter+1)-pinv(J_tip)*J_tip))*(twist_body_update - sensor_jacobi*(pinv(J_tip)*twist_tip_update));
      // // cout << "red_weight: " << red_weight << endl;
      // mat test_mat, test_vector, pinv_test;
      // test_mat = (sensor_jacobi*(eye<mat>(3*segment_num+starter+1,3*segment_num+starter+1)-pinv(J_tip)*J_tip));
      // pinv_test = pinv(test_mat);
      // // test_vector = (twist_body_update - sensor_jacobi*(pinv(J_tip)*twist_tip_update));
      // // twist_body_update.print("tw_body:");
      // pinv(J_tip).print("inv_J_tip:");
      // pinv(sensor_jacobi).print("inv_sensor_jacobi:");
      // test_mat.print("test:");
      // pinv_test.print("pinv:");
      // // test_vector.print("vect:");
      // obs_term.print("obs_term:");
      // // not using redundancy
      obs_term = red_weight*pinv(sensor_jacobi)*twist_body_update;
      // twist_body_update.print("tw_body:");
      // obs_term.print("obs_term2:");
      // obs_term.print("obs_term:");
      // // grad_final.print("grad:");
      // // smid_mat.print("mid:");
      // if(mobile_base == 1)
      // {
      //   grad_mob = -1000*cons*(r_mob - tip.rows(0,1)) - cons*r_dot_mob;
      //   grad_mob = r_dot_mob + grad_mob*dt;
      //   // grad_final.insert_rows(0,r_dot_mob);
      //   grad_final.insert_rows(0,grad_mob);
      //   // grad_final.print("grad:");
      // }
      //
      // // if(red_weight>0)
      // {
      //   // Length redundancy 3rd priority combined with obstacle avoidance
      //   // grad_final.print("grad=");
        // J_tilda = sensor_jacobi*(eye<mat>(3*segment_num+starter,3*segment_num+starter)-pinv(J_tip)*J_tip);
        // red_term = (eye<mat>(3*segment_num+starter,3*segment_num+starter)-pinv(J_tip)*J_tip)*(eye<mat>(3*segment_num+starter,3*segment_num+starter)-pinv(J_tilda)*J_tilda)*grad_final;
        // final_term = gain*red_term;
      //   // final_term.print("final=");
      // }
      // mat final_term_2;
      // // else
      // {

      //   // final_term = gain*((eye<mat>(3*segment_num+starter,3*segment_num+starter)-pinv(J_tip)*J_tip))*(grad_final);

      //   // final_term.print("final=");
      //
      // }
      //

      // grad_final(i,0) = -60*cons*(tension(i,0) - smid_mat)/pow((s_mid-tension_min),2) - 10*cons*dot_tension(i,0);
      // Lenght redundancy 2nd priority

      grad_final.set_size(3*segment_num,1);
      grad_final.zeros();
      // grad_final.print("grad:");
      // tension.print("tens:");
      // dot_tension.print("dot:");
      for(unsigned char i=0; i<3*segment_num; i++)
      {
        if(tension(i,0)<s_mid)
        {
          grad_final(i,0) = -60*cons*(tension(i,0) - s_mid)/pow((s_mid-tension_min),2) - 10*cons*dot_tension(i,0);
        }
        else
        {
          grad_final(i,0) = - 10*cons*dot_tension(i,0);
        }
      }
      // cout << "tes2" << endl;
      grad_final = dot_tension + grad_final*dt;
      // grad_final.zeros();
      total_tension = s_mid*3;
      // for(unsigned char i=0; i<3; i++)
      // {
      //   total_tension = total_tension + tension(i,0);
      // }


      // length_target = 2*norm(goal_active);
      mat base_3D;
      mat grad_base, base_target;
      mat r_bg, r_mob3;
      mat r_goal1;
      if(mobile_base == 1)
      {

        // base_3D.set_size(3,1);
        // base_3D.zeros();
        r_mob3 << r_mob(0,0) << endr
               << r_mob(1,0) << endr
               << 0.0 << endr;
        r_goal1 << goal_active(0,0) << endr
                << goal_active(1,0) << endr
                << 0.0 << endr;
        r_bg = r_mob3 - r_goal1;

        base_3D = L0*r_bg/norm(r_bg) + r_goal1;
        // base_3D.zeros()

        base_target << base_3D(0,0) << endr
                    << base_3D(1,0) << endr;
        // base_target.print("base_target:");
        // r_bg.print("r_bg:");
      }

      mat length_frame;
      double const_length;
      if(mobile_base == 1)
      {
        length_frame = goal_active - base_3D;
        const_length = 1.0;
      }
      else
      {
        length_frame = goal_active;
        const_length = 1.2;
      }



      if(kappa!=0)
      {
        length_target = (2/kappa)*asin(kappa*const_length*norm(length_frame)/2);
      }
      else
      {
        length_target = const_length*norm(length_frame);
      }

      pressure_target = pressure_0 + total_tension/Area_w + (length_target-L0)/L0*Young;
      double pressure_grad;
      // if(pressure<pressure_target)
      {
        pressure_grad = -60*cons*(pressure - pressure_target) - 10*cons*dot_pressure;
      }
      // else
      // {
        // pressure_grad = 0.0;
      // }

      // For mobile base

      if(mobile_base == 1)
      {
        // grad_base = r_dot_mob;
        grad_base.set_size(2,1);
        // grad_base.zeros();
        // base_target << 0.0 << endr
        //             << 0.0 << endr;
        grad_base(0,0) = -10*cons*(r_mob(0,0) - base_target(0,0)) - 1*cons*r_dot_mob(0,0);
        grad_base(1,0) = -10000*cons*(r_mob(1,0) - 0.0) - 1*cons*r_dot_mob(1,0);
        // grad_base = -1*cons*(r_mob - base_target) - 1*cons*r_dot_mob;
        grad_base = r_dot_mob + grad_base*dt;
        // grad_base.print("grad_base:");
        // grad_base.zeros();
      }

      grad_pressure << pressure_grad << endr;
      dot_pressure_mat << dot_pressure << endr;
      grad_pressure = dot_pressure_mat + grad_pressure*dt;
      grad_final.insert_rows(3,grad_pressure);
      if(mobile_base == 1)
      {
        grad_final.insert_rows(0, grad_base);

      }
      // grad_final.print("grad_final:");
      // grad_final.zeros();
      // J_tilda = sensor_jacobi*(eye<mat>(3*segment_num+starter+1,3*segment_num+starter+1)-pinv(J_tip)*J_tip);
      // red_term = (eye<mat>(3*segment_num+starter+1,3*segment_num+starter+1)-pinv(J_tip)*J_tip)*(eye<mat>(3*segment_num+starter+1,3*segment_num+starter+1)-pinv(J_tilda)*J_tilda)*grad_final;
      // final_term = red_weight*gain*red_term;
      // final_term.print("final_term:");

      final_term_2 = gain*((eye<mat>(3*segment_num+starter+1,3*segment_num+starter+1)-pinv(J_tip)*J_tip))*(grad_final);
      // cout << "s_mid" << s_mi  final_term.print("final_term:");d << endl;
      // smid_mat.print("smid_mat");
      // cout << "press_tar" << pressure_target << endl;
      // cout << "len_tar" << length_target << endl;
      // final_term_2.print("final_term_2");

      // dot_vector = pinv(J_tip)*twist_tip_update + obs_term + final_term;
      dot_vector = pinv(J_tip)*twist_tip_update + final_term_2;// + final_term + (1-red_weight)*final_term_2;


    }
    else
    {

      // int index_inc;
      //
      // for(index_inc=0; index_inc<(3*segment_num+starter+1 - 3); index_inc++)
      // {
      //   if((counting % int(pow(inc_num,index_inc))) == 0)
      //   {
      //     cout << dot_vector(index_inc+2,0) << endl;
      //     cout << double(inc_num*increment[index_inc+2]) << endl;
      //     if(double(dot_vector(index_inc+2,0))>=double(inc_num*increment[index_inc+2])/2.0)
      //     {
      //       cout << "a" << endl;
      //       dot_vector(index_inc+2,0) = -increment[index_inc+2]*double(inc_num)/2.0;;
      //     }
      //     else
      //     {
      //       cout << "b" << endl;
      //       dot_vector(index_inc+2,0) = dot_vector(index_inc+2,0) + increment[index_inc+2];
      //     }
      //
      //   }
      //
      //   // cout << "modulo: " << int(pow(inc_num,index_inc)) << endl;
      //   // cout << "tes index: " << (counting % int(pow(inc_num,index_inc))) << endl;
      // }
      // // cout << "counting" << counting << endl;

      ros::Time current_time = ros::Time::now();
      double dur_time = (double)((double)(current_time.sec) + (double)(current_time.nsec)*pow(10,-9) - ((double)(begin.sec) + (double)(begin.nsec)*pow(10,-9)));
      if(mobile_base == 1)
      {
        dot_vector(2,0) = tension_max/2.0*sin(dur_time);
        dot_vector(3,0) = tension_max/2.0*sin(dur_time+2*M_PI/3);
        dot_vector(4,0) = tension_max/2.0*sin(dur_time+4*M_PI/3);
      }
      else
      {
        dot_vector(0,0) = tension_max/2.0*sin(dur_time);
        dot_vector(1,0) = tension_max/2.0*sin(dur_time+2*M_PI/3);
        dot_vector(2,0) = tension_max/2.0*sin(dur_time+4*M_PI/3);
      }

      dot_vector.print("dot:");
      counting++;

    }
    if(mobile_base == 1)
    {
      r_dot_mob = dot_vector.rows(0,1);
      dot_tension = dot_vector.rows(2,4);
      dot_pressure = dot_vector(5,0);
    }
    else
    {
      dot_tension = dot_vector.rows(0,2);
      dot_pressure = dot_vector(3,0);
    }
  }

  tension = tension + dot_tension*dt;
  pressure = pressure + dot_pressure*dt;
  if(mobile_base == 1) r_mob = r_mob + r_dot_mob*dt;

  // // Limit
  // for(unsigned char i=0; i<3*segment_num; i++)
  // {
  //   if(tension(i,0)<tension_min) tension(i,0) = tension_min;
  //   else if(tension(i,0)>tension_max) tension(i,0) = tension_max;
  // }


  // double work = dot(tip_field_dum, twist_tip);
  // if(work > 0.001) cout << work << endl;

  if(flag_obs_data == 1)
  {

    if(mobile_base == 0)
    {
      twist_tip = J_tip*dot_vector;
      twist_body = sensor_jacobi*dot_vector;
    }
    else
    {
      twist_tip = J_tip*dot_vector;
      twist_body = sensor_jacobi*dot_vector;
    }
    if(obs_active == 1)
    {
      flag_obs_data = 0;
    }


    twist_sent.vector.x = twist_tip(0,0);
    twist_sent.vector.y = twist_tip(1,0);
    twist_sent.vector.z = twist_tip(2,0);
    twist_sent.header.stamp = ros::Time::now();
    time_sent = twist_sent.header.stamp;

    twist_body_sent.vector.x = twist_body(0,0);
    twist_body_sent.vector.y = twist_body(1,0);
    twist_body_sent.vector.z = twist_body(2,0);

  }

}

void check_task()
{
  mat tip, dist_to_goal;
  tip = forward_kinematics(segment_num, 1);
  dist_to_goal = goal_pose - tip;
  if(norm(dist_to_goal)<0.001)
  {
    if(task_tracking==0) time_record = ros::Time::now();
    task_tracking = 1;

  }
  else
  {
    task_tracking = 0;
  }
  if(task_tracking == 1)
  {
    time_task = ros::Time::now();
    if((time_task.sec - time_record.sec)>dur_task)
    {
      if(task_completed<(total_task-1))
      {
        task_completed++;
      }
    }
  }

  for(unsigned char i=0; i<3; i++)
  {
    goal_pose(i,0) = goal_task[task_completed](i,0);
  }
  cout << "task_completed: " << task_completed << endl;
  cout << "dur: " << time_task.sec - time_record.sec << endl;
  cout << norm(dist_to_goal) << endl;
  cout << task_tracking << endl;
}

void transform_to_sent()
{
  joint_sent.header.stamp = ros::Time::now();
  joint_sent.position.clear();
  joint_sent.velocity.clear();
  pressure_sent.data.clear();
  if(mobile_base == 1)
  {
    for(unsigned char i = 0; i<2; i++)
    {
      joint_sent.position.push_back(r_mob(i,0));
      joint_sent.velocity.push_back(r_dot_mob(i,0));
    }
  }
  for(unsigned char i = 0; i<3*segment_num; i++)
  {
    joint_sent.position.push_back(tension(i,0));
    joint_sent.velocity.push_back(dot_tension(i,0));
  }
  // For pressure
  joint_sent.position.push_back(pressure);
  joint_sent.velocity.push_back(dot_pressure);

  tip_sent = homo_to_pose(forward(segment_num,1));

  goal_sent.position.x = goal_pose(0,0);
  goal_sent.position.y = goal_pose(1,0);
  goal_sent.position.z = goal_pose(2,0);
  goal_sent.orientation.x = 0.0;
  goal_sent.orientation.y = 0.0;
  goal_sent.orientation.z = 0.0;
  goal_sent.orientation.w = 1.0;
}

int
main(int argc, char** argv)
{
    ros::init(argc,argv, "modal_kinematics");
    ros::NodeHandle n;

    n.param("segment_num", segment_num, segment_num);
    // segment_num = 1;
    n.param("sensor_num", sensor_num, sensor_num);
    // sensor_num = 3;
    n.param("mobile_base", mobile_base, mobile_base);
    n.param("controller", controller, controller);
    n.param("task_on", task_on, task_on);
    n.param("learning_on", learning_on, learning_on);
    n.param("obs_active", obs_active, obs_active);

    ros::Publisher pub_joint = n.advertise<sensor_msgs::JointState>("/agent/joint",10);
    // ros::Publisher pub_pressure = n.advertise<std_msgs::Float64MultiArray>("/agent/pressure",10);
    ros::Publisher pub_marker = n.advertise<visualization_msgs::MarkerArray>("/soft_marker",10);
    ros::Publisher pub_goal = n.advertise<geometry_msgs::Pose>("/agent/tip/desired",10);
    ros::Publisher pub_tip = n.advertise<geometry_msgs::Pose>("/agent/tip/current",10);
    ros::Publisher pub_twist = n.advertise<geometry_msgs::Vector3Stamped>("/agent/tip/twist",10);
    ros::Publisher pub_body_twist = n.advertise<geometry_msgs::Vector3Stamped>("/agent/body/twist",10);
    ros::Subscriber goal_sub = n.subscribe("/joy", 10, get_joy);
    ros::Subscriber sub_body = n.subscribe("/force_body_obs", 10, get_body);
    ros::Subscriber sub_tip = n.subscribe("/force_obs", 10, get_force_tip);
    ros::Subscriber sub_tip_stamp = n.subscribe("/force_obs_stamped", 10, get_force_tip_stamp);
    ros::Subscriber sub_sens = n.subscribe("/agent/sensor_index", 10, get_sensin);
    ros::Subscriber sub_stiff = n.subscribe("/stiffness_scalar", 10, get_stiffness);
    ros::Subscriber sub_redund = n.subscribe("/redundancy_weight", 10, red_callback);

    tension.set_size(3*segment_num, 1);
    tension.zeros();
    dot_tension.set_size(3*segment_num, 1);
    dot_tension.zeros();
    pressure = pressure_0;
    dot_pressure = 0;

    goal_pose.set_size(3,1);
    goal_pose(0,0) = L0*segment_num; goal_pose(1,0) = 0.0; goal_pose(2,0) = 0.0;
    goal_active = goal_pose;

    tip_field_dum.set_size(3,1);
    tip_field_dum.zeros();
    tip_field.set_size(3,1);
    tip_field.zeros();
    body_field.set_size(3,1);
    body_field.zeros();
    twist_tip.set_size(3,1);
    twist_tip.zeros();
    twist_body.set_size(3,1);
    twist_body.zeros();
    r_mob.set_size(2,1);
    r_mob.zeros();
    r_dot_mob.set_size(2,1);
    r_dot_mob.zeros();
    p_mat.set_size(3*segment_num,1);
    if(mobile_base==1)
    {
      dot_vector.set_size(3*segment_num+2+1,1);
      // index_learning.data.clear();
      // for(unsigned char i = 0; i<3*segment_num+2+1; i++)
      // {
      //   index_learning.data.push_back(0);
      // }
    }
    else
    {
      dot_vector.set_size(3*segment_num+1,1);
      // index_learning.data.clear();
      // for(unsigned char i = 0; i<3*segment_num+1; i++)
      // {
      //   index_learning.data.push_back(0);
      // }
    }
    if(learning_on==0)
    {
      dot_vector.zeros();
    }
    else
    {
      dot_vector.zeros();
      // for(unsigned char i =0; i<3; i++)
      // {
      //   dot_vector(2+i,0) = -increment[2+i]*double(inc_num)/2.0;
      // }

    }

    // Task definition
    // goal_task[0] << 0.09 << endr
    //              << 0.0 << endr
    //              << 0.0 << endr;
    if(task_on == 1)
    {
      flag_move = 1;
      flag_obs_active = 1;
      goal_task[0] << 0.1 << endr
                   << 0.02 << endr
                   << 0.05 << endr;
      goal_task[1] << 0.3 << endr
                   << 0.0 << endr
                   << 0.02 << endr;
      goal_task[2] << 0.45 << endr
                   << 0.02 << endr
                   << -0.02 << endr;
      goal_task[3] << 0.55 << endr
                   << -0.02 << endr
                   << -0.03 << endr;

      goal_task[4] << 0.73 << endr
                   << 0.02 << endr
                   << 0.05 << endr;

    }
    else
    {
      flag_move = 0; flag_obs_active = 0;
    }


    float freq = 50.0;
    dt = 1.0/freq;
    ros::Rate loop_rate(freq);
    flag_obs_data = 1;
    stiff.data = 1;
    counting = 1;
    begin = ros::Time::now();
    while(ros::ok())
    {
      // cout << "a" << endl;
      marker_pub();
      broadcaster();
      // cout << "b" << endl;
      inv_kin();
      if(task_on == 1)
      {
        check_task();
      }
      transform_to_sent();
      pub_marker.publish(markerArray);
      pub_joint.publish(joint_sent);
      pub_goal.publish(goal_sent);
      pub_tip.publish(tip_sent);
      pub_twist.publish(twist_sent);
      pub_body_twist.publish(twist_body_sent);
      // pub_pressure.publish(pressure_sent);
      ros::spinOnce();
      loop_rate.sleep();
    }
}
