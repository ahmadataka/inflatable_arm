#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


from numpy import *
from matplotlib import pyplot as plt
from lwpr import LWPR
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import Vector3
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
from sklearn import preprocessing
import os

class record_data(object):
  def __init__(self):
      rospy.init_node('learning_lwpr')
      self.segment_num = rospy.get_param('/segment_num')
      mobile = rospy.get_param('/mobile_base')
      self.model_load = rospy.get_param('/model_load')
      if(mobile==1):
          self.starting = 2
      else:
          self.starting = 0
      self.mse = 0.0
      rec_tip = rospy.Subscriber('/agent/tip/current', Pose, self.tip_callback)
      rec_joint = rospy.Subscriber('/agent/joint', JointState, self.joint_callback)
      rec_twist = rospy.Subscriber('/agent/tip/twist', Vector3Stamped, self.twist_callback)
      send_pub = rospy.Publisher('/joint_dot', Float64MultiArray, queue_size = 10)
      self.tip_pose = Pose()
      self.l_space = Float64MultiArray()
      self.l_dot_space = Float64MultiArray()
      for i in range(0,3*self.segment_num+self.starting+1):
          self.l_space.data.append(0)
          self.l_dot_space.data.append(0)

      self.twist = Vector3()
      self.index_count = 0.0
      self.frequency = 50.0
      r = rospy.Rate(self.frequency)

      self.delta_pose = 0.00001
      self.delta_tension = 0.001
      self.delta_pressure = 1000
      self.qdelta = [self.delta_pose, self.delta_pose, self.delta_tension, self.delta_tension, self.delta_tension, self.delta_pressure]

      self.initialize_model()
      while not rospy.is_shutdown():
          self.learning()
          self.jacobian_update()
          # send_pub.publish(self.send)
          r.sleep()

  def initialize_model(self):
      # initialize the LWPR model
      self.size_u = 3*self.segment_num+self.starting+1
      # self.size_u = 3
      self.size_p = 3
      self.size_input = self.size_u*2+self.size_p
      self.size_output = self.size_p
      # self.size_input = 9
      # self.size_output = self.size_p
      if(self.model_load == 0):
          self.model = LWPR(self.size_input, self.size_output)
          self.model.init_D = 20 * eye(self.size_input)
          self.model.update_D = True
          self.model.init_alpha = 40 * eye(self.size_input)
          self.model.meta = False
      else:
          os.chdir("/home/ahmadataka/catkin_ws/src/inflatable_arm/learning")
          self.model = LWPR("testing.xml")

  def save_model(self):
    os.chdir("/home/ahmadataka/catkin_ws/src/inflatable_arm/learning")
    self.model.write_XML("testing.xml")
    self.model2 = LWPR("testing.xml")

    # print self.model
    # print self.model2
    print "print"
    # print os.getcwd()

  def learning(self):
      # train the model

      InputVector = []
      for i in range(0,self.size_u):
          # self.l_space.data[i] = random.random()
          InputVector.append(self.l_space.data[i])

      for i in range(0,self.size_u):
          # self.l_dot_space.data[i] = random.random()
          InputVector.append(self.l_dot_space.data[i])


      # self.tip_pose.position.x = random.random()
      # self.tip_pose.position.y = random.random()
      # self.tip_pose.position.z = random.random()
      InputVector.append(self.tip_pose.position.x)
      InputVector.append(self.tip_pose.position.y)
      InputVector.append(self.tip_pose.position.z)

      OutputVector = []
      # self.twist.x = cos(self.l_space.data[0]*self.l_dot_space.data[0])/(sin(self.tip_pose.position.x)+1)
      # self.twist.y = cos(self.l_space.data[1]*self.l_dot_space.data[1])/(sin(self.tip_pose.position.y)+1)
      # self.twist.z = cos(self.l_space.data[2]*self.l_dot_space.data[2])/(sin(self.tip_pose.position.z)+1)
      OutputVector.append(self.twist.x)
      OutputVector.append(self.twist.y)
      OutputVector.append(self.twist.z)

      PreScaleVector = asarray(InputVector).reshape(-1,1)
      standardscaler=preprocessing.StandardScaler().fit(PreScaleVector)
      NewInputVector = standardscaler.transform(PreScaleVector)
      # print "Input"
      # print InputVector
      # print "New Input"
      # print NewInputVector

      # yp = self.model.update(NewInputVector, asarray(OutputVector))
      yp = self.model.update(asarray(InputVector), asarray(OutputVector))

      self.mse = self.mse + linalg.norm(yp - OutputVector)**2
      self.index_count = self.index_count + 1.0
      nmse = self.mse/(self.index_count)*10000
      if(nmse<2.5):
          self.save_model()
      print nmse
      print "Out"
      print OutputVector
      print "Est"
      print yp
      #
      # # test the model with unseen data
      # Ntest = 500
      # Xtest = linspace(0, 10, Ntest)
      #
      # Ytest = zeros((Ntest,1))
      # Conf = zeros((Ntest,1))
      #
      # for k in range(500):
      #     Ytest[k,:], Conf[k,:] = model.predict_conf(array([Xtest[k]]))
      #
      # plt.plot(Xtr, Ytr, 'r.')
      #
      # plt.plot(Xtest,Ytest,'b-')
      # plt.plot(Xtest,Ytest+Conf,'c-', linewidth=2)
      # plt.plot(Xtest,Ytest-Conf,'c-', linewidth=2)
      #
      # plt.show()

  def tip_callback(self, msg):
    self.tip_pose.position.x = msg.position.x
    self.tip_pose.position.y = msg.position.y
    self.tip_pose.position.z = msg.position.z
    self.tip_pose.orientation.w = msg.orientation.w
    self.tip_pose.orientation.x = msg.orientation.x
    self.tip_pose.orientation.y = msg.orientation.y
    self.tip_pose.orientation.z = msg.orientation.z

  def joint_callback(self, msg):
    self.l_space.data = []
    self.l_dot_space.data = []
    for i in range(0,3*self.segment_num+self.starting+1):
        self.l_space.data.append(msg.position[i])
        self.l_dot_space.data.append(msg.velocity[i])

  def twist_callback(self, msg):
    self.twist.x = msg.vector.x
    self.twist.y = msg.vector.y
    self.twist.z = msg.vector.z

  def testfunc(self, x):
    return 10*sin(7.8*log(1+x)) / (1 + 0.1*x**2)

  def jacobian_update(self):
      jacobi = zeros((3,self.size_u))
      # print jacobi
      for index_jac in range(0,self.size_u):
          InputVector = []
          for i in range(0,self.size_u):
              InputVector.append(self.l_space.data[i])

          for i in range(0,self.size_u):
              if(i==index_jac):
                  InputVector.append(self.qdelta[index_jac]*self.frequency)
              else:
                  InputVector.append(0.0)

          InputVector.append(self.tip_pose.position.x)
          InputVector.append(self.tip_pose.position.y)
          InputVector.append(self.tip_pose.position.z)

          OutputVector = []
          OutputVector.append(self.twist.x)
          OutputVector.append(self.twist.y)
          OutputVector.append(self.twist.z)

          yp = self.model.update(asarray(InputVector), asarray(OutputVector))
          # print yp
          for i in range(0,3):
              jacobi[i,index_jac] = yp[i]/(self.frequency*self.qdelta[index_jac])
      print "jac_est"
      print jacobi

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
