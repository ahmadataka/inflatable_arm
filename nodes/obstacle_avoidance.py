#!/usr/bin/env python
import roslib
roslib.load_manifest('inflatable_arm')
import rospy
import math
from sympy import *
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Vector3Stamped
from std_msgs.msg import String
from std_msgs.msg import Int32
from std_msgs.msg import Float32
from geometry_msgs.msg import Twist


def find_magnitude(input1, input2, input3):
    magnitude = math.sqrt(input1 ** 2 + input2 ** 2 + input3 ** 2)
    return magnitude

flag_speed = Int32()
sp = Twist()
dist_to_goal = Vector3()
dist_to_obs = Vector3()
current_obs = Vector3()
central_obs = Vector3()
central_obs_real = Vector3()
init = Float32()
def speed_input(variable):
  sp.linear.x = variable.linear.x
  sp.linear.y = variable.linear.y
  sp.linear.z = variable.linear.z
  flag_speed.data = 1

def get_dist(var):
  dist_to_goal.x = var.x
  dist_to_goal.y = var.y
  dist_to_goal.z = var.z

def get_distobs(var):
  dist_to_obs.x = var.x
  dist_to_obs.y = var.y
  dist_to_obs.z = var.z

def get_curobs(var):
  current_obs.x = var.x
  current_obs.y = var.y
  current_obs.z = var.z

def get_obscentral(var):
  central_obs.x = var.x
  central_obs.y = var.y
  central_obs.z = var.z

def get_obscentral_real(var):
  central_obs_real.x = var.x
  central_obs_real.y = var.y
  central_obs_real.z = var.z

def get_init(var):
  init.data = var.data

if __name__ == '__main__':
    KP = 0.5
    rospy.init_node('ataka_collision')
    turtle_name = rospy.get_param('~robot')
    robot_type = rospy.get_param('~robot_type')
    field = rospy.get_param('/field')
    field_add = rospy.get_param('/field_add')
    print(field)
    if(field==0):
        bound = 1.5
        bound = 3.0
        mass_const = 0.5
    else:
        if(robot_type==3):
            bound = 1.5
            # bound = 0.75
        else:
            bound = 0.75
        mass_const = 1.0
    maks = 0
    bound_add = 2.0
    # bound_add = 1.0
    #print(bound)
    sp.linear.x = 0.0
    sp.linear.y = 0.0
    turtle_vel = rospy.Publisher('%s/collision_avoidance' %turtle_name, Vector3, queue_size = 10)
    turtle_vel_stamped = rospy.Publisher('%s/collision_avoidance_stamped' %turtle_name, Vector3Stamped, queue_size = 10)
    turtle_omega = rospy.Publisher('%s/omega_avoidance' %turtle_name, Vector3, queue_size = 10)
    obstacle_current = rospy.Publisher('obstacle_current', Vector3, queue_size = 10)
    agent_current = rospy.Publisher('agent_current', Vector3, queue_size = 10)
    flag_obs_pub = rospy.Publisher('flag_obstacle', Int32, queue_size = 10)
    sub_speed = rospy.Subscriber('%s/twist' %turtle_name, Twist, speed_input)
    sub_dist = rospy.Subscriber('%s/dist_to_goal' %turtle_name, Vector3, get_dist)
    sub_distobs = rospy.Subscriber('%s/dist_to_obs' %turtle_name, Vector3, get_distobs)
    sub_current = rospy.Subscriber('current_obs_global', Vector3, get_curobs)
    sub_central = rospy.Subscriber('obs_central', Vector3, get_obscentral)
    sub_init = rospy.Subscriber('%s/initial_to_goal' %turtle_name, Float32, get_init)
    sub_central_real = rospy.Subscriber('obs_central_real', Vector3, get_obscentral_real)
    vel_msg = Vector3()
    omega_msg = Vector3()
    vel_obs = Vector3()
    vel_obs_add = Vector3()
    o_cur = Vector3()
    ag_cur = Vector3()
    # IROS_RAL_2018
    # bound = 3.5
    # constant2 = 50.0

    # Normal
    # constant2 = 10

    constant2 = 10
    constant = 5
    constant_add = 20
    beta = 2
    flag_obs = Int32()
    flag_goal = 0

    rate = rospy.Rate(100.0)
    dist_min = 100.0
    while not rospy.is_shutdown():
      vel_msg.x = 0;
      vel_msg.y = 0;
      vel_msg.z = 0;
      omega_msg.z = 0.0
      dummy_obs = 0
      if(robot_type==1):
          dist_const = 0.2
      else:
          dist_const = 0.0
      distance = find_magnitude(dist_to_obs.x, dist_to_obs.y, dist_to_obs.z) - dist_const

    #   print distance
      speed = find_magnitude(sp.linear.x, sp.linear.y, sp.linear.z)
    #   print(speed)
      if(field == 0):
    	if distance > bound :
    	  vel_obs.x = 0
    	  vel_obs.y = 0
          vel_obs.z = 0
    	else :
    	  #print('Obstacle Turtle %d\n', i+2)
    	  if distance !=0 :
    	    vel_obs.y = -constant*(1/distance - 1/bound)*(dist_to_obs.y)/(distance ** 3)
    	    vel_obs.x = -constant*(1/distance - 1/bound)*(dist_to_obs.x)/(distance ** 3)
            vel_obs.z = -constant*(1/distance - 1/bound)*(dist_to_obs.z)/(distance ** 3)
    	  else :
    	    print("Colliding...\n")
      elif(field == 1):
    	# Current generation on the obstacle
    	#((current_obs.x**2 + current_obs.y**2 + current_obs.z**2) < 1.0) and
    	if((dist_to_obs.x!=0) and (dist_to_obs.y!=0) and (dist_to_obs.z!=0)):
    	  obs_cur = Matrix([[current_obs.x], [current_obs.y], [current_obs.z]])

    	  if distance > 2*bound :
    	    vel_obs.x = 0
    	    vel_obs.y = 0
            vel_obs.z = 0
    	  else :
    	    # print("obs")
    	    dummy_obs = dummy_obs + 1
    	    # new magnetic-based potential
    	    # Current generation on the robot
    	    agent_cur = Matrix([[dist_to_obs.x], [dist_to_obs.y], [dist_to_obs.z]])
    	    agent_mag = find_magnitude(dist_to_obs.x, dist_to_obs.y, dist_to_obs.z)

    	    agent_cur = agent_cur/(agent_mag)

    	    #Magnetic Field generation
    	    #print(obs_cur[1,0])
    	    lc_cross = Matrix([[0, -obs_cur[2,0], obs_cur[1,0]], [obs_cur[2,0], 0, -obs_cur[0,0]], [-obs_cur[1,0], obs_cur[0,0], 0]])
    	    #print(lc_cross)
    	    B = lc_cross*Matrix([[dist_to_obs.x],[dist_to_obs.y],[dist_to_obs.z]])/distance**1

    	    #Force acting on a robot
    	    la_cross = Matrix([[0, -agent_cur[2,0], agent_cur[1,0]], [agent_cur[2,0], 0, -agent_cur[0,0]], [-agent_cur[1,0], agent_cur[0,0], 0]])
    	    Force = constant2*la_cross*B
    	    vel_obs.x = Force[0,0]
    	    vel_obs.y = Force[1,0]
            vel_obs.z = Force[2,0]
      elif(field == 2):
    	# if((dist_to_obs.x!=0) and (dist_to_obs.y!=0) and (dist_to_obs.z!=0)):
        if(distance!=0):
    	  # Initial direction of goal
    	#   obs_cur_old = Matrix([[current_obs.x], [current_obs.y], [current_obs.z]])

    	  if distance > 2*bound:
    	    vel_obs.x = 0
    	    vel_obs.y = 0
            vel_obs.z = 0

            flag_obs.data = 0
            # print "clear"
    	  else :
            flag_obs.data = 1
            # print "obs"
            if(distance < dist_min):
                dist_min = distance
                # print(dist_min)
    	    # Current generation on the robot

    	    agent_cur = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
            if(speed!=0):
                agent_cur = agent_cur/(speed)
                # print(sp.linear.x)
                # print(sp.linear.y)
            # print(agent_cur)

            # New method
            dist_from_obs = Matrix([[-1*dist_to_obs.x],[-1*dist_to_obs.y],[-1*dist_to_obs.z]])
            # print "dist"
            # print dist_from_obs.norm()
            # print "agent_cur"
            # print agent_cur
            obs_cur = agent_cur - (agent_cur.dot(dist_from_obs)/dist_from_obs.norm())*(dist_from_obs/dist_from_obs.norm())
            # print("before:")
            mag = (obs_cur[0,0]**2+obs_cur[1,0]**2+obs_cur[2,0]**2)**0.5
            # print(mag)
            mag_bound = 0.000003
            # print mag
            # print obs_cur
            if(mag < mag_bound and mag!=0):
                # obs_cur = Matrix([[0, 1, 0],[-1, 0, 0], [0, 0, 1]])*agent_cur
                obs_cur = obs_cur/obs_cur.norm()
                # print obs_cur
                print("rot\n")
            elif(mag==0):
                print "zero current"
            # print(obs_cur.norm())

            # Try commented this below

            # obs_cur = obs_cur/obs_cur.norm()
            # print obs_cur
            # print "a"
            # print("after:")
            # print(obs_cur)
            # cos_theta_print = obs_cur.dot(obs_cur_old)/(obs_cur.norm()*obs_cur_old.norm())
            # print(cos_theta_print)
            o_cur.x = obs_cur[0,0]
            o_cur.y = obs_cur[1,0]
            o_cur.z = obs_cur[2,0]
            ag_cur.x = agent_cur[0,0]
            ag_cur.y = agent_cur[1,0]
            ag_cur.z = agent_cur[2,0]
            # obs_cur = Matrix([[current_obs.x], [current_obs.y], [current_obs.z]])

    	    #print("agent_current")
    	    #print(agent_cur)

    	    #Magnetic Field generation
    	    #print(obs_cur[1,0])
    	    lc_cross = Matrix([[0, -obs_cur[2,0], obs_cur[1,0]], [obs_cur[2,0], 0, -obs_cur[0,0]], [-obs_cur[1,0], obs_cur[0,0], 0]])
    	    B = lc_cross*Matrix([[sp.linear.x],[sp.linear.y],[sp.linear.z]])/distance**1
            # B = lc_cross*Matrix([[sp.linear.x],[sp.linear.y],[0]])*(1/distance**1-1/(2*bound))
            # print lc_cross
            # print B
    	    #Force acting on a robot
    	    la_cross = Matrix([[0, -agent_cur[2,0], agent_cur[1,0]], [agent_cur[2,0], 0, -agent_cur[0,0]], [-agent_cur[1,0], agent_cur[0,0], 0]])
    	    Force = constant2*la_cross*B
            # print "Force"
            # print Force
            # Add repulsion
            if distance > bound_add :
                vel_obs_add.x = 0
                vel_obs_add.y = 0
                vel_obs_add.z = 0
            else :
                if distance !=0 :
                    if(field_add == 1):
                        vel_obs_add.y = -constant_add*(1/distance - 1/bound_add)*(dist_to_obs.y)/(distance ** 1)
                        vel_obs_add.x = -constant_add*(1/distance - 1/bound_add)*(dist_to_obs.x)/(distance ** 1)
                        vel_obs_add.z = -constant_add*(1/distance - 1/bound_add)*(dist_to_obs.z)/(distance ** 1)
                    else:
                        vel_obs_add.y = -constant2*speed*(dist_to_obs.y)/(distance ** 1)
                        vel_obs_add.x = -constant2*speed*(dist_to_obs.x)/(distance ** 1)
                        vel_obs_add.z = -constant2*speed*(dist_to_obs.z)/(distance ** 1)
                else :
                    print("Colliding...\n")
            Force_add = Matrix([[vel_obs_add.x],[vel_obs_add.y],[vel_obs_add.z]])
            Force_add = Force_add - Force_add.dot(agent_cur)*agent_cur
            # print "Force_add"
            # print Force_add
            sign_f = Force.dot(Force_add)
            # print "sign"
            # print sign_f
            # if(sign_f<0):
            #     print "Force"
            #     print Force
            #
            #     print "Force_add_mag"
            #     print Force_add
            #
            #     print "Force_tot"
            #     print (Force+Force_add)
            #
            #     print "dist_to_obs"
            #     print distance
            if(field_add != 0):
                vel_obs.x = Force[0,0] + Force_add[0,0]
                vel_obs.y = Force[1,0] + Force_add[1,0]
                vel_obs.z = Force[2,0] + Force_add[2,0]
            else:
                vel_obs.x = Force[0,0]
                vel_obs.y = Force[1,0]
                vel_obs.z = Force[2,0]
            # print "collision_node_1"
            # print vel_obs
            # print sp.linear
            # dot_out = (vel_obs.x*sp.linear.x + vel_obs.y*sp.linear.y + vel_obs.z*sp.linear.z)
            # print dot_out
            # print "collision_node_2"
            # print Force+Force_add
            # print agent_cur*speed
            # print (Force+Force_add).dot(agent_cur)*speed
            if(robot_type==1):
                if(speed!=0):
                    omega_msg.z = find_magnitude(vel_obs.x, vel_obs.y, 0.0)/(mass_const*speed)
                    dummy_omega = la_cross*Force
                    if(dummy_omega[2,0]>=0):
                        omega_msg.z = omega_msg.z
                    else:
                        omega_msg.z = -1*omega_msg.z
                else:
                    omega_msg.z = 0.0

      elif(field==3):
          # Haddadin
          if(distance!=0):
              if distance > 2*bound:
                  vel_obs.x = 0
                  vel_obs.y = 0
                  vel_obs.z = 0
              else:
                  flag_obs.data = 1
                  agent_cur = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
                  if(speed!=0):
                      agent_cur = agent_cur/(speed)

                  # New method
                  b_vect = Matrix([[dist_to_goal.x], [dist_to_goal.y], [dist_to_goal.z]])

                  # Distance to the centre of obstacle
                  m_x_vect = Matrix([[central_obs.x], [central_obs.y], [central_obs.z]])
                  # Trial --> centre of obstacle is chosen as closest point
                #   m_x_vect = Matrix([[-1*dist_to_obs.x],[-1*dist_to_obs.y],[-1*dist_to_obs.z]])
                #   m_x_vect = Matrix([[central_obs_real.x], [central_obs_real.y], [central_obs_real.z]])

                  d_vect = m_x_vect - m_x_vect.dot(b_vect)*b_vect/b_vect.norm()
                  dist_from_obs = Matrix([[-1*dist_to_obs.x],[-1*dist_to_obs.y],[-1*dist_to_obs.z]])
                  n_cross = Matrix([[0, -dist_from_obs[2,0], dist_from_obs[1,0]],[dist_from_obs[2,0], 0, -dist_from_obs[0,0]], [-dist_from_obs[1,0], dist_from_obs[0,0], 0]])
                  d_cross = Matrix([[0, -d_vect[2,0], d_vect[1,0]],[d_vect[2,0], 0, -d_vect[0,0]], [-d_vect[1,0], d_vect[0,0], 0]])
                  obs_cur = (n_cross/distance)*(d_cross*b_vect/(d_cross*b_vect).norm())
                  # print("before:")
                  mag = (obs_cur[0,0]**2+obs_cur[1,0]**2+obs_cur[2,0]**2)**0.5
                  # print(mag)
                  o_cur.x = obs_cur[0,0]
                  o_cur.y = obs_cur[1,0]
                  o_cur.z = obs_cur[2,0]
                  ag_cur.x = agent_cur[0,0]
                  ag_cur.y = agent_cur[1,0]
                  ag_cur.z = agent_cur[2,0]
                  lc_cross = Matrix([[0, -obs_cur[2,0], obs_cur[1,0]], [obs_cur[2,0], 0, -obs_cur[0,0]], [-obs_cur[1,0], obs_cur[0,0], 0]])
                  B = lc_cross*Matrix([[sp.linear.x],[sp.linear.y],[sp.linear.z]])/(speed*distance**2)
                  la_cross = Matrix([[0, -agent_cur[2,0], agent_cur[1,0]], [agent_cur[2,0], 0, -agent_cur[0,0]], [-agent_cur[1,0], agent_cur[0,0], 0]])
                #   Force = 10*constant2*la_cross*B
                  Force = 10*constant2*la_cross*B*speed
                  vel_obs.x = Force[0,0]
                  vel_obs.y = Force[1,0]
                  vel_obs.z = Force[2,0]
      elif(field==4):
          # Sabattini
          if(distance!=0):
              if distance > 2*bound:
            #   if distance > 3*bound:
                  vel_obs.x = 0
                  vel_obs.y = 0
                  vel_obs.z = 0
              else:
                  agent_vel = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
                  vect_to_obs = Matrix([[dist_to_obs.x],[dist_to_obs.y],[dist_to_obs.z]])
                  if(speed!=0):
                      agent_cur = agent_vel
                  else:
                      agent_cur = vect_to_obs
                  # New method
                  u_t = Matrix([[dist_to_goal.x], [dist_to_goal.y], [dist_to_goal.z]])
                  u_t = u_t*0.1
                  w_vect = u_t - u_t.dot(agent_cur)*agent_cur/(agent_cur.norm()**2)
                #   print w_vect.norm()
                  u_g_vect = -constant2*w_vect/w_vect.norm()

                  v_scalar = agent_vel.dot(vect_to_obs)
                  if(v_scalar>=0):
                      sigma = 1
                      sigmoid = 1
                  else:
                      sigma = 0
                      sigmoid = -1
                  energy = 0.1*(init.data**2)
                  damper = -0.05*sigmoid*energy/vect_to_obs.norm()*(abs(v_scalar)+math.exp(-abs(v_scalar)))*vect_to_obs/vect_to_obs.norm()
                #   print "sigma"
                #   print sigma
                #   print "sigmoid"
                #   print sigmoid
                #   print u_g_vect
                #   print damper.norm()
                  print "e"
                  print energy

                  Force = sigma*(u_g_vect+damper)

                  vel_obs.x = Force[0,0]
                  vel_obs.y = Force[1,0]
                  vel_obs.z = Force[2,0]
      elif(field==5):
          # VFF
          vect_to_obs = Matrix([[dist_to_obs.x], [dist_to_obs.y], [dist_to_obs.z]])
          agent_vel = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
          if(agent_vel.norm()!=0 and vect_to_obs.norm()!=0):
              cos_theta = vect_to_obs.dot(agent_vel)/(agent_vel.norm()*vect_to_obs.norm())
              v_max = 0.75
              Rr = 0.18
              k_vff = 5.0
              rho_0 = 0.2
              P_vff = 20.0
              C_vff = 2.0
              F_max = 20.0
              Q_vff = 20.0
              Tp = 1.0
              vr = agent_vel.norm()
              Er = vr/(v_max*C_vff)
              D_max = k_vff*Er*Rr*Tp/(1-Er*cos_theta)
              D_min = rho_0*D_max
              print D_max
              print D_min
              print vect_to_obs
              print agent_vel
              if(distance > D_max):
                  vel_obs.x = 0
                  vel_obs.y = 0
                  vel_obs.z = 0
              elif(distance<D_max and distance>=D_min):
                  mag_vel = (D_max - distance)*P_vff/(D_max-D_min)
                  vel_obs.x = -mag_vel*vect_to_obs[0,0]/vect_to_obs.norm()
                  vel_obs.y = -mag_vel*vect_to_obs[1,0]/vect_to_obs.norm()
                  vel_obs.z = -mag_vel*vect_to_obs[2,0]/vect_to_obs.norm()
              else:
                  mag_vel = F_max
                  vel_obs.x = -mag_vel*vect_to_obs[0,0]/vect_to_obs.norm()
                  vel_obs.y = -mag_vel*vect_to_obs[1,0]/vect_to_obs.norm()
                  vel_obs.z = -mag_vel*vect_to_obs[2,0]/vect_to_obs.norm()
          else:
              vel_obs.x = 0
              vel_obs.y = 0
              vel_obs.z = 0
      else:
    	#dynamic potential field

    	if(speed!=0 and distance!=0):
    	  cos_theta = ((sp.linear.x*-dist_to_obs.x + sp.linear.y*-dist_to_obs.y)/(speed*distance))
    	  if(cos_theta>1.0):
    	    cos_theta = 1.0
    	  elif(cos_theta<-1.0):
    	    cos_theta = -1.0
    	  #print(cos_theta)
    	  #print(math.acos(cos_theta))
    	  theta = math.acos(cos_theta)
    	  grad_cos_theta = [(sp.linear.x*(-dist_to_obs.y)**2-sp.linear.y*-dist_to_obs.x*-dist_to_obs.y)/(speed*(distance**3)), (sp.linear.y*(-dist_to_obs.x)**2-sp.linear.x*-dist_to_obs.x*-dist_to_obs.y)/(speed*(distance**3))]
    	else:
    	  theta = 0;

    	  #print(theta)

    	if((theta > math.pi/2) and (theta <= math.pi ) and (distance!=0) and (speed!=0)):
    	  vel_obs.x = constant*(-cos_theta)**(beta-1)*speed/distance*(beta*grad_cos_theta[0]-cos_theta*-dist_to_obs.x/distance**2)
    	  vel_obs.y = constant*(-cos_theta)**(beta-1)*speed/distance*(beta*grad_cos_theta[1]-cos_theta*-dist_to_obs.y/distance**2)
    	else:
    	  vel_obs.x = 0
    	  vel_obs.y = 0


      ## CHANGE THIS
      #if(abs(vel_obs.x)>maks):
	#maks = abs(vel_obs.x)
	##print(maks)
      #if(abs(vel_obs.y)>maks):
	#maks = abs(vel_obs.y)
	##print(maks)
    #   print(vel_obs.x)
    #   print(vel_obs.y)
      if(robot_type==1 and field==2):
          vel_msg.x = vel_msg.x
          vel_msg.y = vel_msg.y
          vel_msg.z = vel_msg.z

      else:
        #   print "before"
        #   print vel_msg
          vel_msg.x = vel_msg.x + vel_obs.x/mass_const
          vel_msg.y = vel_msg.y + vel_obs.y/mass_const
          vel_msg.z = vel_msg.z + vel_obs.z/mass_const
        #   print "after"
        #   print vel_msg


      #print(active_obstacle)
    #   print "flag_av"
    #   print flag_speed
    #   if(flag_speed.data == 1):
        #   turtle_vel.publish(vel_msg)
        #   flag_speed.data = 0

      vel_msg_stamped = Vector3Stamped()
      vel_msg_stamped.header.stamp = rospy.get_rostime()
      vel_msg_stamped.vector.x = vel_msg.x
      vel_msg_stamped.vector.y = vel_msg.y
      vel_msg_stamped.vector.z = vel_msg.z

      turtle_vel_stamped.publish(vel_msg_stamped)
      turtle_vel.publish(vel_msg)

      turtle_omega.publish(omega_msg)
      agent_current.publish(ag_cur)
      obstacle_current.publish(o_cur)
      flag_obs_pub.publish(flag_obs)
      rate.sleep()
