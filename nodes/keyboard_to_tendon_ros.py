#!/usr/bin/env python
# The code is used to read the wrist's quarternion data from the Serial.
# Created by King's College London and Queen Mary University of London, 2017.

import roslib; roslib.load_manifest('inflatable_arm')
import math
import rospy
import serial
import string
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float32
from sensor_msgs.msg import Joy
from std_msgs.msg import Int32

class read_wrist(object):
  def __init__(self):
    # Initialize the ROS Node
    rospy.init_node('tendon_sent_ros')
    self.control = rospy.get_param('~control_pressure')

    rec_joy = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    rec_press = rospy.Subscriber('/pressure_read', Float32, self.read_pressure)
    length_dot_pub = rospy.Publisher('/length_dot', Float64MultiArray, queue_size = 10)
    press_pub = rospy.Publisher('/pressure_status', Float32, queue_size = 10)
    reset_pub = rospy.Publisher('/reset_status', Int32, queue_size = 10)
    # self.flag_joy = 0
    self.pressure = Float64MultiArray()
    self.pressure_joy = []
    self.inflate = 0.0
    self.zeros = 0
    self.inflate_sent = Float32()
    self.inflate_sent.data = 0.0
    self.zeros_sent = Int32()
    const_press_arduino = 20.0
    const_press = float(9.0/256.0)*const_press_arduino
    self.press_add = 0.05/const_press
    for i in range(0,3):
        self.pressure_joy.append(0)
    # Set the rate
    r = rospy.Rate(50.0)
    self.pressure_now = 0.0
    KP = 5.0
    if(self.control == 0):
        self.flag_joy = 0
    else:
        self.flag_joy = 1
    # a_var = 1
    p_max = 256.0/const_press_arduino
    while not rospy.is_shutdown():
        # print self.control
        # print self.inflate_sent
        # print self.flag_joy
        if(self.flag_joy == 1):
            self.pressure.data = []
            for i in range(0,3):
                self.pressure.data.append(self.pressure_joy[i])
            if(self.control == 1):
                pressure_desired = 8.0
                delta_p = -KP*(self.pressure_now - pressure_desired)
                self.inflate_sent.data = self.pressure_now + delta_p
                print "tes"
                print delta_p
                print self.pressure_now
                print self.inflate_sent.data
                if(self.inflate_sent.data>p_max):
                    self.inflate_sent.data = p_max
                print self.inflate_sent.data
            else:
                self.flag_joy = 0
                # print "tes2"
                self.inflate_sent.data = self.inflate
            self.zeros_sent.data = self.zeros
        else:
            self.pressure.data = []
            for i in range(0,3):
                self.pressure.data.append(0.0)
            # self.inflate_sent.data = 0
            # self.zeros_sent.data = 0
        print self.inflate*const_press

        press_pub.publish(self.inflate_sent)
        length_dot_pub.publish(self.pressure)
        reset_pub.publish(self.zeros_sent)
        r.sleep()



  def ps3_callback(self,msg):
    self.flag_joy = 1
    self.pressure_joy[0] = msg.axes[0]
    self.pressure_joy[2] = msg.axes[1]
    self.pressure_joy[1] = msg.axes[3]
    if(msg.axes[2]==1):
        self.inflate = self.inflate + self.press_add;
    elif(msg.axes[2]==-1):
        self.inflate = self.inflate - self.press_add;
    elif(msg.axes[5] == 1):
        self.inflate = 0;
    if(msg.buttons[3]==1 and msg.buttons[1]==0):
        self.zeros = 1;
    elif(msg.buttons[1]==1 and msg.buttons[3]==0):
        self.zeros = 0;
    elif(msg.axes[4]==1):
        self.zeros = 2;

  def read_pressure(self,msg):
    self.pressure_now = msg.data

if __name__ == '__main__':
    try:
        read_wrist()
    except rospy.ROSInterruptException: pass
