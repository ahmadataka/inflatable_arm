#!/usr/bin/env python
# The code is used to read the wrist's quarternion data from the Serial.
# Created by King's College London and Queen Mary University of London, 2017.

import roslib; roslib.load_manifest('inflatable_arm')
import math
import rospy
import serial
import string
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy

class read_wrist(object):
  def __init__(self):
    # Initialize the ROS Node
    rospy.init_node('tendon_sent')

    # Initialize the serial class
    ser = serial.Serial()
    # Define the serial port
    # ser.port = "/dev/rfcomm0"
    ser.port = "/dev/ttyACM0"
    # Set the baudrate
    ser.baudrate = 9600
    # Set the timeout limit
    ser.timeout = 1
    # Open the serial
    ser.open()

    rec_joy = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    self.flag_joy = 0
    self.pressure = []
    self.pressure_joy = []
    self.inflate = 0
    self.zeros = 0
    self.press_add = 1
    for i in range(0,5):
        self.pressure.append(0)
        self.pressure_joy.append(0)
    # Set the rate
    r = rospy.Rate(50.0)
    # a_var = 1
    while not rospy.is_shutdown():
        if(self.flag_joy == 1):
            for i in range(0,3):
                self.pressure[i] = self.pressure_joy[i]
            self.pressure[3] = self.inflate
            self.pressure[4] = self.zeros
        else:
            for i in range(0,3):
                self.pressure[i] = 0
            self.pressure[3] = 0
            self.pressure[4] = 0
        a_var = ''
        for j in range(0,5):
            # ser.write(str(chr(j)))
            a_var = a_var + str(self.pressure[j])
            if(j!=5-1):
                a_var = a_var + ','
            else:
                a_var = a_var + '\n'
            # ser.write(str(chr(ord('a')+j)))
            # ser.write(str(chr(self.pressure[j])))
            # print j
            # print self.pressure[j]
        if(self.flag_joy == 1):
            ser.write(a_var)
            self.flag_joy = 0
            print a_var

        r.sleep()

    ser.close()

  def ps3_callback(self,msg):
    self.flag_joy = 1
    if(msg.axes[0]!=0):
        self.pressure_joy[0] = msg.axes[0]+2
    else:
        self.pressure_joy[0] = msg.axes[0]
    if(msg.axes[1]!=0):
        self.pressure_joy[2] = msg.axes[1]+2
    else:
        self.pressure_joy[2] = msg.axes[1]
    if(msg.axes[3]!=0):
        self.pressure_joy[1] = msg.axes[3]+2
    else:
        self.pressure_joy[1] = msg.axes[3]
    if(msg.axes[2]==1):
        self.inflate = self.inflate + self.press_add;
    elif(msg.axes[2]==-1):
        self.inflate = 0;
    if(msg.buttons[3]==1 and msg.buttons[1]==0):
        self.zeros = 1;
    elif(msg.buttons[1]==1 and msg.buttons[3]==0):
        self.zeros = 0;
    elif(msg.axes[4]==1):
        self.zeros = 2;



if __name__ == '__main__':
    try:
        read_wrist()
    except rospy.ROSInterruptException: pass
