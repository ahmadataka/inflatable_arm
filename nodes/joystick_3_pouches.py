#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
import numpy as np
from keras.models import model_from_yaml
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from sensor_msgs.msg import Joy
import rospkg
import math
from inflatable_arm.msg import inflatable_msg

class bending_model(object):
  def __init__(self):
    self.bend_number = 3
    self.pouch_number = 3
    rospy.init_node('bending_model')
    
    self.pub_bending = rospy.Publisher('/bending', Float64MultiArray, queue_size = 10)
    self.pub_pressure_rate = rospy.Publisher('/pressure_rate', Float64MultiArray, queue_size = 10)
    self.pub_pressure = rospy.Publisher('/pressure', Float64MultiArray, queue_size = 10)
    rec_joy = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    
    
    # freq = 100.0
    freq = 50
    self.delta_t = 1.0 / freq
    r = rospy.Rate(freq)
    # self.p_in = [0.0, 0. , 0.01 , 0.01, 0., 0.0, 0.01]
    self.p_in = []
    for i in range(0, self.bend_number*self.pouch_number+1):
        self.p_in.append(0.0)
    self.p_change = np.zeros((self.bend_number,1))
    self.dp = 0.05
    self.link = []
    for i in range(0, self.bend_number):
        if(i!=(self.bend_number-1)):
            self.link.append(0.1)
        else:
            self.link.append(0.0)
    self.bend_section = 0.42
    rospy.set_param('bend_number', self.bend_number)
    self.p_dot = np.zeros((self.bend_number,1))
    self.p0_dot = 0.0
    self.bending_sent = Float64MultiArray()
    self.pressure_sent = Float64MultiArray()
    self.pressure_rate_sent = Float64MultiArray()
    self.pose_sent = PoseArray()
    self.goal_sent = Pose()
    self.goal_kinect_sent = Pose()
    jacobi_sent = Float64MultiArray()


    
    flag_goal = 0

    # Goal init    
    self.moving_flag = 0
    self.active_segment = 0
    rospy.set_param('active_segment', self.active_segment)

    ## Kalman
    self.state_flag = 0
    self.flag_estimation = 0
    self.state_training = 0
    rospy.set_param('start_estimate', self.flag_estimation)
    rospy.sleep(5)

    self.starting = rospy.get_time()
    while not rospy.is_shutdown():
        rospy.set_param('start_estimate', self.flag_estimation)
        rospy.set_param('active_segment', self.active_segment)
        
        # Send data
        self.pub_bending.publish(self.bending_sent)
        
        self.pressure_sent.data = []
        for i in range(0,len(self.p_in)):
            self.pressure_sent.data.append(self.p_in[i])
        self.pub_pressure.publish(self.pressure_sent)
        self.pub_pressure_rate.publish(self.pressure_rate_sent)
        # print self.ls_param
        self.pressure_rate_sent.data = []
        for i in range(0, len(self.p_dot)+1):
            if(i==0):
                self.pressure_rate_sent.data.append(self.p0_dot)
                # self.p0_dot = 0.0
            else:
                self.pressure_rate_sent.data.append(self.p_dot[i-1,0])
        print self.active_segment

        r.sleep()

  def ps3_callback(self,msg):
    ## For Keyboard:
    # a, d, w, s to move goal in Inverse Kinematics
    # w, s to change pouch pressure in Forward Kinematics
    # k, l to change inflation pressure
    # c, v to change active segment in Forward Kinematics
    # b Reset
    # m Moving
    # u Switch Kinematics mode

    if(msg.buttons[8] == 1):
        self.p_in = [0.0, 0. , 0.0 , 0.0, 0., 0.0, 0.0, 0.0, 0.0, 0.0]
    self.p_in[3*self.active_segment+1] = self.p_in[3*self.active_segment+1]+1*msg.axes[1]*self.dp
    self.p_in[3*self.active_segment+2] = self.p_in[3*self.active_segment+2]+1*msg.axes[3]*self.dp
    self.p_in[3*self.active_segment+3] = self.p_in[3*self.active_segment+3]+1*msg.axes[0]*self.dp
    self.p_in[0] = self.p_in[0] + (msg.axes[4]-msg.axes[5])*self.dp
    for i in range(0,10):
        if(self.p_in[i]<0):
            self.p_in[i]=0
    self.active_segment = self.active_segment - int(msg.axes[6])
    if(self.active_segment>2):
        self.active_segment = 0
    if(self.active_segment<0):
        self.active_segment = 2
        
if __name__ == '__main__':
    try:
        bending_model()
    except rospy.ROSInterruptException: pass
