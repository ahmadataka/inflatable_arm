#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import PoseArray
import numpy as np

class record_data(object):
  def __init__(self):
    rospy.init_node('automatic_recording')

    self.num = 0
    i = j = k = 0
    min = [0.3, 0.0, 0.0]
    max = [1.2, 1.5, 1.5]
    delta = [0.05, 0.05]
    self.pub_press = rospy.Publisher('/pressure', Float64MultiArray, queue_size = 10)
    rec_pcd = rospy.Subscriber('/bending', Float64MultiArray, self.bending_callback)
    rec_tip = rospy.Subscriber('/tip_pose', PoseArray, self.pose_callback)


    self.press_sent = Float64MultiArray()
    self.bending = Float64MultiArray()
    self.pose = Float64MultiArray()
    flag_switch = 0

    print "starting"
    rospy.sleep(5.0)
    while not rospy.is_shutdown():
        # Sending pressure
        self.press_sent.data = []
        self.press_sent.data.append(min[0]+i*delta[0])
        self.press_sent.data.append(0.0)
        self.press_sent.data.append(0.0)
        self.press_sent.data.append(min[1]+j*delta[1])
        self.press_sent.data.append(min[2]+k*delta[1])

        self.pub_press.publish(self.press_sent)
        print self.num
        print self.press_sent
        rospy.sleep(0.2)

        # Record CSV
        # self.record_bending()
        # self.record_pressure()
        # self.record_tip()

        # Increasing the index
        self.num = self.num + 1

        if(self.num % int(2*(max[1]-min[1])/delta[1]+2)==0):
            i = i+1
        if(flag_switch == 0):
            j = j+1
        else:
            k = k+1
        if(min[0]+i*delta[0] > max[0]):
            i = 0
            rospy.signal_shutdown(self.myhook)
        if(j*delta[1] > max[1]):
            j = 0
            flag_switch = 1
        if(k*delta[1] > max[2]):
            k = 0
            flag_switch = 0
        # rospy.sleep(1.0)

  def myhook(self):
      print "shutdown time!"

  def record_tip(self):
      print("Save the pose!")
      np.savetxt('pose_'+str(self.num)+'.csv', np.asarray(self.pose.data).reshape(1,len(self.pose.data)), delimiter=',')   # X is an array

  def record_bending(self):
      print("Save bending!")
      np.savetxt('bending_'+str(self.num)+'.csv', np.asarray(self.bending.data).reshape(1,len(self.bending.data)), delimiter=',')   # X is an array

  def record_pressure(self):
      print("Save pressure!")
      press_save = []
      for i in range(0,5):
          press_save.append(self.press_sent.data[i])
      np.savetxt('pressure_'+str(self.num)+'.csv', np.asarray(press_save).reshape(1,len(press_save)), delimiter=',')   # X is an array

  def bending_callback(self,msg):
      self.bending.data = []
      for i in range(0,len(msg.data)):
          self.bending.data.append(msg.data[i])

  def pose_callback(self, msg):
    self.pose.data = []
    for i in range(0, len(msg.poses)):
        self.pose.data.append(msg.poses[i].position.x)
        self.pose.data.append(msg.poses[i].position.y)

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
