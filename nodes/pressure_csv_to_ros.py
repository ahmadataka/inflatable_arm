#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray
import math
import pandas as pd

class bending_model(object):
  def __init__(self):
    rospy.init_node('bending_model')
    file_name = '~/catkin_ws/src/inflatable_arm/wormbot/control/tip_tracking_exp.csv'
    #file_name = '~/wormbot_exp_2/bending_1.csv'
    df_tracking_exp = pd.read_csv(file_name)
    selected_data_list = ['field.l_space.data0','field.l_space.data1','field.l_space.data2','field.l_space.data3','field.l_space.data4']
    data_full_exp = np.array(df_tracking_exp[selected_data_list])
    pressure_exp  = data_full_exp[1:len(data_full_exp),0:5]

    self.pub_pressure = rospy.Publisher('/pressure', Float64MultiArray, queue_size = 10)

    freq = 50.0
    self.delta_t = 1.0 / freq
    r = rospy.Rate(freq)
    index = 0
    self.pressure_sent = Float64MultiArray()
    # print pressure_exp
    while not rospy.is_shutdown():
        # Map
        p_now = np.empty(((pressure_exp.shape[1]-1)/2,1), float)
        for i in range(0,(pressure_exp.shape[1]-1)/2):
            if(pressure_exp[index,1+2*i]<=0.001 and pressure_exp[index, 2+2*i]>0.001):
                p_now[i,0] = -1.*pressure_exp[index,2+2*i]
            else:
                p_now[i,0] = pressure_exp[index,1+2*i]

        self.pressure_sent.data = []
        self.pressure_sent.data.append(pressure_exp[index,0])
        for i in range(0,(pressure_exp.shape[1]-1)/2):
            # self.pressure_sent.data.append(pressure_exp[index,i])
            self.pressure_sent.data.append(p_now[i,0])
        self.pub_pressure.publish(self.pressure_sent)
        if(index<pressure_exp.shape[0]):
            index = index + 1
        else:
            break
        r.sleep()

if __name__ == '__main__':
    try:
        bending_model()
    except rospy.ROSInterruptException: pass
