#!/usr/bin/env python
# The code is used to read the wrist's quarternion data from the Serial.
# Created by King's College London and Queen Mary University of London, 2017.

import roslib; roslib.load_manifest('inflatable_arm')
import math
import rospy
import serial
import string
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Int32
from sensor_msgs.msg import Joy
from sensor_msgs.msg import JointState

class read_wrist(object):
  def __init__(self):
    # Initialize the ROS Node
    rospy.init_node('tendon_sent')
    self.inverse_input = rospy.get_param('~inverse')

    # # Initialize the serial class
    # ser = serial.Serial()
    # # Define the serial port
    # # ser.port = "/dev/rfcomm0"
    # ser.port = "/dev/ttyACM0"
    # # Set the baudrate
    # ser.baudrate = 9600
    # # Set the timeout limit
    # ser.timeout = 1
    # # Open the serial
    # ser.open()

    rec_joy = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    rec_joint = rospy.Subscriber('/agent/joint', JointState, self.joint_callback)
    length_dot_pub = rospy.Publisher('/length_dot', Float64MultiArray, queue_size = 10)
    press_pub = rospy.Publisher('/pressure_status', Int32, queue_size = 10)
    self.flag_joy = 0
    self.pressure = []
    self.pressure_now = []
    self.pressure_target = []
    self.pressure_next = []
    self.pressure_dot = []
    self.pressure_dot_rec = []
    self.pressure_dot_sent = Float64MultiArray()
    self.press_stat_sent = Int32()
    self.inflate = 0
    self.zeros = 0
    self.press_add = 1
    self.delta_l = 0.05
    if(self.inverse_input == 0):
        self.l_zero = 9.0
    else:
        self.l_zero = 0.15
    for i in range(0,3):
        self.pressure_now.append(self.l_zero)
        self.pressure_target.append(self.l_zero)
        self.pressure_next.append(self.l_zero)
        self.pressure_dot.append(0.0)
        self.pressure_dot_rec.append(0.0)
    for i in range(0,6):
        self.pressure.append(0)

    self.constant_transfer = 10.0
    # Set the rate
    freq = 10.0
    dt = 1.0 / freq
    r = rospy.Rate(freq)
    # a_var = 1
    while not rospy.is_shutdown():
        self.pressure_dot_sent.data = []
        for i in range(0,3):
            if(self.inverse_input == 0):
                self.pressure_dot[i] = -1.0*(self.pressure_now[i]-self.pressure_target[i])
            else:
                self.pressure_dot[i] = self.pressure_dot_rec[i]
            self.pressure_next[i] = self.pressure_now[i]+self.pressure_dot[i]*dt
            self.pressure_dot_sent.data.append(self.pressure_dot[i])
        for i in range(0,3):
            self.pressure[i] = self.pressure_now[i]
        for i in range(0,3):
            self.pressure[i+3] = self.pressure_next[i]
        a_var = ''
        for j in range(0,6):
            # ser.write(str(chr(j)))
            a_var = a_var + str(self.pressure[j])
            if(j!=6-1):
                a_var = a_var + ','
            else:
                a_var = a_var + '\n'
        print a_var
        for i in range(0,3):
            self.pressure_now[i] = self.pressure_next[i]
        # ser.write(a_var)
        press_pub.publish(self.press_stat_sent)
        length_dot_pub.publish(self.pressure_dot_sent)
        r.sleep()

    # ser.close()

  def ps3_callback(self,msg):
    self.flag_joy = 1
    if(msg.axes[0]!=0):
        self.pressure_target[0] = self.pressure_target[0] + msg.axes[0]*self.delta_l
    if(msg.axes[1]!=0):
        self.pressure_target[2] = self.pressure_target[2] + msg.axes[1]*self.delta_l
    if(msg.axes[3]!=0):
        self.pressure_target[1] = self.pressure_target[1] + msg.axes[3]*self.delta_l
    if(msg.axes[4]==1):
        self.press_stat_sent = 1
    elif(msg.axes[4]==-1):
        self.press_stat_sent = 0

  def joint_callback(self,msg):
    self.pressure_dot_rec = []
    for i in range(0,3):
        self.pressure_dot_rec.append(self.constant_transfer*msg.velocity[i])


if __name__ == '__main__':
    try:
        read_wrist()
    except rospy.ROSInterruptException: pass
