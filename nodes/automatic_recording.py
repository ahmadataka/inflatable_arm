#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Image
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
from sensor_msgs.msg import PointCloud2
import numpy as np

class record_data(object):
  def __init__(self):
    rospy.init_node('automatic_recording')
    # Instantiate CvBridge
    self.bridge = CvBridge()

    self.num = 0
    self.num_repeat = 0

    self.image_rec = 0
    i = j = k = 0
    min = [0.3, 0.0, 0.0]
    max = [1.5, 4.0, 1.5]
    delta = [0.6, 0.5]
    self.pub_press = rospy.Publisher('/pressure', Float64MultiArray, queue_size = 10)
    image_topic = "/kinect2/qhd/image_color"
    # Set up your subscriber and define its callback
    rospy.Subscriber(image_topic, Image, self.image_callback)
    rec_pcd = rospy.Subscriber('/obstacle_point_kinect', PointCloud2, self.pcd_callback)
    rec_tip = rospy.Subscriber('/pressure_read', Float64MultiArray, self.p_read_callback)


    self.press_sent = Float64MultiArray()
    self.p_read = Float64MultiArray()

    flag_switch = 0
    self.initialize()
    print "starting"
    self.press_sent.data = []
    self.press_sent.data.append(0.)
    self.press_sent.data.append(3.)
    self.press_sent.data.append(0.)
    self.press_sent.data.append(0.)
    self.press_sent.data.append(0.)
    self.press_sent.data.append(0.)
    self.press_sent.data.append(0.)
    rospy.sleep(2.0)

    for index in range(0,3):
        self.pub_press.publish(self.press_sent)
        rospy.sleep(0.2)

    rospy.sleep(5.0)

    while not rospy.is_shutdown():
        # # Sending pressure
        # self.press_sent.data = []
        # self.press_sent.data.append(0.2)
        # self.press_sent.data.append(3.0)
        # self.press_sent.data.append(0.0)
        # self.press_sent.data.append(0.0)
        # self.press_sent.data.append(3.0)
        #
        # self.pub_press.publish(self.press_sent)
        #
        # rospy.sleep(3.0)
        # self.press_sent.data = []
        # self.press_sent.data.append(0.2)
        # self.press_sent.data.append(0.0)
        # self.press_sent.data.append(3.0)
        # self.press_sent.data.append(3.0)
        # self.press_sent.data.append(0.0)
        #
        # self.pub_press.publish(self.press_sent)
        #
        # rospy.sleep(3.0)

        # For repeatability
        for repeat in range(0,1):
            # Sending pressure
            self.press_sent.data = []
            self.press_sent.data.append(min[0]+i*delta[0])
            self.press_sent.data.append(0.)
            self.press_sent.data.append(0.)
            self.press_sent.data.append(0.)
            self.press_sent.data.append(0.)
            self.press_sent.data.append(min[1]+j*delta[1])
            self.press_sent.data.append(0.)
            # self.press_sent.data.append(min[2]+k*delta[1])

            for index in range(0,3):
                self.pub_press.publish(self.press_sent)
                rospy.sleep(0.2)

            print self.num_repeat
            print self.press_sent
            rospy.sleep(10.0)

            # # Record CSV
            self.record_image()
            # self.record_pcd()
            self.record_pressure()

            self.press_sent.data = []
            self.press_sent.data.append(min[0]+i*delta[0])
            self.press_sent.data.append(0.)
            self.press_sent.data.append(0.)
            self.press_sent.data.append(0.)
            self.press_sent.data.append(0.)
            self.press_sent.data.append(0.)
            self.press_sent.data.append(0.)

            for index in range(0,3):
                self.pub_press.publish(self.press_sent)
                rospy.sleep(0.2)
            rospy.sleep(10.0)
            self.num_repeat = self.num_repeat + 1

        # Increasing the index
        self.num = self.num + 1

        # if(self.num % int(2*(max[1]-min[1])/delta[1]+2)==0):
        #     i = i+1
        # if(flag_switch == 0):
        #     j = j+1
        # else:
        #     k = k+1

        # One side only
        if(self.num % int((max[1]-min[1])/delta[1]+1)==0):
            i = i+1
        j = j+1

        if(min[0]+i*delta[0] > max[0]):
            i = 0
            rospy.signal_shutdown(self.myhook)
        if(j*delta[1] > max[1]):
            j = 0
            flag_switch = 1
        if(k*delta[1] > max[2]):
            k = 0
            flag_switch = 0
        # rospy.sleep(1.0)

        # self.press_sent.data = []
        # self.press_sent.data.append(min[0]+i*delta[0])
        # self.press_sent.data.append(0.)
        # self.press_sent.data.append(0.)
        # self.press_sent.data.append(0.)
        # self.press_sent.data.append(0.)
        # self.press_sent.data.append(0.)
        # self.press_sent.data.append(0.)

        # for index in range(0,3):
        #     self.pub_press.publish(self.press_sent)
        #     rospy.sleep(0.2)
        # rospy.sleep(10.0)


  def myhook(self):
      print "shutdown time!"

  def record_image(self):
      print("Save an image!")
      cv2.imwrite('camera_image'+str(self.num_repeat)+'.jpeg', self.cv2_img)

  def record_pcd(self):
      print("Save pointclouds!")
      np.savetxt('pcd_'+str(self.num_repeat)+'.csv', self.points, delimiter=',')   # X is an array

  def record_pressure(self):
      print("Save pressure!")
      press_save = []
      for i in range(0,7):
          press_save.append(self.press_sent.data[i])
      for i in range(0,7):
          press_save.append(self.p_read.data[i])
      np.savetxt('pressure_'+str(self.num_repeat)+'.csv', np.asarray(press_save).reshape(1,14), delimiter=',')   # X is an array

  def image_callback(self,msg):
      try:
          # Convert your ROS Image message to OpenCV2
          self.cv2_img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
          self.image_rec = 1
      except CvBridgeError, e:
          print(e)
      # else:
          # Save your OpenCV2 image as a jpeg
          # cv2.imwrite('camera_image'+str(num)+'.jpeg', self.cv2_img)
  def pcd_callback(self, msg):
    pc = ros_numpy.numpify(msg)

    self.points=np.zeros((pc.shape[0],3))

    self.points[:,0]=np.reshape(pc['x'], (np.product(pc['x'].shape),))
    self.points[:,1]=np.reshape(pc['y'], (np.product(pc['y'].shape),))
    self.points[:,2]=np.reshape(pc['z'], (np.product(pc['z'].shape),))

  def initialize(self):
      self.p_read.data = []
      for i in range(0, 7):
          self.p_read.data.append(0.0)

  def p_read_callback(self, msg):
    self.p_read.data = []
    for i in range(0, 7):
        self.p_read.data.append(msg.data[i])


if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
