#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import Float32

class record_data(object):
  def __init__(self):
    rospy.init_node('record_tension')
    self.send = Float32MultiArray()
    self.tension = 0.0
    self.p_write = 0.0
    self.p_read = 0.0
    send_pub = rospy.Publisher('/record_tension', Float32MultiArray, queue_size = 10)
    rec_goal = rospy.Subscriber('/tension', Float32, self.tension_callback)
    rec_tip = rospy.Subscriber('/pressure_write', Float32, self.write_callback)
    rec_joint = rospy.Subscriber('/pressure_read', Float32, self.read_callback)

    frequency = 50.0
    r = rospy.Rate(frequency)

    while not rospy.is_shutdown():
        self.transform_to_sent()
        send_pub.publish(self.send)
        r.sleep()

  def tension_callback(self, msg):
    self.tension = msg.data

  def write_callback(self, msg):
    self.p_write = msg.data

  def read_callback(self, msg):
      self.p_read = msg.data

  def transform_to_sent(self):
    self.send.data = []
    self.send.data.append(self.p_write)
    self.send.data.append(self.p_read)
    self.send.data.append(self.tension)

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
