#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy
import math

class bending_model(object):
  def __init__(self):
    rospy.init_node('bending_model')
    self.pub_pressure = rospy.Publisher('/pressure', Float64MultiArray, queue_size = 10)
    rec_joy = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    
    # freq = 100.0
    freq = 50.0
    self.delta_t = 1.0 / freq
    r = rospy.Rate(freq)

    # self.p_in = [0.3, 0. , 0.3 , 0.3, 0., 0.0, 0.3]
    self.p_in = [0.0, 0. , 0.0 , 0.0, 0., 0.0, 0.0]
    # self.p_in = [1.5, 0. , 0.3, 0.3, 0., 0.0, 0.3]

    self.p_change = np.zeros(((len(self.p_in)-1)/2,1))
    self.dp = 0.05
    self.pressure_sent = Float64MultiArray()
    rospy.sleep(5)

    while not rospy.is_shutdown():
        self.pressure_sent.data = []
        for i in range(0,len(self.p_in)):
            self.pressure_sent.data.append(self.p_in[i])
        self.pub_pressure.publish(self.pressure_sent)
        r.sleep()

  def ps3_callback(self,msg):
    self.p_in[1] = self.p_in[1] + msg.axes[0]*self.dp
    self.p_in[2] = self.p_in[2] + msg.axes[1]*self.dp
    self.p_in[3] = self.p_in[3] + msg.axes[2]*self.dp
    self.p_in[4] = self.p_in[4] + msg.axes[3]*self.dp
    self.p_in[5] = self.p_in[5] + msg.axes[6]*self.dp
    self.p_in[6] = self.p_in[6] + msg.axes[7]*self.dp
    self.p_in[0] = self.p_in[0] + (msg.axes[4]-msg.axes[5])*self.dp
    for i in range(0, len(self.p_in)):
        if(self.p_in[i]<0.0):
            self.p_in[i] = 0.0
    if(msg.buttons[3] == 1):
        self.p_in = [0.0, 0. , 0.0 , 0.0, 0., 0.0, 0.0]
if __name__ == '__main__':
    try:
        bending_model()
    except rospy.ROSInterruptException: pass
