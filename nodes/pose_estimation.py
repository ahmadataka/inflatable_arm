#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray

class bending_model(object):
  def __init__(self):
    rospy.init_node('pose_estimation')
    # self.is_pose = rospy.get_param('/pose_available')
    self.noisy = rospy.get_param('/noisy')
    self.experiment = rospy.get_param('/experiment')
    self.is_pose = 0
    self.pub_state = rospy.Publisher('/states', Float64MultiArray, queue_size = 10)
    self.pub_output = rospy.Publisher('/output_estimate', Float64MultiArray, queue_size = 10)
    rec_bending = rospy.Subscriber('/bending', Float64MultiArray, self.bending_callback)
    rec_pressure = rospy.Subscriber('/pressure', Float64MultiArray, self.pressure_callback)
    rec_rate = rospy.Subscriber('/pressure_rate', Float64MultiArray, self.pressure_rate_callback)

    # freq = 100.0
    freq = 50.0
    self.delta_t = 1.0 / freq
    r = rospy.Rate(freq)
    self.index_j = self.index_i = 0
    self.p_now = np.zeros((7,1))
    self.flag_callback = 0
    # self.state = np.array([[0.3], [0.], [0.], [0.], [100.], [100.], [100.], [100.], [100.], [100.]])
    # self.P_mat = np.eye(10)
    if(self.experiment == 0):
        self.state = 50*np.array([[1.], [1.], [1.], [1.], [1.], [1.]])
    else:
        # self.state = np.array([[60], [2.], [60.], [1.], [35.], [1.]])
        self.state = 50*np.array([[1.], [1.], [1.], [1.], [1.], [1.]])
    self.P_mat = 0.5*np.eye(6)
    self.bending_est = np.zeros((3, 1))
    self.persistent_mat = np.zeros((3,3))
    if(self.is_pose==1):
        self.state_pose = np.array([[0.], [0.], [0.], [0.1], [0.1], [0.1], [0.1], [0.1]])
        self.P_mat_pose = np.eye(8)
    self.state_sent = Float64MultiArray()
    self.output_sent = Float64MultiArray()
    self.p_rate = []
    for i in range(0, 4):
        self.p_rate.append(0.)
    rospy.sleep(8)
    while not rospy.is_shutdown():
        self.flag_estimation = rospy.get_param('/start_estimate')
        if(self.flag_estimation == 1):
            self.kalman_estimate()


        self.state_sent.data = []
        self.output_sent.data = []
        for i in range(0, len(self.state)):
            self.state_sent.data.append(self.state[i,0])
        for i in range(0, len(self.bending_est)):
            self.output_sent.data.append(self.bending_est[i,0])
        self.pub_state.publish(self.state_sent)
        self.pub_output.publish(self.output_sent)

        r.sleep()

  def bending_callback(self,msg):
    self.bending = []
    self.flag_callback = 1
    for i in range(0, len(msg.data)):
        self.bending.append(msg.data[i])

  def pressure_rate_callback(self,msg):
    self.p_rate = []
    self.flag_callback = 1
    for i in range(0, len(msg.data)):
        self.p_rate.append(msg.data[i])

  def pressure_callback(self,msg):
    self.p_in = []
    self.flag_callback = 1
    for i in range(0, len(msg.data)):
        self.p_in.append(msg.data[i])

    self.p_now = np.empty(((len(self.p_in)-1)/2+1,1), float)
    self.p_now[0,0] = self.p_in[0]
    for i in range(0,(len(self.p_in)-1)/2):
        if(self.p_in[1+2*i]<=0.001 and self.p_in[2+2*i]>0.001):
            self.p_now[i+1,0] = -1.*self.p_in[2+2*i]
        else:
            self.p_now[i+1,0] = self.p_in[1+2*i]

  def least_square(self,input):
    p0_est = input[0,0]

    c1_est = np.empty((3,1), float)
    c2_est = np.empty((3,1), float)
    for i in range(0,3):
        c1_est[i,0] = input[4+i*2,0]
        c2_est[i,0] = input[5+i*2,0]
    # e0_est = input[6,0]
    out = np.array([[0.],[0.],[0.]])
    for i in range(0, len(out)):
        # out[i,0] = (c1_est*input[i+1,0])/(e0_est+c2_est*p0_est)
        out[i,0] = (c1_est[i,0]*input[i+1,0])/(1.+c2_est[i,0]*p0_est)

    return out

  def kalman_estimate(self):
      # A_mat = np.eye(7)
      if(self.is_pose==0):
        #   Q_mat = 1.*np.eye(10)
        #   Q_mat = 0.0000001*np.eye(6)
          if(self.experiment == 0):
            if(self.noisy == 0):
                # R_mat = 0.0000001*np.eye(3)
                Q_mat = 10*np.eye(6)
                R_mat = 10*np.eye(3)
            else:
                # R_mat = 0.1*np.eye(3)
                Q_mat = 1*np.eye(6)
                R_mat = 50*np.eye(3)
          else:
            Q_mat = 1*np.eye(6)
            R_mat = 50*np.eye(3)
      else:
          Q_mat = 1.*np.eye(8)
          R_mat = 1.*np.eye(9)

    #   # Try input
    #   self.p_now[0,0] = 0.1 + self.index_i*0.005
    #   for i in range(0, 1):
    #     self.p_now[i+1,0] = -2.5 + self.index_j*0.05
    #   self.index_i = self.index_i + 1
    #   if(self.index_i>=100):
    #     self.index_i = 0
    #     self.index_j = self.index_j + 1
    #     if(self.index_j>=100):
    #         self.index_j = 0
    #   # output
    #   self.bending = []
    #   for i in range(0,3):
    #       bend = 58.77*self.p_now[i+1,0]/(1+1.16*self.p_now[0,0])
    #       self.bending.append(bend)


      state_est = self.state
      P_mat_est = self.P_mat + Q_mat
      
      # Estimation
      d_i = []
      f_i = []
      p0_est = self.p_now[0,0]
      c1_est = np.empty((3,1), float)
      c2_est = np.empty((3,1), float)
      for i in range(0,3):
          c1_est[i,0] = state_est[i*2,0]
          c2_est[i,0] = state_est[1+i*2,0]

      for i in range(0,3):
          d_i.append(self.p_now[i+1,0]/(1.+c2_est[i,0]*p0_est))
          f_i.append(-c1_est[i,0]*p0_est*self.p_now[i+1,0]/(1.+c2_est[i,0]*p0_est)**2)
      
      H_mat = np.array([[d_i[0], f_i[0], 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, d_i[1], f_i[1], 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, d_i[2], f_i[2]]])
    #   self.persistent_mat = self.persistent_mat + np.matmul(H_mat, np.transpose(H_mat))
    #   print(np.linalg.det(self.persistent_mat))
      HP_mat = np.matmul(np.matmul(H_mat, P_mat_est),np.transpose(H_mat)) + R_mat
      K_gain = np.matmul(np.matmul(P_mat_est,np.transpose(H_mat)),np.linalg.inv(HP_mat))
      bending_segment = []
      for i in range(0,3):
          bending_segment.append((c1_est[i,0]*self.p_now[i+1,0])/(1.+c2_est[i,0]*p0_est))
      self.bending_est = np.array([[bending_segment[0]],[bending_segment[1]],[bending_segment[2]]])
      
      bending_output = np.transpose(np.array([self.bending]))
      if(len(bending_output)==len(self.bending_est)):
        self.state = state_est + np.matmul(K_gain,(bending_output-self.bending_est))

        self.P_mat = np.matmul((np.eye(6)-np.matmul(K_gain,H_mat)), P_mat_est)
    #   self.P_mat = np.matmul((np.eye(10)-np.matmul(K_gain,H_mat)), P_mat_est)
      
if __name__ == '__main__':
    try:
        bending_model()
    except rospy.ROSInterruptException: pass
