#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from sensor_msgs.msg import PointCloud2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class record_data(object):
  def __init__(self):
    rospy.init_node('record_data')
    self.num = rospy.get_param('~number')
    rec_pcd = rospy.Subscriber('/obstacle_point_kinect', PointCloud2, self.pcd_callback)
    # rec_pcd = rospy.Subscriber('/cloud_pcd', PointCloud2, self.pcd_callback)
    self.flag_record = 0
    rospy.spin()

  def pcd_callback(self, msg):
    pc = ros_numpy.numpify(msg)
    print pc
    points=np.zeros((pc.shape[0],3))
    print points
    points[:,0]=np.reshape(pc['x'], (np.product(pc['x'].shape),))
    points[:,1]=np.reshape(pc['y'], (np.product(pc['y'].shape),))
    points[:,2]=np.reshape(pc['z'], (np.product(pc['z'].shape),))
    print points

    if(self.flag_record == 0):
        # Saving to csv
        np.savetxt('data_all_'+str(self.num)+'.csv', points, delimiter=',')   # X is an array


        # # Plotting
        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # ax.scatter(points[:,0], points[:,1], points[:,2])
        # plt.show()

        self.flag_record = 1

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
