#!/usr/bin/env python
# The code is used to read the wrist's quarternion data from the Serial.
# Created by King's College London and Queen Mary University of London, 2017.

import roslib; roslib.load_manifest('inflatable_arm')
import math
import rospy
import serial
import string
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Int32
from sensor_msgs.msg import Joy
from sensor_msgs.msg import JointState

class read_wrist(object):
  def __init__(self):
    # Initialize the ROS Node
    rospy.init_node('tension_sent')

    rec_joy = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    # rec_joint = rospy.Subscriber('/agent/joint', JointState, self.joint_callback)
    length_dot_pub = rospy.Publisher('/tension_desired', Float64MultiArray, queue_size = 10)
    press_pub = rospy.Publisher('/pressure_status', Int32, queue_size = 10)
    self.flag_joy = 0
    self.tension_desired = []
    self.tension_desired_sent = Float64MultiArray()
    self.press_stat_sent = Int32()
    for i in range(0,1):
        self.tension_desired.append(0)
    self.press_stat_sent = 0
    self.delta_t = 0.1
    # Set the rate
    freq = 10.0
    dt = 1.0 / freq
    r = rospy.Rate(freq)
    # a_var = 1
    while not rospy.is_shutdown():
        self.tension_desired_sent.data = []
        for i in range(0,1):
            self.tension_desired_sent.data.append(self.tension_desired[i])
        # ser.write(a_var)
        press_pub.publish(self.press_stat_sent)
        length_dot_pub.publish(self.tension_desired_sent)
        r.sleep()

    # ser.close()

  def ps3_callback(self,msg):
    self.flag_joy = 1
    if(msg.axes[0]!=0):
        self.tension_desired[0] = self.tension_desired[0] + msg.axes[0]*self.delta_t
    # if(msg.axes[1]!=0):
    #     self.pressure_target[2] = self.pressure_target[2] + msg.axes[1]*self.delta_l
    # if(msg.axes[3]!=0):
    #     self.pressure_target[1] = self.pressure_target[1] + msg.axes[3]*self.delta_l
    if(msg.axes[4]==1):
        self.press_stat_sent = 1
    elif(msg.axes[4]==-1):
        self.press_stat_sent = 0


if __name__ == '__main__':
    try:
        read_wrist()
    except rospy.ROSInterruptException: pass
