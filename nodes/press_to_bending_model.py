#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
import numpy as np
# from keras.models import model_from_yaml
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Joy
import rospkg
import math
from inflatable_arm.msg import inflatable_msg
from scipy import optimize

class bending_model(object):
  def __init__(self):
    rospy.init_node('bending_model')
    self.model = rospy.get_param('/model')
    self.saturation = rospy.get_param('/saturated')
    self.angle_control = rospy.get_param('/angle_control')
    self.inv_kin = rospy.get_param('/inverse_kinematics')
    self.speed_on = rospy.get_param('/speed_level')
    self.tracking = rospy.get_param('/tracking')
    self.kalman_on = rospy.get_param('/kalman_on')
    self.recording = rospy.get_param('/recording')
    self.demoing = rospy.get_param('/demoing')
    self.experiment = rospy.get_param('/experiment')
    self.noisy = rospy.get_param('/noisy')
    self.eversion_on = rospy.get_param('/eversion_on')
    self.bend_number = rospy.get_param('/bend_number')
    self.inverse_distal = rospy.get_param('/inverse_distal')
    self.inverse_growth = rospy.get_param('/inverse_growth')
    self.bending_inverse = rospy.get_param('/bending_inverse')

    self.pub_bending = rospy.Publisher('/bending', Float64MultiArray, queue_size = 10)
    self.pub_pressure_rate = rospy.Publisher('/pressure_rate', Float64MultiArray, queue_size = 10)
    if(self.recording == 0):
        self.pub_pressure = rospy.Publisher('/pressure', Float64MultiArray, queue_size = 10)
    else:
        rec_pressure = rospy.Subscriber('/pressure', Float64MultiArray, self.press_callback)
    if(self.eversion_on==1):
        self.pub_length = rospy.Publisher('/length', Float64MultiArray, queue_size = 10)
    self.pub_pose = rospy.Publisher('/tip_pose', PoseArray, queue_size = 10)
    self.pub_twist = rospy.Publisher('/tip_twist', Vector3Stamped, queue_size = 10)
    self.pub_goal = rospy.Publisher('/goal_pose', Pose, queue_size = 10)
    self.pub_param = rospy.Publisher('/ls_param', Float64MultiArray, queue_size = 10)
    pub_goal_kinect = rospy.Publisher('/goal_kinect', Pose, queue_size = 10)
    pub_jacobi = rospy.Publisher('/jacobi_error', Float64MultiArray, queue_size = 10)
    rec_joy = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    rec_state = rospy.Subscriber('/states', Float64MultiArray, self.state_callback)
    # rec_goal_kinect = rospy.Subscriber('/goal_kinect', Pose, self.goal_kinect_callback)
    rec_pose_kinect = rospy.Subscriber('/kinect_to_data', inflatable_msg, self.pose_kinect_callback)
    rec_force = rospy.Subscriber('/force_obs', Vector3, self.obs_callback)
    self.force_obs = np.array([[0.], [0.]])
    rospack = rospkg.RosPack()
    yaml_name = rospack.get_path('inflatable_arm') + '/nodes/wormbot_bending_NN.yaml'
    h5_name = rospack.get_path('inflatable_arm') + '/nodes/wormbot_bending_NN.h5'

    if(self.model == 1):
        # # load YAML and create model
        # yaml_file = open(yaml_name, 'r')
        # loaded_model_yaml = yaml_file.read()
        # yaml_file.close()
        # self.loaded_model = model_from_yaml(loaded_model_yaml)
        # self.loaded_model.load_weights(h5_name)

        # self.ls_param = [0.0, 0.0, 0.0]
        print "Machine Learning Model"
    elif(self.model == 0):
        if(self.saturation == 0):
            ls_name = rospack.get_path('inflatable_arm') + '/nodes/least_square_param_new.csv'
            self.ls_param = np.genfromtxt(ls_name, delimiter=',')
            ls_name = rospack.get_path('inflatable_arm') + '/nodes/least_square_param_1st_segment.csv'
            self.ls_param_first = np.genfromtxt(ls_name, delimiter=',')
            ls_name = rospack.get_path('inflatable_arm') + '/nodes/least_square_param_3rd_segment.csv'
            self.ls_param_third = np.genfromtxt(ls_name, delimiter=',')
            print "Least Square Model no Saturation"
        else:
            ls_name = rospack.get_path('inflatable_arm') + '/nodes/least_square_param_2nd_segment_saturation.csv'
            self.ls_param = np.genfromtxt(ls_name, delimiter=',')
            ls_name = rospack.get_path('inflatable_arm') + '/nodes/least_square_param_1st_segment_saturation.csv'
            self.ls_param_first = np.genfromtxt(ls_name, delimiter=',')
            ls_name = rospack.get_path('inflatable_arm') + '/nodes/least_square_param_3rd_segment_saturation.csv'
            self.ls_param_third = np.genfromtxt(ls_name, delimiter=',')
            print "Least Square Model with Saturation"
    else:
        rad = 0.045
        l0 = 0.05
        D = 0.13
        diam = 0.1
        L = 0.4
        self.param = [math.pi*rad**2/(2*l0*D), L/diam]
        
    # freq = 100.0
    freq = 50
    self.delta_t = 1.0 / freq
    r = rospy.Rate(freq)
    self.l0 = 0.1
    self.len_dot_max = 0.1
    self.delta_l0 = 0.0
    self.delta_delta_l0 = 0.05
    self.grow = 0
    link_length = 0.1
    self.bend_section_max = 0.42
    
    if(self.eversion_on == 1):
        self.bend_number = int(math.floor(self.l0 / self.bend_section_max))+1
    self.link = []
    for i in range(0,self.bend_number):
        if(i!=(self.bend_number-1)):
            self.link.append(link_length)
        else:
            self.link.append(0.0)
    self.bend_section = []
    for i in range(0,self.bend_number):
        if(i!=(self.bend_number-1)):
            self.bend_section.append(self.bend_section_max)
        else:
            if(self.eversion_on == 1):
                self.bend_section.append(self.l0-(self.bend_number-1)*self.bend_section_max)
            else:
                self.bend_section.append(self.bend_section_max)
    rospy.set_param('bend_number', self.bend_number)
    self.bend_number_save = self.bend_number
    # self.p_in = [0.0, 0. , 0.01 , 0.01, 0., 0.0, 0.01]
    self.p_in = []
    for i in range(0,2*self.bend_number+1):
        self.p_in.append(0.0)
    # if(self.experiment == 0):
    #     # self.p_in = [0.6, 0.3 , 0.0, 0.0, 0.3, 0.3, 0.0]
    #     self.p_in = [0.6, 1. , 0.0, 0.0, 1., 1., 0.0]
    # else:
    #     self.p_in = [1.2, 0.6 , 0.0, 0.0, 0.6, 0.6, 0.0]

    # self.p_in = [0.3, 0.3 , 0.0 , 0.0, 0.3, 0.3, 0.]
    # self.p_in = [0.3, 0. , 0. , 0., 0.]
    self.p_change = np.zeros(((len(self.p_in)-1)/2,1))
    self.dp = 0.05
    # self.link = [0.1, 0.43, 0.0]
    self.p_dot = np.zeros(((len(self.p_in)-1)/2,1))
    self.p0_dot = 0.0
    self.bending_sent = Float64MultiArray()
    self.pressure_sent = Float64MultiArray()
    self.pressure_rate_sent = Float64MultiArray()
    self.pose_sent = PoseArray()
    self.twist_sent = Vector3Stamped()
    self.goal_sent = Pose()
    self.goal_kinect_sent = Pose()
    jacobi_sent = Float64MultiArray()
    self.length_sent = Float64MultiArray()

    self.ls_param_sent = Float64MultiArray()
    self.ls_param_sent.data = []
    if(self.model==0):
        self.ls_param_sent.data.append(self.ls_param_first[0])
        self.ls_param_sent.data.append(self.ls_param_first[1])
        self.ls_param_sent.data.append(self.ls_param[0])
        self.ls_param_sent.data.append(self.ls_param[1])
        self.ls_param_sent.data.append(self.ls_param_third[0])
        self.ls_param_sent.data.append(self.ls_param_third[1])


    flag_goal = 0

    # Goal init
    self.goal = np.array([[0.], [0.]])
    # self.goal_kinect = np.array([[7.0], [-334.0]])
    self.goal_kinect = np.array([[-136.0], [-483.0]])
    self.pose_kinect = np.array([[0.], [0.]])
    self.speed_goal = np.array([[0.], [0.]])
    self.goal_angle = np.array([[0.]])
    self.bending_goal = np.array([[0.0],[0.0],[0.0]])

    # Control
    if(self.experiment==0):
        self.KP = 5.
        # self.KP = 0.1
    else:
        if(self.kalman_on == 1):
            self.KP = 0.1
        else:
            self.KP = 0.1
    self.NOT_PUBLISH = 0
    self.PUBLISH = 1
    self.NOT_ESTIMATE = 0
    self.ESTIMATE = 1
    self.WITH_NOISE = 1
    self.NO_NOISE = 0
    self.moving_flag = 0
    self.on1 = self.on2 = self.on3 = 0
    self.active_segment = 0
    rospy.set_param('active_segment', self.active_segment)

    ## Kalman
    self.index_i = 0
    self.index_i_central = 0
    self.index_j = 0
    self.state_est_i = self.state_est_j = 0
    self.index_ = 0
    self.state_flag = 0
    self.flag_estimation = 0
    self.state_training = 0
    self.c1_est = np.zeros((int((len(self.p_in)-1)/2),1))
    self.c2_est = np.zeros((int((len(self.p_in)-1)/2),1))
    rospy.set_param('start_estimate', self.flag_estimation)
    rospy.sleep(5)

    self.starting = rospy.get_time()
    while not rospy.is_shutdown():
        if(self.eversion_on == 1):
            self.bend_number = int(math.floor(self.l0 / self.bend_section_max))+1
            self.link = []
            for i in range(0,self.bend_number):
                if(i!=(self.bend_number-1)):
                    self.link.append(link_length)
                else:
                    self.link.append(0.0)
            self.bend_section = []
            for i in range(0,self.bend_number):
                if(i!=(self.bend_number-1)):
                    self.bend_section.append(self.bend_section_max)
                else:
                    if(self.eversion_on == 1):
                        self.bend_section.append(self.l0%self.bend_section_max)
                    else:
                        self.bend_section.append(self.bend_section_max)
            if(self.bend_number>self.bend_number_save):
                for i in range(0,2):
                    self.p_in.append(0.0)
            elif(self.bend_number<self.bend_number_save):
                for i in range(0,2):
                    del self.p_in[-1]
            self.p_change = np.zeros(((len(self.p_in)-1)/2,1))
            if(self.active_segment==self.bend_number):
                self.active_segment = self.bend_number-1
            self.bend_number_save = self.bend_number
            rospy.set_param('bend_number', self.bend_number)

            self.l0 = self.l0 + self.delta_l0*self.delta_t
            if(self.l0<0.1):
                self.l0=0.1
        if(self.inverse_distal == 0):
            self.p_active = self.p_in
            self.active_inv = self.bend_number
        else:
            self.active_inv = 2
            if(self.bend_number>=self.active_inv):
                self.p_active = []
                self.p_active.append(self.p_in[0])
                for i in range(0, 2*self.active_inv):
                    self.p_active.append(self.p_in[2*self.bend_number-3+i])
            
                print "p"
                print self.p_active
                print self.p_in
        rospy.set_param('start_estimate', self.flag_estimation)
        rospy.set_param('active_segment', self.active_segment)
        rospy.set_param('/inverse_kinematics', self.inv_kin)
        if(self.demoing == 0):
            if(self.inv_kin == 1):
                # print self.pose_kinect
                # print self.goal_kinect
                if(self.moving_flag == 1):
                    if(self.kalman_on==0):
                        if(self.bending_inverse==0):
                            self.p_active = self.inverse_kinematics(self.NOT_ESTIMATE, self.p_active)
                        else:
                            self.p_active = self.bending_inverse_kinematics(self.NOT_ESTIMATE, self.p_active)
                    else:
                        if(self.bending_inverse==0):
                            self.p_active = self.inverse_kinematics(self.ESTIMATE, self.p_active)
                        else:
                            self.p_active = self.bending_inverse_kinematics(self.ESTIMATE, self.p_active)
                    if(self.inverse_distal == 1):
                        for i in range(0, 2*self.active_inv):
                            self.p_in[2*self.bend_number-(2*self.active_inv-1)+i] = self.p_active[i+1]
                    else:
                        self.p_in = self.p_active
            else:
                self.forward_velocity()
        else:
            self.demo()
            
        tip_pose = self.forward_kinematics(self.p_in, self.PUBLISH, self.NOT_ESTIMATE, self.WITH_NOISE)

        if(flag_goal == 0):
            flag_goal = 1
            self.goal[0,0] = tip_pose[0,3]
            self.goal[1,0] = tip_pose[1,3]
        if(self.tracking == 1):
            self.update_goal()

        if(self.state_flag == 1):
            jacobi = np.array(self.jacobian_calculate(self.p_in, self.ESTIMATE, self.NO_NOISE))
            jacobi_real = np.array(self.jacobian_calculate(self.p_in, self.NOT_ESTIMATE, self.NO_NOISE))
            rmse = (((jacobi_real - jacobi)**2).mean())**0.5
            jacobi_error = jacobi_real - jacobi
            jacobi_sent.data = []
            for i in range(0, jacobi_error.shape[0]):
                for j in range(0, jacobi_error.shape[1]):
                    jacobi_sent.data.append(jacobi_error[i,j])
            pub_jacobi.publish(jacobi_sent)
            # print rmse
        # Send data
        if(self.experiment == 0):        
            self.pub_bending.publish(self.bending_sent)
        elif(self.experiment == 1 and self.kalman_on==0):
            self.pub_bending.publish(self.bending_sent)
            self.goal_kinect_sent.position.x = self.goal_kinect[0,0]
            self.goal_kinect_sent.position.y = self.goal_kinect[1,0]
        elif(self.experiment == 1 and self.kalman_on==1):
            self.goal_kinect_sent.position.x = self.goal_kinect[0,0]
            self.goal_kinect_sent.position.y = self.goal_kinect[1,0]
        if(self.eversion_on==1):
            self.length_sent.data = []
            self.length_sent.data.append(self.l0)
            self.pub_length.publish(self.length_sent)
        if(self.recording == 0):
            self.pressure_sent.data = []
            for i in range(0,len(self.p_in)):
                self.pressure_sent.data.append(self.p_in[i])
            # # If the main chamber is not connected
            # for i in range(0,len(self.p_in)-1):
            #     self.pressure_sent.data.append(self.p_in[i+1])
            self.pub_pressure.publish(self.pressure_sent)
        quat_goal = self.euler_to_quaternion(0.,0.,self.goal_angle[0,0])
        self.goal_sent.position.x = self.goal[0,0]
        self.goal_sent.position.y = self.goal[1,0]
        self.goal_sent.orientation.x = quat_goal[0]
        self.goal_sent.orientation.y = quat_goal[1]
        self.goal_sent.orientation.z = quat_goal[2]
        self.goal_sent.orientation.w = quat_goal[3]
        
        self.pub_pose.publish(self.pose_sent)
        self.pub_twist.publish(self.twist_sent)
        self.pub_goal.publish(self.goal_sent)
        pub_goal_kinect.publish(self.goal_kinect_sent)
        self.pub_pressure_rate.publish(self.pressure_rate_sent)
        self.pub_param.publish(self.ls_param_sent)
        # print self.ls_param
        # self.pressure_rate_sent.data = []
        # for i in range(0, len(self.p_dot)+1):
        #     if(i==0):
        #         self.pressure_rate_sent.data.append(self.p0_dot)
        #         # self.p0_dot = 0.0
        #     else:
        #         self.pressure_rate_sent.data.append(self.p_dot[i-1,0])


        r.sleep()

  def ps3_callback(self,msg):
    ## NOTES ON KEYBOARD
    # f, gc to control eversion speed
    # a, d, w, s to move goal in Inverse Kinematics
    # w, s to change pouch pressure in Forward Kinematics
    # k, l to change inflation pressure
    # c, v to change active segment in Forward Kinematics
    # b Reset
    # m Moving
    # u Switch Kinematics mode
    # n start/stop estimation

    if(msg.buttons[3] == 1):
        if(self.moving_flag == 0):
            self.moving_flag = 1
        else:
            self.moving_flag = 0
    if(msg.buttons[8] == 1):
        self.inv_kin = 0
        self.p_in = [0.0, 0. , 0.0 , 0.0, 0., 0.0, 0.0]
    
    if(self.demoing == 0):
        if(self.inv_kin == 1 and self.tracking == 0):
            if(self.bending_inverse==0):
                self.goal[0,0] = self.goal[0,0] + msg.axes[0]*(-0.01)
                self.goal[1,0] = self.goal[1,0] + msg.axes[1]*0.01
                if(self.experiment==1):
                    self.goal_kinect[0,0] = self.goal_kinect[0,0] + msg.axes[0]*(-1)
                    self.goal_kinect[1,0] = self.goal_kinect[1,0] + msg.axes[1]
                self.goal_angle[0,0] = self.goal_angle[0,0] + msg.axes[3]*0.01
            else:
                self.bending_goal[0,0] = self.bending_goal[0,0] + msg.axes[0]*30.0*3.14/180.0
                self.bending_goal[1,0] = self.bending_goal[1,0] + msg.axes[1]*30.0*3.14/180.0
                self.bending_goal[2,0] = self.bending_goal[2,0] + msg.axes[3]*30.0*3.14/180.0
            # self.p_in[0] = self.p_in[0] + msg.axes[2]*self.dp
            self.p_in[0] = self.p_in[0] + (msg.axes[4]-msg.axes[5])*self.dp
            self.p0_dot = msg.axes[2]*self.dp/self.delta_t
        else:
            if(self.speed_on==1):
                self.p_dot[0,0] = self.p_dot[0,0] + msg.axes[1]*self.dp
                self.p_dot[1,0] = self.p_dot[1,0] + msg.axes[3]*self.dp
                self.p_dot[2,0] = self.p_dot[2,0] + msg.axes[0]*self.dp
                # self.p0_dot = self.p0_dot + msg.axes[2]*self.dp
                self.p_in[0] = self.p_in[0] + (msg.axes[4]-msg.axes[5])*self.dp
            else:
                self.p_change[self.active_segment,0] = -1*msg.axes[1]*self.dp
                # self.p_change[1,0] = msg.axes[3]*self.dp
                # self.p_change[2,0] = msg.axes[7]*self.dp
                self.p_in[0] = self.p_in[0] + (msg.axes[4]-msg.axes[5])*self.dp
                if(self.p_in[0]<0):
                    self.p_in[0]=0
                if(self.eversion_on == 1):
                    self.l0 = self.l0 + (msg.buttons[4]-msg.buttons[5])*0.05
                    
                    if(msg.axes[7]>0):
                        self.delta_l0 = self.delta_l0 + self.delta_delta_l0
                    elif(msg.axes[7]<0):
                        self.delta_l0 = self.delta_l0 - self.delta_delta_l0
                
                self.active_segment = self.active_segment - int(msg.axes[6])
                if(self.active_segment>(self.bend_number-1)):
                    self.active_segment = 0
                if(self.active_segment<0):
                    self.active_segment = self.bend_number-1
                # self.p_in[0] = self.p_in[0] + msg.axes[2]*self.dp
                # self.p0_dot = msg.axes[2]*self.dp/self.delta_t
        # if(msg.axes[5]==1):
        if(msg.buttons[1]==1):
            if(self.flag_estimation == 0):
                self.flag_estimation = 1
            else:
                self.flag_estimation = 0
        if(msg.buttons[2]==1):
            if(self.tracking == 0):
                self.tracking = 1
                self.p_in = [0.6, 0.3 , 0.0, 0.0, 0.3, 0.3, 0.0]
            else:
                self.tracking = 0
        if(msg.buttons[0]==1):
            if(self.inv_kin == 0):
                self.inv_kin = 1
            else:
                self.inv_kin = 0

            print self.inv_kin
    else:
        if(msg.axes[0]==-1):
            self.on1 = 1
        if(msg.axes[1]==-1):
            self.on2 = 1
        if(msg.axes[0]==1):
            self.on3 = 1

  def state_callback(self,msg):
    self.state_flag = 1
    # if(self.inv_kin == 0):
    for i in range(0,int((len(self.p_in)-1)/2)):
        self.c1_est[i,0] = msg.data[len(msg.data)-6+i*2]
        self.c2_est[i,0] = msg.data[len(msg.data)-5+i*2]
    # print "a"
    # print self.c1_est
    # print self.c2_est

#   def goal_kinect_callback(self,msg):
#     self.goal_kinect[0,0] = msg.position.x
#     self.goal_kinect[1,0] = msg.position.y

  def pose_kinect_callback(self,msg):
    self.pose_kinect[0,0] = msg.tip.poses[0].position.x
    self.pose_kinect[1,0] = msg.tip.poses[0].position.y

  def obs_callback(self,msg):
    self.force_obs[0,0] = msg.x
    self.force_obs[1,0] = msg.y
    
  def press_callback(self,msg):
    self.p_in = [msg.data[0], msg.data[1], msg.data[2], msg.data[3], msg.data[4], 0.0, 0.0]

  def euler_to_quaternion(self, roll, pitch, yaw):

        qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
        qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
        qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
        qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)

        return [qx, qy, qz, qw]

  def forward_velocity(self):
      # Mapping pressure
      p_now = np.empty(((len(self.p_in)-1)/2,1), float)

      for i in range(0,(len(self.p_in)-1)/2):
          if(self.p_in[1+2*i]<=0.001 and self.p_in[2+2*i]>0.001):
              p_now[i,0] = -1.*self.p_in[2+2*i]
          else:
              p_now[i,0] = self.p_in[1+2*i]

      if(self.kalman_on == 0):
        if(self.speed_on==1):
            p_now = p_now + self.p_dot*self.delta_t
            self.p_in[0] = self.p_in[0] + self.p0_dot*self.delta_t
        else:
            p_now = p_now + self.p_change
            self.p_change = np.zeros(((len(self.p_in)-1)/2,1))
      else:
          if(self.flag_estimation==1):
            if(self.experiment == 0):
                # len_data = 50
                # self.p_in[0] = 0.3 + 0.2*math.sin(2*3.14*self.index_i/float(len_data))
                len_data = 500
                index_central = 4
                # self.p_in[0] = 0.6 + 0.4*math.sin(2*3.14*self.index_i/float(len_data))
                # self.p_in[0] = 0.6 + 0.4*math.cos(2*3.14*self.index_i/float(len_data))
                # p_now[1,0] = 1.0*math.sin(2*3.14*self.index_j/float(len_data))
                # len_data = 200
                self.p_in[0] = 0.3# + 0.4*math.cos(2*3.14*self.index_i/float(len_data))
                # p_now[0,0] = 1.5
                # p_now[1,0] = -1.5
                # p_now[2,0] = 1.5
                p_now[0,0] = 0.3#*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
                p_now[1,0] = -0.3#*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
                p_now[2,0] = 0.3#*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
            else:
                len_data = 5000
                index_central = 1
                # self.p_in[0] = 0.9 + 0.3*math.cos(2*3.14*self.index_i/float(len_data))
                # # p_now[0,0] = 1.5
                # # p_now[1,0] = -1.5
                # # p_now[2,0] = 1.5
                self.p_in[0] = 0.3
                # p_now[0,0] = 1.0*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
                # p_now[1,0] = 1.0*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
                # p_now[2,0] = 1.0*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
                p_now[0,0] = 0.3#*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
                p_now[1,0] = -0.3#*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
                p_now[2,0] = 0.3#*math.sin(2*3.14*self.index_i_central/float(index_central*len_data))
            #   self.index_ = self.index_ + 1
            #   if(self.state_est_i == 0):
            #     self.p_in[0] = 0.1 + self.index_i*(0.6-0.1)/30.0
                
            #   else:
            #       self.p_in[0] = 0.6 - self.index_i*(0.6-0.1)/30.0
            
            #   if(self.state_est_j == 0):
            #       for i in range(0, 1):
            #         p_now[i,0] = -1.0 + 2*self.index_j/30.0
            #   else:
            #       for i in range(0, 1):
            #         p_now[i,0] = 1.0 - 2*self.index_j/30.0

            self.index_i = self.index_i + 1
            self.index_i_central = self.index_i_central + 1
            if(self.index_i_central>=index_central*len_data):
                self.index_i_central = 0
            if(self.index_i>=len_data):
                if(self.state_training == 0):
                    self.state_training = 1
                else:
                    self.state_training = 0
                self.index_i = 0
                self.index_j = self.index_j + 1
                if(self.index_j>=len_data):
                    self.index_j = 0
                    if(self.state_est_j == 0):
                        self.state_est_j = 1
                    else:
                        self.state_est_j = 0
                if(self.state_est_i == 0):
                    self.state_est_i = 1
                else:
                    self.state_est_i = 0
          else:
            p_now = p_now + self.p_change
            self.p_change = np.zeros(((len(self.p_in)-1)/2,1))
      # Remap pressure
      for i in range(0,(len(self.p_in)-1)/2):
          if( p_now[i,0]<0.):
              self.p_in[2+2*i] = -1.*p_now[i,0]
              self.p_in[1+2*i] = 0.
          else:
              self.p_in[2+2*i] = 0.
              self.p_in[1+2*i] = p_now[i,0]

  def demo(self):
      # Mapping pressure
      p_now = np.empty(((len(self.p_in)-1)/2,1), float)
      omega_goal = 2*3.14/30.0
      now = rospy.get_time()

      for i in range(0,(len(self.p_in)-1)/2):
          if(self.p_in[1+2*i]<=0.001 and self.p_in[2+2*i]>0.001):
              p_now[i,0] = -1.*self.p_in[2+2*i]
          else:
              p_now[i,0] = self.p_in[1+2*i]


      # p_now[0,0] = 1.0*np.sin(omega_goal*(now - self.starting))
      # p_now[1,0] = 1.0*np.cos(omega_goal*(now - self.starting))
      if(self.on1==1):
          p_now[0,0] = 1.0
      else:
          p_now[0,0] = 0.0
      if(self.on2==1):
          p_now[1,0] = -1.0
      else:
          p_now[1,0] = 0.0
      if(self.on3==1):
          p_now[2,0] = 1.0
      else:
          p_now[2,0] = 0.0

      # Remap pressure
      for i in range(0,(len(self.p_in)-1)/2):
          if( p_now[i,0]<0.):
              self.p_in[2+2*i] = -1.*p_now[i,0]
              self.p_in[1+2*i] = 0.
          else:
              self.p_in[2+2*i] = 0.
              self.p_in[1+2*i] = p_now[i,0]

  def update_goal(self):
    omega_goal = 2*3.14/30.0
    now = rospy.get_time()


    if(self.angle_control == 0):
        # Straight Line X
        # amp = [0.25, 0.98]
        amp = [0.3, 1.40]
        # amp = [0.25, 0.88]
        # amp = [0.35, 1.4]

        self.goal[0] = amp[0]*np.sin(omega_goal*(now - self.starting))
        self.goal[1] = amp[1]
        self.speed_goal[0] = amp[0]*omega_goal*np.cos(omega_goal*(now - self.starting))
        self.speed_goal[1] = 0.
    else:
        # amp = [0.01, 1.38]
        # self.goal[0] = amp[0]
        # self.goal[1] = amp[1]
        # self.goal_angle[0,0] = 0.3*(1+np.sin(omega_goal*(now - self.starting)))

        amp = [0.35, 1.4]

        self.goal[0] = amp[0]/2*(np.sin(omega_goal*(now - self.starting))-1)
        self.goal[1] = amp[1]
        self.speed_goal[0] = amp[0]*omega_goal*np.cos(omega_goal*(now - self.starting))
        self.speed_goal[1] = 0.
        # self.goal[0] = -amp[0]/2
        # self.goal[1] = amp[1]
        # self.speed_goal[0] = 0.
        # self.speed_goal[1] = 0.
        self.goal_angle[0,0] = 0.0#0.3*(1+np.sin(omega_goal*(now - self.starting)))
        

        # amp = [0.25, 1.38]
        # self.goal[0] = amp[0]*np.sin(omega_goal*(now - self.starting))
        # self.goal[1] = amp[1]
        # self.speed_goal[0] = amp[0]*omega_goal*np.cos(omega_goal*(now - self.starting))
        # self.speed_goal[1] = 0.
        # self.goal_angle[0,0] = 0.0

    # Straight Line XY
    # amp = [0.3, 0.05]
    # offset = [0.3, 0.9
    # self.goal[0] = amp[0]
    # self.goal[1] = offset+amp[1]*np.sin(omega_goal*(now - self.starting))
    # self.speed_goal[0] = 0.
    # self.speed_goal[1] = amp[1]*omega_goal*np.cos(omega_goal*(now - self.starting))

    # # Ellipse
    # amp = [0.15, 0.08]
    # offset = 0.9
    # self.goal[0] = amp[0]*np.sin(omega_goal*(now - self.starting))
    # self.goal[1] = offset+amp[1]*np.cos(omega_goal*(now - self.starting))
    # self.speed_goal[0] = amp[0]*omega_goal*np.cos(omega_goal*(now - self.starting))
    # self.speed_goal[1] = -amp[1]*omega_goal*np.sin(omega_goal*(now - self.starting))


  def least_square(self,input, bend_index):
    p0 = input[0,0]
    if(input[0,1]<=0.001 and input[0,2]>0.001):
        p1 = -1.*input[0,2]
    else:
        p1 = input[0,1]

    e0 = 1
    if(bend_index == 0):
        c1 = self.ls_param_first[0]
        c2 = self.ls_param_first[1]
    elif(bend_index == 1):
        c1 = self.ls_param[0]
        c2 = self.ls_param[1]
    else:
        c1 = self.ls_param_third[0]
        c2 = self.ls_param_third[1]
    # print c1
    # print c2
    out = np.array([[(c1*p1)/(e0+c2*p0)]])

    return out

  def least_square_est(self,input, bend_index):
    p0 = input[0,0]
    if(input[0,1]<=0.001 and input[0,2]>0.001):
        p1 = -1.*input[0,2]
    else:
        p1 = input[0,1]

    # Small one
    c1 = self.c1_est[bend_index]
    c2 = self.c2_est[bend_index]
    e0 = 1.
    if(bend_index == 0):
        out = np.array([[(c1*p1)/(e0+c2*p0)]])
    else:
        out = np.array([[(c1*p1)/(e0+c2*p0)]])

    # Big one
    # [143.86842428   0.72053624   2.46253233]
    # c1 = 143.86842428
    # c2 = 0.72053624
    # c3 = 2.46253233

    # out = (c1*p1)/((1+c2*p0)*(1+abs(c3*p1)))

    return out

  def individual_bending(self, bending_angle, section_ind):
      pose = np.empty((3,1), float)

      if(bending_angle>0):
        #   pose[0] = - self.bend_section[section_ind]/bending_angle*(1-np.cos(bending_angle)) - self.link[section_ind]*np.sin(bending_angle)
        #   pose[1] = self.bend_section[section_ind]/bending_angle*np.sin(bending_angle) + self.link[section_ind]*np.cos(bending_angle)
          pose[0] = - self.bend_section[section_ind]*(bending_angle/2-bending_angle**3/(4*3*2)) - self.link[section_ind]*np.sin(bending_angle)
          pose[1] = self.bend_section[section_ind]*(1-bending_angle**2/(3*2)) + self.link[section_ind]*np.cos(bending_angle)
          pose[2] = 0.0
      elif(bending_angle<0):
          pose[0] = -self.bend_section[section_ind]*(bending_angle/2-bending_angle**3/(4*3*2)) + self.link[section_ind]*np.sin(abs(bending_angle))
          pose[1] = self.bend_section[section_ind]*(1-bending_angle**2/(3*2)) + self.link[section_ind]*np.cos(bending_angle)
          pose[2] = 0.0
      else:
          pose[0] = 0.0
          pose[1] = self.bend_section[section_ind] + self.link[section_ind]
          pose[2] = 0.0
      return pose

  def forward_kinematics(self, press, published, estimate, with_noise):
      bend_num = (len(press)-1)/2
      homo_matrix = np.eye(4)


      self.total_bending = 0.
      if(published):
          self.bending_sent.data = []
          self.pose_sent.poses = []
      for i in range(0,bend_num):
          # Get the bending
          p_in = [press[0], press[2*i+1], press[2*i+2]]
          # print i
          # print p_in
          input_model = np.array([p_in])
          if(self.model == 1):
              bending_np = self.loaded_model.predict(input_model)
          elif(self.model == 0):
              if(estimate == 0):
                  bending_np = self.least_square(input_model, i)
                  # print bending_np
                  bending_np = bending_np.reshape((1,))
              else:
                  # TEMPORARY!!!!
                #   if(i==1):
                  bending_np = self.least_square_est(input_model, i)
                #   else:
                #     bending_np = self.least_square(input_model, i)
                  # print bending_np
                  bending_np = bending_np.reshape((1,))
          else:
              f = lambda x, a: np.cos(x) - x*a
              fder = lambda x, a: -np.sin(x) - a
              if(input_model[0,1]<=0.001 and input_model[0,2]>0.001):
                cos_alpha_per_alpha = self.param[0]*input_model[0,0]/input_model[0,2]
                alpha = optimize.newton(f, 0.5, fprime=fder, args=(cos_alpha_per_alpha, ))
                theta = (-1)*self.param[1]*(1-np.sin(alpha)/alpha)
              elif(input_model[0,2]<=0.001 and input_model[0,1]>0.001):
                cos_alpha_per_alpha = self.param[0]*input_model[0,0]/input_model[0,1]
                alpha = optimize.newton(f, 0.5, fprime=fder, args=(cos_alpha_per_alpha, ))
                theta = self.param[1]*(1-np.sin(alpha)/alpha)
              else:
                theta = 0.0
              bending_np = np.array([theta*180.0/3.14])
              bending_np = bending_np.reshape((1,))

          if(self.eversion_on==1):
              bending_np = bending_np*self.bend_section[i]/self.bend_section_max

          if(with_noise == 1):
            if(self.noisy == 0):
                angle = bending_np[0]*3.14/180.0
                angle = angle.reshape((1,))
            else:
                # mu, sigma = 0, 0.08 # mean and standard deviation
                mu, sigma = 0, 0.1 # mean and standard deviation
                noises = np.random.normal(mu, sigma, 1)
                # print noises
                # print bending_np[0]
                angle = (bending_np[0]+noises)*3.14/180.0
                angle = angle.reshape((1,))
          else:
              angle = bending_np[0]*3.14/180.0
              angle = angle.reshape((1,))
          # Get the pose
          tip = self.individual_bending(angle, i)

          # Build the matrix
          mat_tip = np.array([[np.cos(angle)[0], -np.sin(angle)[0], 0., tip[0,0]], [np.sin(angle)[0], np.cos(angle)[0], 0., tip[1,0]], [0., 0., 1., tip[2,0]], [0., 0., 0., 1.]])

          homo_matrix = np.matmul(homo_matrix, mat_tip)

          self.total_bending = self.total_bending + angle[0]
          if(published == 1):
              if(self.noisy==0):
                self.bending_sent.data.append(bending_np[0])
              else:
                self.bending_sent.data.append(bending_np[0]+noises)

              pose_tip = Pose()
              pose_tip.position.x = homo_matrix[0,3]
              pose_tip.position.y = homo_matrix[1,3]
              pose_tip.position.z = homo_matrix[2,3]
              pose_tip.orientation.x = 0.0
              pose_tip.orientation.y = 0.0
              pose_tip.orientation.z = np.sin(0.5*self.total_bending)
              pose_tip.orientation.w = np.cos(0.5*self.total_bending)
              self.pose_sent.poses.append(pose_tip)


      return homo_matrix

  def homo_to_pose(self, homo):
      pose_return = np.array([[homo[0,3]], [homo[1,3]]])
      return pose_return

  def rotationMatrixToEulerAngles(self,R) :

    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])

    singular = sy < 1e-6

    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0

    return np.array([x, y, z])


  def jacobian_calculate(self, q_in, estimate, with_noise):
    q_out_cur = self.forward_kinematics(self.p_in, self.NOT_PUBLISH, estimate, with_noise)
    delta_p = 0.00001
    delta_l = 0.0001
    # delta_p = 1.0

    if(self.inverse_growth==0):
        jac=np.zeros((2,(len(q_in)-1)/2))
    else:
        jac=np.zeros((2,(len(q_in)-1)/2+1))
    if(self.inverse_growth==1):
        self.bend_section[self.bend_number-1] = self.bend_section[self.bend_number-1] + delta_l
        q_out_new = self.forward_kinematics(self.p_in, self.NOT_PUBLISH, estimate, with_noise)
        for j in range(0,2):
            jac[j,0] = (q_out_new[j,3]-q_out_cur[j,3])/delta_l
        self.bend_section[self.bend_number-1] = self.bend_section[self.bend_number-1] - delta_l

    for i in range(0,(len(q_in)-1)/2):
        if(q_in[1+2*i]<=0.001 and q_in[2+2*i]>0.001):
            q_in[2+2*i] = q_in[2+2*i] - delta_p
            q_in[1+2*i] = 0.
        else:
            q_in[1+2*i] = q_in[1+2*i] + delta_p
            q_in[2+2*i] = 0.
        p_in_update = self.p_in
        for j in range(0, 2*self.active_inv):
            p_in_update[2*self.bend_number-(2*self.active_inv-1)+j] = q_in[j+1]
        q_out_new = self.forward_kinematics(p_in_update, self.NOT_PUBLISH, estimate, with_noise)
        for j in range(0,2):
            if(self.inverse_growth == 0):
                jac[j,i] = (q_out_new[j,3]-q_out_cur[j,3])/delta_p
            else:
                jac[j,i+1] = (q_out_new[j,3]-q_out_cur[j,3])/delta_p
        if(q_in[1+2*i]<=0.001 and q_in[2+2*i]>0.001):
            q_in[2+2*i] = q_in[2+2*i] + delta_p
            q_in[1+2*i] = 0.
        else:
            q_in[1+2*i] = q_in[1+2*i] - delta_p
            q_in[2+2*i] = 0.

    if(self.angle_control == 1):
        bending_cur= self.rotationMatrixToEulerAngles(q_out_cur[0:3,0:3])[2]

        jac_angle=np.zeros((1,(len(q_in)-1)/2))
        for i in range(0,(len(q_in)-1)/2):
            if(q_in[1+2*i]<=0.001 and q_in[2+2*i]>0.001):
                q_in[2+2*i] = q_in[2+2*i] - delta_p
                q_in[1+2*i] = 0.
            else:
                q_in[1+2*i] = q_in[1+2*i] + delta_p
                q_in[2+2*i] = 0.
            p_in_update = self.p_in
            for j in range(0, 2*self.active_inv):
                p_in_update[2*self.bend_number-(2*self.active_inv-1)+j] = q_in[j+1]
            q_out_new = self.forward_kinematics(p_in_update, self.NOT_PUBLISH, estimate, with_noise)
            bending_new = self.rotationMatrixToEulerAngles(q_out_new[0:3,0:3])[2]
            jac_angle[0,i] = (bending_new-bending_cur)/delta_p
            if(q_in[1+2*i]<=0.001 and q_in[2+2*i]>0.001):
                q_in[2+2*i] = q_in[2+2*i] + delta_p
                q_in[1+2*i] = 0.
            else:
                q_in[1+2*i] = q_in[1+2*i] - delta_p
                q_in[2+2*i] = 0.
        jac = np.vstack([jac, jac_angle])
        # print jac
    return jac

  def inverse_kinematics(self, estimate, p_inv):
      # Mapping pressure
      p_now = np.empty(((len(p_inv)-1)/2,1), float)

      for i in range(0,(len(p_inv)-1)/2):
          if(p_inv[1+2*i]<=0.001 and p_inv[2+2*i]>0.001):
              p_now[i,0] = -1.*p_inv[2+2*i]
          else:
              p_now[i,0] = p_inv[1+2*i]
      # print "tes"
      # print p_now

      # Compute jacobian and inverse jacobian
      # Standard use
      jacobi = np.array(self.jacobian_calculate(p_inv, estimate, self.NO_NOISE))
      # Noisy Jacobian
    #   jacobi = np.array(self.jacobian_calculate(p_inv, estimate, self.WITH_NOISE))

      # v = np.array([[0.0], [0.0]])
      print "a"
      print self.pose_kinect
      print self.goal_kinect
      
      if(self.tracking == 0):
          if(self.experiment == 0):
            v = -self.KP*(self.homo_to_pose(self.forward_kinematics(self.p_in, self.NOT_PUBLISH, estimate, self.WITH_NOISE))-self.goal) + self.force_obs
          else:
            v = -self.KP*0.01*(self.pose_kinect-self.goal_kinect)
            v[1,0] = -1*v[1,0]
            print v
      else:
          v = -self.KP*(self.homo_to_pose(self.forward_kinematics(self.p_in, self.NOT_PUBLISH, estimate, self.WITH_NOISE))-self.goal) + self.speed_goal
      if(self.angle_control == 1):
          # # Standard control
          # omega = -self.KP*(self.total_bending - self.goal_angle)
          # print "a"
          # print self.total_bending
          # print self.goal_angle

          # Geometric control
          R_1 = np.array([[np.cos(self.total_bending), -np.sin(self.total_bending), 0.], [np.sin(self.total_bending), np.cos(self.total_bending), 0.], [0., 0., 1.]])
          R_2 = np.array([[np.cos(self.goal_angle)[0,0], -np.sin(self.goal_angle)[0,0], 0.], [np.sin(self.goal_angle)[0,0], np.cos(self.goal_angle)[0,0], 0.], [0., 0., 1.]])
          R_mat = np.matmul(np.transpose(R_2),R_1)
          cos_theta = (0.5*((R_mat.trace())-1))
          if(cos_theta>0.9999999):
              cos_theta = 0.9999999
          elif(cos_theta<-0.9999999):
              cos_theta = -0.9999999
          theta = np.arccos(cos_theta)
          if(np.sin(theta)!=0):
              out = (theta/np.sin(theta))*(R_mat - R_mat.transpose())
          else:
              out = (R_mat - R_mat.transpose())
          omega_so3 = -self.KP*out
          omega_3 = np.matmul(R_2,np.array([[omega_so3[2,1]],[omega_so3[0,2]],[omega_so3[1,0]]]))
          omega = omega_3[2,0]
          # print omega

          # Concatenate vector
          v = np.vstack([v, omega])
      if(self.inverse_growth==0):
        if(jacobi.shape[0] == jacobi.shape[1]):
            self.p_dot = np.matmul(np.linalg.inv(jacobi), v)
        else:
            self.p_dot = np.matmul(np.linalg.pinv(jacobi), v)
        p_now = p_now + self.p_dot*self.delta_t
        act_dot_real = self.p_dot
      else:
        if(jacobi.shape[0] == jacobi.shape[1]):
            act_dot = np.matmul(np.linalg.inv(jacobi), v)
        else:
            act_dot = np.matmul(np.linalg.pinv(jacobi), v)
        act_dot_real = act_dot
        if(abs(act_dot[0,0])<self.len_dot_max):
            self.l0 = self.l0 + act_dot[0,0]*self.delta_t
        else:
            self.l0 = self.l0 + (act_dot[0,0]*self.len_dot_max/abs(act_dot[0,0]))*self.delta_t
            act_dot_real[0,0] = act_dot[0,0]*self.len_dot_max/abs(act_dot[0,0])
        self.p_dot = act_dot[1:2*self.bend_number,0]
        p_now = p_now + self.p_dot*self.delta_t

      # Calculate twist
      twist = np.matmul(jacobi, act_dot_real)
      self.twist_sent.vector.x = twist[0,0]
      self.twist_sent.vector.y = twist[1,0]
      self.twist_sent.header.stamp = rospy.get_rostime()

      # Remap pressure
      for i in range(0,(len(p_inv)-1)/2):
          if( p_now[i,0]<0.):
              p_inv[2+2*i] = -1.*p_now[i,0]
              p_inv[1+2*i] = 0.
          else:
              p_inv[2+2*i] = 0.
              p_inv[1+2*i] = p_now[i,0]
      # print p_inv
      return p_inv
      # p_inv = [0.3, 0.2 , 0. , 0., 0.2]

  def bending_forward_kinematics(self, press, published, estimate, with_noise):
      bend_num = (len(press)-1)/2
      homo_matrix = np.zeros((3,1))


      self.total_bending = 0.
      if(published):
          self.bending_sent.data = []
          self.pose_sent.poses = []
      for i in range(0,bend_num):
          # Get the bending
          p_in = [press[0], press[2*i+1], press[2*i+2]]
          # print i
          # print p_in
          input_model = np.array([p_in])
          if(self.model == 1):
              bending_np = self.loaded_model.predict(input_model)
          else:
              if(estimate == 0):
                  bending_np = self.least_square(input_model, i)
                  # print bending_np
                  bending_np = bending_np.reshape((1,))
              else:
                  # TEMPORARY!!!!
                #   if(i==1):
                  bending_np = self.least_square_est(input_model, i)
                #   else:
                #     bending_np = self.least_square(input_model, i)
                  # print bending_np
                  bending_np = bending_np.reshape((1,))
          if(self.eversion_on==1):
              bending_np = bending_np*self.bend_section[i]/self.bend_section_max

          if(with_noise == 1):
            if(self.noisy == 0):
                angle = bending_np[0]*3.14/180.0
                angle = angle.reshape((1,))
            else:
                mu, sigma = 0, 0.08 # mean and standard deviation
                noises = np.random.normal(mu, sigma, 1)
                # print noises
                # print bending_np[0]
                angle = (bending_np[0]+noises)*3.14/180.0
                angle = angle.reshape((1,))
          else:
              angle = bending_np[0]*3.14/180.0
              angle = angle.reshape((1,))
          homo_matrix[i,0] = angle
      return homo_matrix

  def bending_jacobian_calculate(self, q_in, estimate):
    q_out_cur = self.bending_forward_kinematics(self.p_in, self.NOT_PUBLISH, estimate, self.NO_NOISE)
    delta_p = 0.00001
    delta_l = 0.0001
    # delta_p = 1.0

    if(self.inverse_growth==0):
        jac=np.zeros((3,(len(q_in)-1)/2))
    else:
        jac=np.zeros((3,(len(q_in)-1)/2+1))
    if(self.inverse_growth==1):
        self.bend_section[self.bend_number-1] = self.bend_section[self.bend_number-1] + delta_l
        q_out_new = self.bending_forward_kinematics(self.p_in, self.NOT_PUBLISH, estimate, self.NO_NOISE)
        for j in range(0,3):
            jac[j,0] = (q_out_new[j,0]-q_out_cur[j,0])/delta_l
        self.bend_section[self.bend_number-1] = self.bend_section[self.bend_number-1] - delta_l

    for i in range(0,(len(q_in)-1)/2):
        if(q_in[1+2*i]<=0.001 and q_in[2+2*i]>0.001):
            q_in[2+2*i] = q_in[2+2*i] - delta_p
            q_in[1+2*i] = 0.
        else:
            q_in[1+2*i] = q_in[1+2*i] + delta_p
            q_in[2+2*i] = 0.
        p_in_update = self.p_in
        for j in range(0, 2*self.active_inv):
            p_in_update[2*self.bend_number-(2*self.active_inv-1)+j] = q_in[j+1]
        q_out_new = self.bending_forward_kinematics(p_in_update, self.NOT_PUBLISH, estimate, self.NO_NOISE)
        for j in range(0,3):
            if(self.inverse_growth == 0):
                jac[j,i] = (q_out_new[j,0]-q_out_cur[j,0])/delta_p
            else:
                jac[j,i+1] = (q_out_new[j,0]-q_out_cur[j,0])/delta_p
        if(q_in[1+2*i]<=0.001 and q_in[2+2*i]>0.001):
            q_in[2+2*i] = q_in[2+2*i] + delta_p
            q_in[1+2*i] = 0.
        else:
            q_in[1+2*i] = q_in[1+2*i] - delta_p
            q_in[2+2*i] = 0.

    # print jac
    return jac

  def bending_inverse_kinematics(self, estimate, p_inv):
      # Mapping pressure
      p_now = np.empty(((len(p_inv)-1)/2,1), float)

      for i in range(0,(len(p_inv)-1)/2):
          if(p_inv[1+2*i]<=0.001 and p_inv[2+2*i]>0.001):
              p_now[i,0] = -1.*p_inv[2+2*i]
          else:
              p_now[i,0] = p_inv[1+2*i]
      # print "tes"
      # print p_now

      # Compute jacobian and inverse jacobian
      jacobi = np.array(self.bending_jacobian_calculate(p_inv, estimate))
    #   print jacobi
      # v = np.array([[0.0], [0.0]])
    #   self.bending_goal = np.array([[30.0],[30.0],[30.0]])*3.14/180.0
      print self.bending_forward_kinematics(self.p_in, self.NOT_PUBLISH, estimate, self.WITH_NOISE)
    #   print jacobi
      v = -1*(self.bending_forward_kinematics(self.p_in, self.NOT_PUBLISH, estimate, self.WITH_NOISE)-self.bending_goal)
      
      self.p_dot = np.matmul(np.linalg.pinv(jacobi), v)
      p_now = p_now + self.p_dot*self.delta_t
      
      
      # Remap pressure
      for i in range(0,(len(p_inv)-1)/2):
          if( p_now[i,0]<0.):
              p_inv[2+2*i] = -1.*p_now[i,0]
              p_inv[1+2*i] = 0.
          else:
              p_inv[2+2*i] = 0.
              p_inv[1+2*i] = p_now[i,0]
      # print p_inv
      return p_inv
      # p_inv = [0.3, 0.2 , 0. , 0., 0.2]


if __name__ == '__main__':
    try:
        bending_model()
    except rospy.ROSInterruptException: pass
