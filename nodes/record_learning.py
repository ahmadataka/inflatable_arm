#!/usr/bin/env python
# Data recorded are:
# 1. Pressure given --> pressure, Float64MultiArray, 5
# 2. Pressure read --> pressure_read, Float64MultiArray, 5
# 3. Bending sensor --> sensor_read, Float64MultiArray, 4
# 4. PointCloud --> obstacle_point_kinect, PointCloud2,
# 5. Image --> /kinect2/qhd/image_color, sensor_msgs/Image


import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from std_msgs.msg import Float64MultiArray

class record_data(object):
  def __init__(self):
    rospy.init_node('record_sensor')
    send_pub = rospy.Publisher('/record_sensor', Float64MultiArray, queue_size = 10)
    rec_goal = rospy.Subscriber('/pressure', Float64MultiArray, self.p_write_callback)
    rec_tip = rospy.Subscriber('/pressure_read', Float64MultiArray, self.p_read_callback)
    rec_joint = rospy.Subscriber('/sensor_read', Float64MultiArray, self.bending_callback)

    frequency = 50.0
    r = rospy.Rate(frequency)
    self.data_complete = Float64MultiArray()
    self.p_write = Float64MultiArray()
    self.p_read = Float64MultiArray()
    self.bending = Float64MultiArray()
    self.initialize()
    while not rospy.is_shutdown():
        self.transform_to_sent()
        send_pub.publish(self.data_complete)
        r.sleep()

  def initialize(self):
      self.p_write.data = []
      for i in range(0, 5):
          self.p_write.data.append(0.0)
      self.p_read.data = []
      for i in range(0, 5):
          self.p_read.data.append(0.0)
      self.bending.data = []
      for i in range(0,4):
          self.bending.data.append(0.0)

  def p_write_callback(self, msg):
    self.p_write.data = []
    for i in range(0, 5):
        self.p_write.data.append(msg.data[i])

  def p_read_callback(self, msg):
    self.p_read.data = []
    for i in range(0, 5):
        self.p_read.data.append(msg.data[i])

  def bending_callback(self, msg):
    self.bending.data = []
    for i in range(0,4):
        self.bending.data.append(msg.data[i])

  def transform_to_sent(self):
    self.data_complete.data = []
    for i in range(0,5):
        self.data_complete.data.append(self.p_write.data[i])
    for i in range(0,5):
        self.data_complete.data.append(self.p_read.data[i])
    for i in range(0,4):
        self.data_complete.data.append(self.bending.data[i])

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
