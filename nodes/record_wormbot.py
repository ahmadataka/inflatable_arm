#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Float64MultiArray
from inflatable_arm.msg import wormbot_msg

class record_data(object):
  def __init__(self):
    rospy.init_node('record_position')
    self.send = wormbot_msg()
    self.experiment = rospy.get_param('/experiment')
    send_pub = rospy.Publisher('/record_wormbot', wormbot_msg, queue_size = 10)
    rec_tip = rospy.Subscriber('/tip_pose', PoseArray, self.tip_callback)
    bend_sub = rospy.Subscriber('/bending', Float64MultiArray, self.get_bending)
    if(self.experiment==0):
      goal_sub = rospy.Subscriber('/goal_pose', Pose, self.get_goal)
    else:
      goal_sub = rospy.Subscriber('/goal_kinect', Pose, self.get_goal)
    rec_joint = rospy.Subscriber('/pressure', Float64MultiArray, self.joint_callback)
    rec_state = rospy.Subscriber('/states', Float64MultiArray, self.state_callback)
    rec_out = rospy.Subscriber('/output_estimate', Float64MultiArray, self.output_callback)
    rec_param = rospy.Subscriber('/ls_param', Float64MultiArray, self.ls_callback)
    rec_error = rospy.Subscriber('/jacobi_error', Float64MultiArray, self.error_callback)

    frequency = 50.0
    r = rospy.Rate(frequency)

    while not rospy.is_shutdown():
        self.send.now = rospy.get_rostime()
        send_pub.publish(self.send)
        r.sleep()

  def tip_callback(self, msg):
    self.send.tip.poses = []
    for i in range(0, len(msg.poses)):
        tip_pose = Pose()
        tip_pose.position.x = msg.poses[i].position.x
        tip_pose.position.y = msg.poses[i].position.y
        tip_pose.position.z = msg.poses[i].position.z
        tip_pose.orientation.w = msg.poses[i].orientation.w
        tip_pose.orientation.x = msg.poses[i].orientation.x
        tip_pose.orientation.y = msg.poses[i].orientation.y
        tip_pose.orientation.z = msg.poses[i].orientation.z
        self.send.tip.poses.append(tip_pose)

  def joint_callback(self, msg):
    self.send.pressure.data = []
    for i in range(0,len(msg.data)):
        self.send.pressure.data.append(msg.data[i])

  def get_goal(self, msg):
      self.send.goal.position.x = msg.position.x
      self.send.goal.position.y = msg.position.y
      self.send.goal.position.z = msg.position.z
      self.send.goal.orientation.w = msg.orientation.w
      self.send.goal.orientation.x = msg.orientation.x
      self.send.goal.orientation.y = msg.orientation.y
      self.send.goal.orientation.z = msg.orientation.z

  def get_bending(self,variable):
      self.send.bending.data = []
      for i in range(0, len(variable.data)):
          self.send.bending.data.append(variable.data[i])

  def state_callback(self,msg):
    self.send.state_est.data = []
    for i in range(0,len(msg.data)):
        self.send.state_est.data.append(msg.data[i])

  def output_callback(self,msg):
    self.send.output.data = []
    for i in range(0,len(msg.data)):
        self.send.output.data.append(msg.data[i])
  
  def ls_callback(self,msg):
    self.send.state.data = []
    for i in range(0,len(msg.data)):
        self.send.state.data.append(msg.data[i])

  def error_callback(self,msg):
    self.send.error.data = []
    for i in range(0,len(msg.data)):
        self.send.error.data.append(msg.data[i])


if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
