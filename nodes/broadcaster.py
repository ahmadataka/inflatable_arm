#!/usr/bin/env python
import roslib
roslib.load_manifest('inflatable_arm')
import rospy

import tf
from geometry_msgs.msg import PoseArray

def handle_turtle_pose(msg):
    for i in range(0, len(msg.poses)):
        br = tf.TransformBroadcaster()
        br.sendTransform((msg.poses[i].position.x, msg.poses[i].position.y, msg.poses[i].position.z),
                         (msg.poses[i].orientation.x,msg.poses[i].orientation.y,msg.poses[i].orientation.z,msg.poses[i].orientation.w),
                         rospy.Time.now(),
                         "tip"+str(i),
                         "world")

if __name__ == '__main__':
    rospy.init_node('turtle_tf_broadcaster')

    rospy.Subscriber('tip_pose',
                     PoseArray,
                     handle_turtle_pose)
    rospy.spin()
