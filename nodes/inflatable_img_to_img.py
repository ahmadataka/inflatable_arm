#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from std_msgs.msg import Float64MultiArray
from inflatable_arm.msg import inflatable_msg
from inflatable_arm.msg import inflatable_img_msg
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy as np
import imutils

global data_full

class record_data(object):
  def __init__(self):
    rospy.init_node('record_position')
    global data_full
    data_full = np.empty((0,2), float)

    self.send = inflatable_img_msg()
    self.bridge = CvBridge()

    bend_sub = rospy.Subscriber('/record_bending', inflatable_img_msg, self.get_msg)
    self.image_pub = rospy.Publisher('/image_tracking', Image, queue_size = 10)


    rospy.spin()


  def get_msg(self,msg):
      global data_full
      try:
          # self.image_pub.publish(msg.img)
          image = self.bridge.imgmsg_to_cv2(msg.img, "bgr8")
          bending = 0.
          hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
          upper_range = np.array([179,255,255])
          lower_range = np.array([160,20,20])

          mask = cv2.inRange(hsv, lower_range, upper_range)
          mask = cv2.erode(mask, None, iterations=1)
          mask = cv2.dilate(mask, None, iterations=1)

          cnts_hsv = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
          cnts_hsv = imutils.grab_contours(cnts_hsv)
          # print(len(cnts_hsv))
          if(len(cnts_hsv)>=8):
            points_red_x = []
            points_red_y = []

            for i in range(0,len(cnts_hsv)):
                central_point = sum(cnts_hsv[i][:,0])/len(cnts_hsv[i][:,0])
                points_red_x.append([central_point[0]])
                points_red_y.append([central_point[1]])
            points_red_x = np.asarray(points_red_x)
            points_red_y = np.asarray(points_red_y)
            points_red_x.reshape((len(points_red_x),1))
            points_red_y.reshape((len(points_red_y),1))
            # print len(points_red_x)
            # print points_red_y
            if(len(cnts_hsv) > 8):
                # print "a"
                # print points_red_x
                # print points_red_y
                points_merge_y = []
                points_merge_x = []
                for i in range(0,len(points_red_y)-1):
                    if((((points_red_y[i+1,0]-points_red_y[i,0])**2+(points_red_x[i+1,0]-points_red_x[i,0])**2)**0.5)<15.1):
                        points_merge_y.append(points_red_y[i,0])
                        points_merge_y.append(points_red_y[i+1,0])
                        points_merge_x.append(points_red_x[i,0])
                        points_merge_x.append(points_red_x[i+1,0])
                # print "b"
                # print((points_merge_x))
                # print((points_merge_y))
                points_red_y_new = []
                points_red_x_new = []
                reset_i = 0
                for i in range(0, len(cnts_hsv)):
                    # reset_i = 0
                    if(reset_i == 0):
                        for j in range(0, len(points_merge_y)/2):
                            if(points_red_y[i,0]==points_merge_y[2*j] and points_red_x[i,0]==points_merge_x[2*j] and points_red_y[i+1,0]==points_merge_y[2*j+1] and points_red_x[i+1,0]==points_merge_x[2*j+1]):
                                y_new = (int(round(0.5*(points_merge_y[2*j]+points_merge_y[2*j+1]))))
                                x_new = (int(round(0.5*(points_merge_x[2*j]+points_merge_x[2*j+1]))))
                                reset_i = 1
                            else:
                                x_old = points_red_x[i,0]
                                y_old = points_red_y[i,0]

                        if(reset_i == 0):
                            points_red_x_new.append(x_old)
                            points_red_y_new.append(y_old)
                        else:
                            points_red_x_new.append(x_new)
                            points_red_y_new.append(y_new)
                    else:
                        reset_i = 0

                # print (points_red_x_new)
                # print (points_red_y_new)
                points_red_x = []
                points_red_y = []
                for i in range(0, len(points_red_x_new)):
                    points_red_x.append(points_red_x_new[i])
                    points_red_y.append(points_red_y_new[i])
                    # print points_red_x
                    # print points_red_y


                # print points_red_y
            points_red_x = np.asarray(points_red_x).reshape((len(points_red_x),1))
            points_red_y = np.asarray(points_red_y).reshape((len(points_red_y),1))
            # print points_red_x
            # print points_red_y
            N = 3
            sigma_x = 0.0
            sigma_y = 0.0
            sigma_xy = 0.0
            sigma_x2 = 0.0
            for i in range(0, N):
                # Gradient normal
                sigma_x = sigma_x + float(points_red_x[i,0])
                sigma_y = sigma_y + float(points_red_y[i,0])
                sigma_x2 = sigma_x2 + float(points_red_x[i,0]**2)
                sigma_xy = sigma_xy + float(points_red_x[i,0]*points_red_y[i,0])

            if((N*sigma_x2 - sigma_x**2)!=0):
                m_base = (N*sigma_xy - sigma_x*sigma_y)/(N*sigma_x2 - sigma_x**2)

            sigma_x = 0.0
            sigma_y = 0.0
            sigma_xy = 0.0
            sigma_x2 = 0.0
            for i in range(0, N):
                # Gradient normal
                sigma_x = sigma_x + float(points_red_x[len(points_red_x)-N+i,0])
                sigma_y = sigma_y + float(points_red_y[len(points_red_y)-N+i,0])
                sigma_x2 = sigma_x2 + float(points_red_x[len(points_red_x)-N+i,0]**2)
                sigma_xy = sigma_xy + float(points_red_x[len(points_red_x)-N+i,0]*points_red_y[len(points_red_y)-N+i,0])

            if((N*sigma_x2 - sigma_x**2)!=0):
                m_tip = (N*sigma_xy - sigma_x*sigma_y)/(N*sigma_x2 - sigma_x**2)
            if(m_base<0 and m_tip<0):
                bending = 3.14 - abs(np.arctan(m_base)) + abs(np.arctan(m_tip))
                # print "1"
                # print bending*180.0/3.14
            elif(m_base>=0 and m_tip>=0):
                bending = 3.14 - abs(np.arctan(m_base)) + abs(np.arctan(m_tip))
                # print "2"
            elif(m_base<0 and m_tip>=0):
                bending = 6.28 - (abs(np.arctan(m_base)) + abs(np.arctan(m_tip)))
                # print "3"
            else:
                bending = (abs(np.arctan(m_base)) + abs(np.arctan(m_tip)))
                # if(bending > 3.14):
                #     print "4"
                #     print bending
                # print "a"
                # print bending*180.0/3.14
                # print msg.now
                # print msg.c_space
            bending = bending*180.0/3.14
            # print "a"
            # print m_base
            # print m_tip
            # print np.arctan(m_base)*180.0/3.14
            # print np.arctan(m_tip)*180.0/3.14
            # print bending
            # print "c"
            points_red_x = np.asarray(points_red_x).reshape((len(points_red_x),1))
            points_red_y = np.asarray(points_red_y).reshape((len(points_red_y),1))
            # print points_red_x    print self.data_full
            for i in range(0,N):
                cv2.circle(image,(points_red_x[i,0],points_red_y[i,0]), 5, (0,255,0), -1)
                cv2.circle(image,(points_red_x[len(points_red_x)-N+i,0],points_red_y[len(points_red_x)-N+i,0]), 5, (0,0,255), -1)
            image_sent = self.bridge.cv2_to_imgmsg(image, "bgr8")
            self.image_pub.publish(image_sent)

            data_new = np.array([[msg.c_space.data[0], bending]])
            # print data_new
            data_full = np.vstack([data_full,data_new])
            # print self.data_full



      except CvBridgeError, e:
          print(e)

if __name__ == '__main__':
    try:
        record_data()
    # except rospy.ROSInterruptException:
    #     pass
    finally:
        global data_full
        file_location = '/home/arqlab/record_bending/data_full.csv'
        np.savetxt(file_location, data_full, delimiter=',')   # X is an array
