#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
from inflatable_arm.msg import inflatable_msg

class record_data(object):
  def __init__(self):
    rospy.init_node('record_position')
    self.segment_num = rospy.get_param('/segment_num')
    mobile = rospy.get_param('/mobile_base')
    self.send = inflatable_msg()
    self.tip_pose = Pose()
    if(mobile==1):
        self.starting = 2
    else:
        self.starting = 0
    send_pub = rospy.Publisher('/record_position', inflatable_msg, queue_size = 10)
    rec_goal = rospy.Subscriber('/agent/tip/desired', Pose, self.goal_callback)
    rec_tip = rospy.Subscriber('/agent/tip/current', Pose, self.tip_callback)
    rec_joint = rospy.Subscriber('/agent/joint', JointState, self.joint_callback)

    frequency = 50.0
    r = rospy.Rate(frequency)

    while not rospy.is_shutdown():
        self.transform_to_sent()
        send_pub.publish(self.send)
        r.sleep()

  def tip_callback(self, msg):
    self.tip_pose.position.x = msg.position.x
    self.tip_pose.position.y = msg.position.y
    self.tip_pose.position.z = msg.position.z
    self.tip_pose.orientation.w = msg.orientation.w
    self.tip_pose.orientation.x = msg.orientation.x
    self.tip_pose.orientation.y = msg.orientation.y
    self.tip_pose.orientation.z = msg.orientation.z

  def joint_callback(self, msg):
    self.send.l_space.data = []
    for i in range(0,3*self.segment_num+self.starting+1):
        self.send.l_space.data.append(msg.position[i])

  def goal_callback(self, msg):
      self.send.goal.position.x = msg.position.x
      self.send.goal.position.y = msg.position.y
      self.send.goal.position.z = msg.position.z
      self.send.goal.orientation.w = msg.orientation.w
      self.send.goal.orientation.x = msg.orientation.x
      self.send.goal.orientation.y = msg.orientation.y
      self.send.goal.orientation.z = msg.orientation.z

  def transform_to_sent(self):
    self.send.tip.poses = []
    self.send.tip.poses.append(self.tip_pose)
    self.send.now = rospy.get_rostime()

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
