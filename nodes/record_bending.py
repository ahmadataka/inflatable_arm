#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from std_msgs.msg import Float64MultiArray
from inflatable_arm.msg import inflatable_msg
from inflatable_arm.msg import inflatable_img_msg
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class record_data(object):
  def __init__(self):
    rospy.init_node('record_position')
    self.send = inflatable_img_msg()
    self.bridge = CvBridge()
    image_topic = "/kinect2/qhd/image_color"
    # Set up your subscriber and define its callback
    rospy.Subscriber(image_topic, Image, self.image_callback)

    send_pub = rospy.Publisher('/record_bending', inflatable_img_msg, queue_size = 10)
    bend_sub = rospy.Subscriber('/sensor_read', Float64MultiArray, self.get_bending)
    self.image_pub = rospy.Publisher('/image_tracking', Image, queue_size = 10)
    self.callback = 0
    frequency = 50.0
    r = rospy.Rate(frequency)

    while not rospy.is_shutdown():
        if(self.callback == 1):
            self.send.now = rospy.get_rostime()
            image_sent = self.bridge.cv2_to_imgmsg(self.cv2_img[200:700,250:600], "bgr8")
            image_sent.header.stamp = self.send.now
            self.send.img = image_sent
            # print "a"
            # print image_sent.header.stamp
            # print "b"
            # print self.send.now
            self.image_pub.publish(image_sent)
            send_pub.publish(self.send)
        r.sleep()

  def image_callback(self,msg):
      self.callback = 1
      try:
          # Convert your ROS Image message to OpenCV2
          self.cv2_img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
          # self.time_now = msg.header.stamp
      except CvBridgeError, e:
          print(e)

  def get_bending(self,variable):
      self.send.c_space.data = []
      for i in range(0, len(variable.data)):
          self.send.c_space.data.append(variable.data[i])

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
