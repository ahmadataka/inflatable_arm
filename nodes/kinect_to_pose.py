#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Image
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
import numpy as np
import imutils
import matplotlib.pyplot as plt
from inflatable_arm.msg import inflatable_msg
from scipy import signal

class record_data(object):
  def __init__(self):
    rospy.init_node('kinect_to_bending')
    # Instantiate CvBridge
    self.bridge = CvBridge()

    self.num = 0
    # self.bend_press = rospy.Publisher('/bending', Float64MultiArray, queue_size = 10)
    self.pose_press = rospy.Publisher('/pose_kinect', Pose, queue_size = 10)
    self.total_pub = rospy.Publisher('/kinect_to_data', inflatable_msg, queue_size = 10)
    self.image_pub = rospy.Publisher('/image_tracking', Image, queue_size = 10)
    image_topic = "/kinect2/qhd/image_color"
    rec_pressure = rospy.Subscriber('/pressure', Float64MultiArray, self.pressure_callback)
    self.goal = rospy.Subscriber('/goal_kinect', Pose, self.goal_callback)
    # Set up your subscriber and define its callback
    rospy.Subscriber(image_topic, Image, self.image_callback)
    self.callback = 0

    freq = 50.0
    self.delta_t = 1.0 / freq
    r = rospy.Rate(freq)
    self.bending_sent = Float64MultiArray()
    # self.tip_pose = Pose()
    self.data_sent = inflatable_msg()
    self.data_index = 0
    self.bending_total = []
    self.bending_avg = np.zeros((3, 5))
    self.N = 1
    self.flag_pressure = 0
    self.bending_save = np.zeros((3, 1))
    self.goal_kinect = [7, -334]
    # for i in range(0,3):
    #     self.bending_avg.append(0)
    while not rospy.is_shutdown():
        self.image_to_pose()
        # Sending pressure
        # if(self.callback == 0):
        #     self.data_index = self.data_index + 1
        #     if(self.data_index>1000):
        #         self.data_index = 0
        self.bending_sent.data = []
        for i in range(0, len(self.bending_total)):
            self.bending_sent.data.append(self.bending_total[i])
        # self.bend_press.publish(self.bending_sent)
        self.total_pub.publish(self.data_sent)
        r.sleep()
        # self.fig.clf()

  def pressure_callback(self,msg):
    self.p_in = []
    self.flag_pressure = 1
    for i in range(0, len(msg.data)):
        self.p_in.append(msg.data[i])

  def goal_callback(self,msg):
    self.goal_kinect[0] = int(msg.position.x)
    self.goal_kinect[1] = int(msg.position.y)

    
  def image_callback(self,msg):
      self.callback = 1
      try:
          # Convert your ROS Image message to OpenCV2
          self.cv2_img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
          self.time_now = msg.header.stamp
      except CvBridgeError, e:
          print(e)
      # else:
          # Save your OpenCV2 image as a jpeg
          # cv2.imwrite('camera_image'+str(num)+'.jpeg', self.cv2_img)
  # def image_to_edge(self):

  def image_to_pose(self):
      if(self.callback == 1):
          image = self.cv2_img[0:600,200:1000]
          self.callback = 0
        #   print self.cv2_img.shape
      # else:
      #     laptop = 'arqlab'
      #     file_name = '/home/'+laptop+'/wormbot_exp_2/camera_image'+str(self.data_index)+'.jpeg'
      #     # file_name = 'camera_image15.jpeg'
      #     # file_name = '/home/ahmadataka/catkin_ws/src/inflatable_arm/wormbot_exp/camera_image20.jpeg'
      #     image = cv2.imread(file_name)
          bending = 0.
          hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
          # print hsv`
          upper_range = np.array([179,255,255])
          lower_range = np.array([160,20,20])

          # upper_range = np.array([70, 255,255])
          # lower_range = np.array([20, 20,20])

          # upper_range = np.array([255,255,255])
          # lower_range = np.array([0,100,100])

          mask = cv2.inRange(hsv, lower_range, upper_range)
          mask = cv2.erode(mask, None, iterations=1)
          mask = cv2.dilate(mask, None, iterations=1)
          # res = cv2.bitwise_and(image, image, mask=mask)

          cnts_hsv = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
          cnts_hsv = imutils.grab_contours(cnts_hsv)

          
          if(len(cnts_hsv)==7):
              print len(cnts_hsv)
              points_red_x = []
              points_red_y = []

              for i in range(0,len(cnts_hsv)):
                  central_point = sum(cnts_hsv[i][:,0])/len(cnts_hsv[i][:,0])
                  points_red_x.append([central_point[0]])
                  points_red_y.append([central_point[1]])
              
            #   self.bending_total = []
            #   for i in range(0,3):
            #     # bend_vect = np.array([[float(points_red_x[len(points_red_x)-1-i][0]-points_red_x[len(points_red_x)-2-i][0])], [points_red_y[len(points_red_y)-1-i][0]-points_red_y[len(points_red_y)-2-i][0]],[0.]])
            #     bend_vect = np.array([[float(points_red_x[2*i+3][0]-points_red_x[2*i+2][0])], [float(points_red_y[2*i+3][0]-points_red_y[2*i+2][0])],[0.]])
            #     base_vect = np.array([[float(points_red_x[2*i+1][0]-points_red_x[2*i+0][0])], [float(points_red_y[2*i+1][0]-points_red_y[2*i+0][0])],[0.]])
            #     cos_theta = np.vdot(base_vect, bend_vect)/(np.linalg.norm(base_vect)*np.linalg.norm(bend_vect))
            #     sin_theta = np.cross(base_vect.reshape((3,)), bend_vect.reshape((3,)))
            #     # if(self.p_in[1+2*i]<=0.001 and self.p_in[2+2*i]>0.001):
            #     if(sin_theta[2]>=0):
            #         if(abs(cos_theta)<=1):
            #             bending = (-1)*np.arccos(cos_theta)
            #             self.bending_save[i,0] = bending
            #         else:
            #             bending = self.bending_save[i,0]
            #         # print "-1"
            #     else:
            #         if(abs(cos_theta)<=1):
            #             bending = np.arccos(cos_theta)
            #             self.bending_save[i,0] = bending
            #         else:
            #             bending = self.bending_save[i,0]
            #         # print "1"
            #     bending = bending*180.0/3.14
            #     if(self.N<5):
            #         self.bending_avg[i, self.N] = bending
            #         self.bending_total.append(bending)
            #     else:
            #         # print self.N % 5
            #         # print i
            #         self.bending_avg[i, self.N % 5] = bending
            #         self.bending_total.append(np.median(self.bending_avg[i,:]))
            #         # print self.bending_avg
            #         # print np.median(self.bending_avg[i,:])
                    
            #     self.N = self.N+1
              
              if(len(cnts_hsv)!=0):
                  # self.bending_sent.data = []
                  # self.bending_sent.data.append(bending)
                  self.data_sent.now = self.time_now

                  self.data_sent.c_space.data = []
                  self.data_sent.c_space.data.append(bending)
                  self.data_sent.tip.poses = []
                  for i in range(0,len(points_red_x)-1):

                      self.tip_pose = Pose()
                      self.tip_pose.position.x = points_red_x[len(points_red_x)-1-i][0]-points_red_x[0][0]
                      self.tip_pose.position.y = points_red_y[len(points_red_x)-1-i][0]-points_red_y[0][0]
                      self.data_sent.tip.poses.append(self.tip_pose)
                  # self.tip_pose = Pose()
                  # self.tip_pose.position.x = point_rest_x[0][0]-point_base[0][0]
                  # self.tip_pose.position.y = point_rest_y[0][0]-point_base[1][0]
                  # self.data_sent.tip.poses.append(self.tip_pose)
                  # self.tip_pose = Pose()
                  # self.tip_pose.position.x = point_rest_x[1][0]-point_base[0][0]
                  # self.tip_pose.position.y = point_rest_y[1][0]-point_base[1][0]
                  # self.data_sent.tip.poses.append(self.tip_pose)

                  # Image
                #   cv2.circle(image,(points_red_x[len(points_red_x)-1][0],points_red_y[len(points_red_x)-1][0]), 5, (0,0,255), -1)
                  # cv2.circle(image,(point_base[0][0],point_base[1][0]), 5, (0,0,255), -1)
        #   if(len(cnts_hsv)!=0):
        #     cv2.circle(image,(points_red_x[0][0],points_red_y[0][0]), 5, (0,255,0), -1)
        #     cv2.circle(image,(points_red_x[1][0],points_red_y[1][0]), 5, (0,255,0), -1)
        #     cv2.circle(image,(points_red_x[len(points_red_x)-1][0],points_red_y[len(points_red_x)-1][0]), 5, (0,0,255), -1)
        #     cv2.circle(image,(points_red_x[len(points_red_x)-2][0],points_red_y[len(points_red_x)-2][0]), 5, (0,0,255), -1)
        #     for i in range(0, len(points_red_x)):
        #         if(i!=0 and i!=1 and i!=(len(points_red_x)-1) and i!=(len(points_red_x)-2)):
        #             cv2.circle(image,(points_red_x[i][0],points_red_y[i][0]), 5, (0,255,0), -1)

              # # print point_rest_x
              # for i in range(0,len(point_rest_x)):
              #     cv2.circle(image,(point_rest_x[i][0],point_rest_y[i][0]), 5, (0,255,0), -1)
          
        #   hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        #   # print hsv`
        #   upper_range = np.array([80,255,255])
        #   lower_range = np.array([50,100,50])

        #   # upper_range = np.array([70, 255,255])
        #   # lower_range = np.array([20, 20,20])

        #   # upper_range = np.array([255,255,255])
        #   # lower_range = np.array([0,100,100])

        #   mask = cv2.inRange(hsv, lower_range, upper_range)
        #   mask = cv2.erode(mask, None, iterations=1)
        #   mask = cv2.dilate(mask, None, iterations=1)
        #   # res = cv2.bitwise_and(image, image, mask=mask)

        #   cnts_hsv = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        #   cnts_hsv = imutils.grab_contours(cnts_hsv)

        #   print len(cnts_hsv)
        #   if(len(cnts_hsv)==1):
        #     central_point = sum(cnts_hsv[0][:,0])/len(cnts_hsv[0][:,0])
        #     cv2.circle(image,(central_point[0],central_point[1]), 5, (255,0,0), -1)
        #     self.goal_sent.position.x = ([central_point[0]]-points_red_x[0][0])
        #     self.goal_sent.position.y = ([central_point[1]]-points_red_y[0][0])
          for i in range(0,len(cnts_hsv)+1):
              if(i<len(cnts_hsv)):
                  central_point = sum(cnts_hsv[i][:,0])/len(cnts_hsv[i][:,0])
                  if(i==0):
                      save_x = central_point[0]
                      save_y = central_point[1]
                  cv2.circle(image,(central_point[0],central_point[1]), 2, (0,255,0), -1)
                #   print central_point
              else:
                  central_point = [self.goal_kinect[0], self.goal_kinect[1]]
                #   print"x"
                #   print central_point[0]-save_x
                #   print central_point[1]-save_y
                  cv2.circle(image,(central_point[0]+save_x,central_point[1]+save_y), 2, (0,0,255), -1)
            #   if(i==7):
              
          image_sent = self.bridge.cv2_to_imgmsg(image, "bgr8")
          self.image_pub.publish(image_sent)



if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
