#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('inflatable_arm')
import math
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import numpy as np

class markers(object):
  def __init__(self):
    rospy.init_node('markers')
    rospy.sleep(3)
    self.frame_number = rospy.get_param('/bend_number')
    self.eversion_on = rospy.get_param('/eversion_on')
    # self.link = [0.1, 0.43, 0.0]
    self.link = []
    self.NUM_MARK_MAX = 8
    for i in range(0,self.frame_number):
        if(i!=(self.frame_number-1)):
            self.link.append(0.1)
        else:
            self.link.append(0.0)
    bend_section_max = 0.42
    self.l0 = 0.1
    self.num_marker = []
    for i in range(0,self.frame_number):
        if(i!=(self.frame_number-1)):
            self.num_marker.append(self.NUM_MARK_MAX)
        else:
            if(self.eversion_on == 1):
                self.num_marker.append(int((self.l0%bend_section_max)/bend_section_max*self.NUM_MARK_MAX))
            else:
                self.num_marker.append(self.NUM_MARK_MAX)
    
    self.cm_pose = PoseArray()
    self.marker_pub = rospy.Publisher('markers', MarkerArray, queue_size = 10)
    bend_sub = rospy.Subscriber('/bending', Float64MultiArray, self.get_bending)
    goal_sub = rospy.Subscriber('/goal_pose', Pose, self.get_goal)
    if(self.eversion_on==1):
        goal_sub = rospy.Subscriber('/length', Float64MultiArray, self.get_length)

    self.goal = [0., 0., 0.]
    r = rospy.Rate(10)
    self.markerarray = MarkerArray()

    while not rospy.is_shutdown():
        self.bend_section = []
        for i in range(0,self.frame_number):
            if(i!=(self.frame_number-1)):
                self.bend_section.append(bend_section_max)
            else:
                if(self.eversion_on == 1):
                    self.bend_section.append(self.l0%bend_section_max)
                else:
                    self.bend_section.append(bend_section_max)
        if(self.eversion_on==1):
            # print self.num_marker
            self.frame_number = rospy.get_param('/bend_number')
            self.link = []
            for i in range(0,self.frame_number):
                if(i!=(self.frame_number-1)):
                    self.link.append(0.1)
                else:
                    self.link.append(0.0)
            self.num_marker = []
            for i in range(0,self.frame_number):
                if(i!=(self.frame_number-1)):
                    self.num_marker.append(self.NUM_MARK_MAX)
                else:
                    self.num_marker.append(int((self.l0%bend_section_max)/bend_section_max*self.NUM_MARK_MAX))
        self.active_segment = rospy.get_param('active_segment')
        self.inv_kin = rospy.get_param('inverse_kinematics')
        r.sleep()

  def bending_arc(self, bending_angle, marker_index, frame):
      pose = np.empty((3,1), float)

      if(bending_angle>0):
          pose[0] = - (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))/(bending_angle*(marker_index+1)/float(self.num_marker[frame]))*(1-np.cos(bending_angle*(marker_index+1)/float(self.num_marker[frame])))
          pose[1] = (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))/(bending_angle*(marker_index+1)/float(self.num_marker[frame]))*np.sin(bending_angle*(marker_index+1)/float(self.num_marker[frame]))
          pose[2] = 0.0
      elif(bending_angle<0):
          pose[0] = (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))/abs(bending_angle*(marker_index+1)/float(self.num_marker[frame]))*(1-np.cos(bending_angle*(marker_index+1)/float(self.num_marker[frame])))
          pose[1] = (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))/abs(bending_angle*(marker_index+1)/float(self.num_marker[frame]))*np.sin(abs(bending_angle*(marker_index+1)/float(self.num_marker[frame])))
          pose[2] = 0.
      else:
          pose[0] = 0.
          pose[1] = (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))
          pose[2] = 0.
      return pose

  def straight_segment(self, segment_ind, length):
      pose = np.empty((3,1), float)

      pose[0] = 0.
      pose[1] = self.link[segment_ind]*length
      pose[2] = 0.
      return pose

  def get_goal(self,variable):
    self.goal = [variable.position.x, variable.position.y, variable.position.z]

  def get_length(self,variable):
    self.l0 = variable.data[0]

  def get_bending(self,variable):
    bending_total = 0.
    self.markerarray.markers = []
    homo_matrix = np.eye(4)
    
    for i in range(0, len(variable.data)):
        bending_angle = variable.data[i]*3.14/180.0

        for j in range(0, self.num_marker[i]):
            marker = Marker()
            tip = self.bending_arc(bending_angle, j, i)

            mat_tip = np.array([[np.cos(bending_angle*(j+1)/float(self.num_marker[i])), -np.sin(bending_angle*(j+1)/float(self.num_marker[i])), 0., tip[0,0]], [np.sin(bending_angle*(j+1)/float(self.num_marker[i])), np.cos(bending_angle*(j+1)/float(self.num_marker[i])), 0., tip[1,0]], [0., 0., 1., tip[2,0]], [0., 0., 0., 1.]])
            matrix_marker = np.matmul(homo_matrix, mat_tip)
            marker.pose.position.x = matrix_marker[0,3]
            marker.pose.position.y = matrix_marker[1,3]
            marker.pose.position.z = matrix_marker[2,3]
            marker.pose.orientation.x = 0.0
            marker.pose.orientation.y = 0.0
            marker.pose.orientation.z = np.sin(0.5*(bending_angle*(j+1)/float(self.num_marker[i])+bending_total))
            marker.pose.orientation.w = np.cos(0.5*(bending_angle*(j+1)/float(self.num_marker[i])+bending_total))

            marker.header.frame_id = "/world"
            marker.header.stamp = rospy.Time.now()
            marker.ns = "arm"+str(i)
            marker.id = j
            marker.action = marker.ADD
            marker.type = marker.CYLINDER
            marker.scale.x = 0.1;
            marker.scale.y = 0.1;
            marker.scale.z = 0.1;
            marker.color.a = 1.0
            if(self.inv_kin == 0):
                if(i == self.active_segment):
                    marker.color.r = 1.0
                    marker.color.g = 0.0
                    marker.color.b = 0.0
                else:
                    marker.color.r = 0.0
                    marker.color.g = 1.0
                    marker.color.b = 0.0
            else:
                marker.color.r = 1.0
                marker.color.g = 0.0
                marker.color.b = 0.0
            marker.lifetime = rospy.Duration()
            self.markerarray.markers.append(marker)
        # Update matrix
        homo_matrix = matrix_marker
        # print homo_matrix
        # print tip
        bending_total = bending_total + bending_angle

        # Add link
        marker = Marker()
        marker.header.frame_id = "/world"
        marker.header.stamp = rospy.Time.now()
        marker.ns = "arm"+str(i)
        marker.id = j
        marker.action = marker.ADD
        marker.type = marker.CUBE
        marker.scale.x = 0.1
        marker.scale.y = self.link[i];
        marker.scale.z = 0.1;
        marker.color.a = 1.0
        if(self.inv_kin == 0):
            marker.color.r = 0.0
            marker.color.g = 1.0
            marker.color.b = 0.0
        else:
            marker.color.r = 1.0
            marker.color.g = 0.0
            marker.color.b = 0.0
        marker.lifetime = rospy.Duration()
        tip = self.straight_segment(i,0.5)
        mat_tip = np.array([[1., 0., 0., tip[0,0]], [0., 1., 0., tip[1,0]], [0., 0., 1., tip[2,0]], [0., 0., 0., 1.]])
        matrix_marker = np.matmul(homo_matrix, mat_tip)
        marker.pose.position.x = matrix_marker[0,3]
        marker.pose.position.y = matrix_marker[1,3]
        marker.pose.position.z = matrix_marker[2,3]
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = np.sin(0.5*bending_total)
        marker.pose.orientation.w = np.cos(0.5*bending_total)
        self.markerarray.markers.append(marker)

        # Update matrix
        tip = self.straight_segment(i,1.)
        mat_tip = np.array([[1., 0., 0., tip[0,0]], [0., 1., 0., tip[1,0]], [0., 0., 1., tip[2,0]], [0., 0., 0., 1.]])
        homo_matrix = np.matmul(homo_matrix, mat_tip)

        # Add goal
        marker = Marker()
        marker.header.frame_id = "/world"
        marker.header.stamp = rospy.Time.now()
        marker.ns = "goal"
        marker.id = j+1
        marker.action = marker.ADD
        marker.type = marker.SPHERE
        marker.scale.x = 0.1
        marker.scale.y = 0.1;
        marker.scale.z = 0.1;
        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 1.0
        marker.lifetime = rospy.Duration()
        marker.pose.position.x = self.goal[0]
        marker.pose.position.y = self.goal[1]
        marker.pose.position.z = self.goal[2]
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0
        self.markerarray.markers.append(marker)

    self.marker_pub.publish(self.markerarray)

if __name__ == '__main__':
    try:
        markers()
    except rospy.ROSInterruptException: pass
