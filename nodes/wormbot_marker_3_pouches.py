#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('inflatable_arm')
import math
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import numpy as np

class markers(object):
  def __init__(self):
    rospy.init_node('markers')
    rospy.sleep(3)
    self.frame_number = rospy.get_param('/bend_number')
    self.eversion_on = rospy.get_param('/eversion_on')
    self.pouch_number = 3
    
    # self.link = [0.1, 0.43, 0.0]
    self.link = []
    self.NUM_MARK_MAX = 8
    for i in range(0,self.frame_number):
        if(i!=(self.frame_number-1)):
            self.link.append(0.1)
        else:
            self.link.append(0.0)
    bend_section_max = 0.42
    self.l0 = 0.1
    self.num_marker = []
    for i in range(0,self.frame_number):
        if(i!=(self.frame_number-1)):
            self.num_marker.append(self.NUM_MARK_MAX)
        else:
            if(self.eversion_on == 1):
                self.num_marker.append(int((self.l0%bend_section_max)/bend_section_max*self.NUM_MARK_MAX))
            else:
                self.num_marker.append(self.NUM_MARK_MAX)
    
    self.cm_pose = PoseArray()
    self.marker_pub = rospy.Publisher('markers', MarkerArray, queue_size = 10)
    bend_sub = rospy.Subscriber('/bending', Float64MultiArray, self.get_bending)
    goal_sub = rospy.Subscriber('/goal_pose', Pose, self.get_goal)
    if(self.eversion_on==1):
        goal_sub = rospy.Subscriber('/length', Float64MultiArray, self.get_length)

    self.goal = [0., 0.]
    r = rospy.Rate(10)
    self.markerarray = MarkerArray()

    while not rospy.is_shutdown():
        self.bend_section = []
        for i in range(0,self.frame_number):
            if(i!=(self.frame_number-1)):
                self.bend_section.append(bend_section_max)
            else:
                if(self.eversion_on == 1):
                    self.bend_section.append(self.l0%bend_section_max)
                else:
                    self.bend_section.append(bend_section_max)
        if(self.eversion_on==1):
            # print self.num_marker
            self.frame_number = rospy.get_param('/bend_number')
            self.link = []
            for i in range(0,self.frame_number):
                if(i!=(self.frame_number-1)):
                    self.link.append(0.1)
                else:
                    self.link.append(0.0)
            self.num_marker = []
            for i in range(0,self.frame_number):
                if(i!=(self.frame_number-1)):
                    self.num_marker.append(self.NUM_MARK_MAX)
                else:
                    self.num_marker.append(int((self.l0%bend_section_max)/bend_section_max*self.NUM_MARK_MAX))
        self.active_segment = rospy.get_param('active_segment')
        self.inv_kin = rospy.get_param('inverse_kinematics')
        r.sleep()

  def bending_arc(self, bending_angle, marker_index, frame):
      pose = np.empty((4,4), float)
      
      if(bending_angle[0]!=0):
          pose[0,3] = (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))/(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame]))*np.cos(bending_angle[1])*(1-np.cos(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame])))
          pose[1,3] = (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))/(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame]))*np.sin(bending_angle[1])*(1-np.cos(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame])))
          pose[2,3] = (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))/(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame]))*np.sin(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame]))
      else:
          pose[0,3] = 0.
          pose[1,3] = 0.
          pose[2,3] = (self.bend_section[frame]*(marker_index+1)/float(self.num_marker[frame]))
      pose[3,3] = 1.0
    #   pose[0,0] = np.cos(bending_angle[1])*np.cos(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame]))
    #   pose[1,0] = np.sin(bending_angle[1])*np.cos(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame]))
    #   pose[2,0] = -np.sin(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame]))
    #   pose[3,0] = pose[3,1] = pose[3,2] = pose[2,1] = 0
    #   pose[0,1] = -np.sin(bending_angle[1])
    #   pose[1,1] = np.cos(bending_angle[1])

      pose[0,0] = (((np.cos(bending_angle[1]))**2)*(np.cos(bending_angle[0])-1)+1)*(marker_index+1)/float(self.num_marker[frame])
      pose[1,0] = (np.sin(bending_angle[1])*np.cos(bending_angle[1])*(np.cos(bending_angle[0])-1))*(marker_index+1)/float(self.num_marker[frame])
      pose[2,0] = (-np.cos(bending_angle[1])*np.sin(bending_angle[0]))*(marker_index+1)/float(self.num_marker[frame])
      pose[3,0] = pose[3,1] = pose[3,2] = 0
      pose[0,1] = (np.sin(bending_angle[1])*np.cos(bending_angle[1])*(np.cos(bending_angle[0])-1))*(marker_index+1)/float(self.num_marker[frame])
      pose[1,1] = (((np.cos(bending_angle[1]))**(2))*(1-np.cos(bending_angle[0]))+np.cos(bending_angle[0]))*(marker_index+1)/float(self.num_marker[frame])
      pose[2,1] = (-np.sin(bending_angle[1])*np.sin(bending_angle[0]))*(marker_index+1)/float(self.num_marker[frame])
      pose[0,2] = (np.cos(bending_angle[1])*np.sin(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame])))
      pose[1,2] = (np.sin(bending_angle[1])*np.sin(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame])))
      pose[2,2] = (np.cos(bending_angle[0]*(marker_index+1)/float(self.num_marker[frame])))

      return pose

  def straight_segment(self, segment_ind, length):
      pose = np.empty((3,1), float)

      pose[0] = 0.
      pose[1] = 0.
      pose[2] = self.link[segment_ind]*length
      return pose

  def get_goal(self,variable):
    self.goal = [variable.position.x, variable.position.y]

  def get_length(self,variable):
    self.l0 = variable.data[0]

  def RtoQuat(self,T_matrix):
    trace = T_matrix[0,0] + T_matrix[1,1] + T_matrix[2,2]
    if( trace > 0 ):
        s = 0.5 / math.sqrt(trace+ 1.0)
        w = 0.25 / s
        x = ( T_matrix[2,1] - T_matrix[1,2] ) * s
        y = ( T_matrix[0,2] - T_matrix[2,0] ) * s
        z = ( T_matrix[1,0] - T_matrix[0,1] ) * s
    else:
        if ( (T_matrix[0,0] > T_matrix[1,1]) and (T_matrix[0,0] > T_matrix[2,2]) ):
            s = 2.0 * math.sqrt( 1.0 + T_matrix[0,0] - T_matrix[1,1] - T_matrix[2,2])
            w = (T_matrix[2,1] - T_matrix[1,2] ) / s
            x = 0.25 * s
            y = (T_matrix[0,1] + T_matrix[1,0] ) / s
            z = (T_matrix[0,2] + T_matrix[2,0] ) / s
        elif (T_matrix[1,1] > T_matrix[2,2]):
            s = 2.0 * math.sqrt( 1.0 + T_matrix[1,1] - T_matrix[0,0] - T_matrix[2,2])
            w = (T_matrix[0,2] - T_matrix[2,0] ) / s
            x = (T_matrix[0,1] + T_matrix[1,0] ) / s
            y = 0.25 * s
            z = (T_matrix[1,2] + T_matrix[2,1] ) / s
        else:
            s = 2.0 * math.sqrt( 1.0 + T_matrix[2,2] - T_matrix[0,0] - T_matrix[1,1] )
            w = (T_matrix[1,0] - T_matrix[0,1] ) / s
            x = (T_matrix[0,2] + T_matrix[2,0] ) / s
            y = (T_matrix[1,2] + T_matrix[2,1] ) / s
            z = 0.25 * s
    T_arma = np.array([[x],[y],[z],[w]])
    return T_arma

  def get_bending(self,variable):
    bending_total = 0.
    self.markerarray.markers = []
    homo_matrix = np.zeros((4,4))
    homo_matrix[0,0] = 1.0
    homo_matrix[1,2] = 1.0
    homo_matrix[2,1] = -1.0
    homo_matrix[3,3] = 1.0
    
    for i in range(0, len(variable.data)/2):
        bending_angle = []
        bending_angle.append(variable.data[2*i])
        bending_angle.append(variable.data[2*i+1])

        for j in range(0, self.num_marker[i]):
            marker = Marker()
            mat_tip = self.bending_arc(bending_angle, j, i)

            matrix_marker = np.matmul(homo_matrix, mat_tip)
            marker_quat = self.RtoQuat(matrix_marker[0:3,0:3])
            marker.pose.position.x = matrix_marker[0,3]
            marker.pose.position.y = matrix_marker[1,3]
            marker.pose.position.z = matrix_marker[2,3]
            marker.pose.orientation.x = marker_quat[0,0]
            marker.pose.orientation.y = marker_quat[1,0]
            marker.pose.orientation.z = marker_quat[2,0]
            marker.pose.orientation.w = marker_quat[3,0]

            marker.header.frame_id = "/world"
            marker.header.stamp = rospy.Time.now()
            marker.ns = "arm"+str(i)
            marker.id = j
            marker.action = marker.ADD
            marker.type = marker.CYLINDER
            marker.scale.x = 0.1
            marker.scale.y = 0.1
            marker.scale.z = 0.1
            marker.color.a = 1.0
            if(self.inv_kin == 0):
                if(i == self.active_segment):
                    marker.color.r = 1.0
                    marker.color.g = 0.0
                    marker.color.b = 0.0
                else:
                    marker.color.r = 0.0
                    marker.color.g = 1.0
                    marker.color.b = 0.0
            else:
                marker.color.r = 1.0
                marker.color.g = 0.0
                marker.color.b = 0.0
            marker.lifetime = rospy.Duration()
            self.markerarray.markers.append(marker)
        # Update matrix
        homo_matrix = matrix_marker
        # print homo_matrix
        # print tip
        # bending_total = bending_total + bending_angle

        # Add link
        marker = Marker()
        marker.header.frame_id = "/world"
        marker.header.stamp = rospy.Time.now()
        marker.ns = "arm"+str(i)
        marker.id = j
        marker.action = marker.ADD
        marker.type = marker.CUBE
        marker.scale.x = 0.1
        marker.scale.y = self.link[i]
        marker.scale.z = 0.1
        marker.color.a = 1.0
        if(self.inv_kin == 0):
            marker.color.r = 0.0
            marker.color.g = 1.0
            marker.color.b = 0.0
        else:
            marker.color.r = 1.0
            marker.color.g = 0.0
            marker.color.b = 0.0
        marker.lifetime = rospy.Duration()
        tip = self.straight_segment(i,0.5)
        
        # mat_link = np.empty((4,4), float)
        # mat_link[0,0] = mat_link[1,1] = mat_link[2,2] = mat_link[3,3] = 1.0
        # mat_link[0,1] = mat_link[0,2] = mat_link[1,0] = mat_link[1,2] = mat_link[2,0] = mat_link[2,1] = 0.0
        # mat_link[0,3] = 0.0
        # mat_link[1,3] = 0.0
        # mat_link[2,3] = self.link[i]*0.5

        mat_tip = np.array([[1., 0., 0., tip[0,0]], [0., 1., 0., tip[1,0]], [0., 0., 1., tip[2,0]], [0., 0., 0., 1.]])

        matrix_marker = np.matmul(homo_matrix, mat_tip)
        link_quat = self.RtoQuat(matrix_marker[0:3,0:3])
        marker.pose.position.x = matrix_marker[0,3]
        marker.pose.position.y = matrix_marker[1,3]
        marker.pose.position.z = matrix_marker[2,3]
        marker.pose.orientation.x = link_quat[0,0]
        marker.pose.orientation.y = link_quat[1,0]
        marker.pose.orientation.z = link_quat[2,0]
        marker.pose.orientation.w = link_quat[3,0]
        self.markerarray.markers.append(marker)

        # Update matrix
        tip = self.straight_segment(i,1.)
        mat_tip = np.array([[1., 0., 0., tip[0,0]], [0., 1., 0., tip[1,0]], [0., 0., 1., tip[2,0]], [0., 0., 0., 1.]])
        homo_matrix = np.matmul(homo_matrix, mat_tip)

        # Add goal
        marker = Marker()
        marker.header.frame_id = "/world"
        marker.header.stamp = rospy.Time.now()
        marker.ns = "goal"
        marker.id = j+1
        marker.action = marker.ADD
        marker.type = marker.SPHERE
        marker.scale.x = 0.1
        marker.scale.y = 0.1
        marker.scale.z = 0.1
        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 1.0
        marker.lifetime = rospy.Duration()
        marker.pose.position.x = self.goal[0]
        marker.pose.position.y = self.goal[1]
        marker.pose.position.z = 0.
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0
        self.markerarray.markers.append(marker)

    self.marker_pub.publish(self.markerarray)

if __name__ == '__main__':
    try:
        markers()
    except rospy.ROSInterruptException: pass
