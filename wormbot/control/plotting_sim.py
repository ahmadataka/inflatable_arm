#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 20:09:11 2019

@author: ahmadataka
"""

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
plt.style.use('default')
# plt.rcParams['pdf.fonttype'] = 42
# plt.rcParams['ps.fonttype'] = 42
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.weight"] = 'bold'
plt.rcParams['text.usetex'] = True
plt.rcParams['axes.linewidth'] = 2.0 #set the value globally

# Get the data from CSV
file_name = '~/catkin_ws/src/inflatable_arm/wormbot/control/tracking1.csv'
#file_name = '~/wormbot_exp_2/bending_1.csv'
df_tracking = pd.read_csv(file_name)
selected_data_list = ['field.now','field.c_space.data0','field.c_space.data1','field.c_space.data2','field.l_space.data0','field.l_space.data1','field.l_space.data2','field.l_space.data3','field.l_space.data4','field.l_space.data5','field.l_space.data6','field.goal.position.x','field.goal.position.y','field.goal.position.z','field.goal.orientation.x','field.goal.orientation.y', 'field.goal.orientation.z', 'field.goal.orientation.w','field.tip.poses2.position.x','field.tip.poses2.position.y','field.tip.poses2.position.z','field.tip.poses2.orientation.x','field.tip.poses2.orientation.y','field.tip.poses2.orientation.z', 'field.tip.poses2.orientation.w',]

# Get all variables
data_full = np.array(df_tracking[selected_data_list])
starting_index = 0
time = data_full[starting_index:len(data_full),0]
bending = data_full[starting_index:len(data_full),1:4]
pressure = data_full[starting_index:len(data_full),4:11]
goal = data_full[starting_index:len(data_full),11:18]
tip = data_full[starting_index:len(data_full),18:25]


time_real = np.zeros((len(time),1))
for i in range(0,len(time)):
    time_real[i] = (time[i]-time[0])*(10**(-9))

for i in range(0,len(tip)):
    if(tip[i+1,0]-tip[i,0]!=0):
        movement_start = (i)
        break

def quaternion_to_euler(x, y, z, w):

        import math
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        X = math.degrees(math.atan2(t0, t1))

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        Y = math.degrees(math.asin(t2))

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        Z = math.degrees(math.atan2(t3, t4))

        return X, Y, Z

tip_angle = np.zeros((len(tip),1))
goal_angle = np.zeros((len(goal),1))
for i in range(0,len(tip)):
    angle = quaternion_to_euler(tip[i,3],tip[i,4],tip[i,5],tip[i,6])
    tip_angle[i] = angle[2]
    angle = quaternion_to_euler(goal[i,3],goal[i,4],goal[i,5],goal[i,6])
    goal_angle[i] = angle[2]
    
fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,goal[:,0], 'g', label='x goal',linewidth=2.0)
ax.plot(time_real,goal[:,1], 'r', label='y goal',linewidth=2.0)
ax.plot(time_real,tip[:,0], '--g', label='x tip',linewidth=2.0)
ax.plot(time_real,tip[:,1], '--r', label='y tip',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Position (m)',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-0.5, 2.0, step=0.5), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-0.5,1.5])
plt.legend(loc='upper right', fontsize=16)

plt.show()
fig.savefig('./wormbot/control/tip_tracking1.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,goal_angle, 'g', label=r'$\theta$ goal',linewidth=2.0)
ax.plot(time_real,tip_angle, '--g', label=r'$\theta$ tip',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Angle ($^\circ$)',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-15, 10, step=5), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-15,5])
plt.legend(loc='upper right', fontsize=16)

plt.show()
fig.savefig('./wormbot/control/tip_angle_tracking1.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/tip_angle_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,pressure[:,0], 'r', label=r'$p_0$',linewidth=2.0)
ax.plot(time_real,pressure[:,2], 'g', label=r'$p_1$',linewidth=2.0)
ax.plot(time_real,pressure[:,3], 'b', label=r'$p_2$',linewidth=2.0)
ax.plot(time_real,pressure[:,6], 'y', label=r'$p_3$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Pressure (bar)',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(0.0, 3.0, step=0.5), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([0,2.5])
plt.legend(loc='upper left', fontsize=16)

plt.show()
fig.savefig('./wormbot/control/pressure_tracking1.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/LS_pressure.jpeg', format='jpeg', dpi=75, bbox_inches='tight')