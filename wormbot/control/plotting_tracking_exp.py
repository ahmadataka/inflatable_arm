#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 18:18:36 2019

@author: arqlab
"""

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd

plt.style.use('default')
# plt.rcParams['pdf.fonttype'] = 42
# plt.rcParams['ps.fonttype'] = 42
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.weight"] = 'bold'
plt.rcParams['text.usetex'] = True
plt.rcParams['axes.linewidth'] = 2.0 #set the value globally

# Get the data from CSV
file_name = '~/catkin_ws/src/inflatable_arm/wormbot/control/tip_tracking_exp_kinect.csv'
#file_name = '~/wormbot_exp_2/bending_1.csv'
df_tracking = pd.read_csv(file_name)
selected_data_list = ['field.now','field.c_space.data0','field.tip.poses0.position.x','field.tip.poses0.position.y','field.tip.poses1.position.x','field.tip.poses1.position.y','field.tip.poses2.position.x','field.tip.poses2.position.y']

starting_index = 0
pix_to_m = 94/30000
end_time_1 = 10279
# Get all variables
data_full = np.array(df_tracking[selected_data_list])
time_kinect = data_full[starting_index:len(data_full),0:1]
bending_kinect = data_full[starting_index:len(data_full),1:2]
tip_kinect = data_full[starting_index:len(data_full),2:8]*pix_to_m

# Get the data from CSV
file_name = '~/catkin_ws/src/inflatable_arm/wormbot/control/tip_tracking_exp.csv'
#file_name = '~/wormbot_exp_2/bending_1.csv'
df_tracking_exp = pd.read_csv(file_name)
selected_data_list = ['field.now','field.c_space.data0','field.c_space.data1','field.l_space.data0','field.l_space.data1','field.l_space.data2','field.l_space.data3','field.l_space.data4','field.goal.position.x','field.goal.position.y','field.tip.poses1.position.x','field.tip.poses1.position.y','field.tip.poses0.position.x','field.tip.poses0.position.y']

# Get all variables
data_full_exp = np.array(df_tracking_exp[selected_data_list])
time_exp = data_full_exp[starting_index:len(data_full_exp),0:1]

bending_exp  = data_full_exp[starting_index:len(data_full_exp),1:3]
pressure_exp  = data_full_exp[starting_index:len(data_full_exp),3:8]
goal_exp  = data_full_exp[starting_index:len(data_full_exp),8:10]
tip_exp  = data_full_exp[starting_index:len(data_full_exp),10:14]

time_kinect_real = np.zeros((len(time_kinect),1))
for i in range(0,len(time_kinect)):
    time_kinect_real[i] = (time_kinect[i]-time_kinect[0])*(10**(-9))
time_exp_real = np.zeros((len(time_exp),1))
for i in range(0,len(time_exp)):
    time_exp_real[i] = (time_exp[i]-time_kinect[0])*(10**(-9))

p_now = np.empty((pressure_exp.shape[0],int((pressure_exp.shape[1]-1)/2+1)), float)
for ind in range(0, len(pressure_exp)):
    p_now[ind,0] = pressure_exp[ind,0]
    for i in range(0,int((pressure_exp.shape[1]-1)/2)):
        if(pressure_exp[ind,1+2*i]<=0.001 and pressure_exp[ind, 2+2*i]>0.001):
            p_now[ind,i+1] = -1.*pressure_exp[ind,2+2*i]
        else:
            p_now[ind,i+1] = pressure_exp[ind,1+2*i]

#
#for i in range(0,len(tip)):
#    if(tip[i+1,0]-tip[i,0]!=0):
#        movement_start = (i)
#        break
#
#def quaternion_to_euler(x, y, z, w):
#
#        import math
#        t0 = +2.0 * (w * x + y * z)
#        t1 = +1.0 - 2.0 * (x * x + y * y)
#        X = math.degrees(math.atan2(t0, t1))
#
#        t2 = +2.0 * (w * y - z * x)
#        t2 = +1.0 if t2 > +1.0 else t2
#        t2 = -1.0 if t2 < -1.0 else t2
#        Y = math.degrees(math.asin(t2))
#
#        t3 = +2.0 * (w * z + x * y)
#        t4 = +1.0 - 2.0 * (y * y + z * z)
#        Z = math.degrees(math.atan2(t3, t4))
#
#        return X, Y, Z
#
#tip_angle = np.zeros((len(tip),1))
#goal_angle = np.zeros((len(goal),1))
#for i in range(0,len(tip)):
#    angle = quaternion_to_euler(tip[i,3],tip[i,4],tip[i,5],tip[i,6])
#    tip_angle[i] = angle[2]
#    angle = quaternion_to_euler(goal[i,3],goal[i,4],goal[i,5],goal[i,6])
#    goal_angle[i] = angle[2]
#    
fig, ax = plt.subplots()
#plt.axvline(x=time_kinect_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_exp_real[0:end_time_1],goal_exp[0:end_time_1,0], 'g', label='x target',linewidth=2.0)
ax.plot(time_exp_real[0:end_time_1],goal_exp[0:end_time_1,1], 'r', label='y target',linewidth=2.0)
ax.plot(time_kinect_real[0:end_time_1],tip_kinect[0:end_time_1,0], '--g', label='x tip',linewidth=2.0)
ax.plot(time_kinect_real[0:end_time_1],(-1)*tip_kinect[0:end_time_1,1], '--r', label='y tip',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Position (m)',fontsize=24)
plt.xticks(np.arange(0, 201, step=40), fontsize=18)
plt.yticks(np.arange(-0.5, 2, step=0.5), fontsize=18)
ax.set_xlim([0,200])
ax.set_ylim([-0.5,1.5])
plt.legend(loc='upper right', ncol=2, fontsize=16)

plt.show()

fig.savefig('./wormbot/control/tip_tracking_exp1.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/tip_tracking_exp1.png', format='png', dpi=300, bbox_inches='tight')

fig, ax = plt.subplots()
plt.scatter(tip_kinect[0,0],(-1)*tip_kinect[0,1], color='b', label='start',linewidth=2.0)
plt.scatter(0.,0., color='k', label='base',linewidth=2.0)
ax.plot(goal_exp[0:end_time_1,0],goal_exp[0:end_time_1,1], 'g', label='target',linewidth=1.0)
ax.plot(tip_kinect[0:end_time_1,0],(-1)*tip_kinect[0:end_time_1,1], '--r', label='tip',linewidth=1.0)
plt.xlabel('x (m)',fontsize=24)
plt.ylabel('y (m)',fontsize=24)
plt.xticks(np.arange(-1, 1, step=0.4), fontsize=18)
plt.yticks(np.arange(0.0, 1.21, step=0.4), fontsize=18)
plt.axis('equal')
ax.set_xlim([-0.5,0.5])
ax.set_ylim([-0.1,1.1])
plt.legend(loc='lower right', fontsize=16)

plt.show()

fig.savefig('./wormbot/control/tip_trajectory_exp1.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/tip_trajectory_exp1.png', format='png', dpi=300, bbox_inches='tight')
#
#fig, ax = plt.subplots()
#plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
#ax.plot(time_real,goal_angle, 'g', label=r'$\theta$ goal',linewidth=2.0)
#ax.plot(time_real,tip_angle, '--g', label=r'$\theta$ tip',linewidth=2.0)
#plt.xlabel('Time (s)',fontsize=24)
#plt.ylabel('Angle ($^\circ$)',fontsize=24)
#plt.xticks(fontsize=18)
#plt.yticks(fontsize=18)
#ax.set_xlim([0,100])
#ax.set_ylim([-15,5])
#plt.legend(loc='upper right', fontsize=16)
#
#plt.show()
#fig.savefig('tip_angle_tracking1.pdf', bbox_inches='tight')
#
fig, ax = plt.subplots()
#plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_exp_real[0:end_time_1],p_now[0:end_time_1,0], 'r', label=r'$p_0$',linewidth=2.0)
ax.plot(time_exp_real[0:end_time_1],p_now[0:end_time_1,1], 'g', label=r'$p_1$',linewidth=2.0)
ax.plot(time_exp_real[0:end_time_1],p_now[0:end_time_1,2], 'b', label=r'$p_2$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Pressure (bar)',fontsize=24)
plt.xticks(np.arange(0, 201, step=40), fontsize=18)
plt.yticks(np.arange(-1.2, 3.61, step=0.6), fontsize=18)
ax.set_xlim([0,200])
ax.set_ylim([-1.2,3.6])
plt.legend(loc='upper left', fontsize=16, ncol=3)

plt.show()
fig.savefig('./wormbot/control/pressure_tracking_exp1.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/pressure_tracking_exp1.png', format='png', dpi=300, bbox_inches='tight')


####### With object ###############################################

fig, ax = plt.subplots()
plt.axvline(x=40, color='k', linestyle='--',linewidth=1.)
plt.axvline(x=70, color='k', linestyle='--',linewidth=1.)
plt.axvline(x=100, color='b', linestyle='--',linewidth=1.)
plt.axvline(x=130, color='b', linestyle='--',linewidth=1.)
plt.axvline(x=160, color='y', linestyle='--',linewidth=1.)
plt.axvline(x=185, color='y', linestyle='--',linewidth=1.)
ax.plot(time_exp_real[end_time_1+100:len(time_exp_real)]-time_kinect_real[end_time_1+100],goal_exp[end_time_1+100:len(time_exp_real),0], 'g', label='x target',linewidth=2.0)
ax.plot(time_exp_real[end_time_1+100:len(time_exp_real)]-time_kinect_real[end_time_1+100],goal_exp[end_time_1+100:len(time_exp_real),1], 'r', label='y target',linewidth=2.0)
ax.plot(time_kinect_real[end_time_1+100:len(time_kinect_real)]-time_kinect_real[end_time_1+100],tip_kinect[end_time_1+100:len(time_kinect_real),0], '--g', label='x tip',linewidth=2.0)
ax.plot(time_kinect_real[end_time_1+100:len(time_kinect_real)]-time_kinect_real[end_time_1+100],(-1)*tip_kinect[end_time_1+100:len(time_kinect_real),1], '--r', label='y tip',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Position (m)',fontsize=24)
plt.xticks(np.arange(0, 201, step=40), fontsize=18)
plt.yticks(np.arange(-0.5, 2, step=0.5), fontsize=18)
ax.set_xlim([0,200])
ax.set_ylim([-0.5,1.5])
plt.legend(loc='upper right', fontsize=16, ncol=2)

plt.show()

fig.savefig('./wormbot/control/tip_tracking_exp1_object.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/tip_tracking_exp1_object.png', format='png', dpi=300, bbox_inches='tight')

fig, ax = plt.subplots()
#plt.scatter(tip_kinect[0,0],(-1)*tip_kinect[0,1], color='b', label='start',linewidth=2.0)
plt.scatter(0.,0., color='k', label='base',linewidth=2.0)
ax.plot(goal_exp[end_time_1+100:len(time_exp_real),0],goal_exp[end_time_1+100:len(time_exp_real),1], 'g', label='target',linewidth=1.0)
ax.plot(tip_kinect[end_time_1+100:len(time_kinect_real),0],(-1)*tip_kinect[end_time_1+100:len(time_kinect_real),1], '--r', label='tip',linewidth=1.0)
plt.xlabel('x (m)',fontsize=24)
plt.ylabel('y (m)',fontsize=24)
plt.xticks(np.arange(-0.8, 0.9, step=0.4), fontsize=18)
plt.yticks(np.arange(0.0, 1.21, step=0.4), fontsize=18)
plt.axis('equal')
ax.set_xlim([-0.5,0.5])
ax.set_ylim([-0.1,1.1])
plt.legend(loc='lower right', fontsize=16)

plt.show()

fig.savefig('./wormbot/control/tip_trajectory_exp1_object.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/tip_trajectory_exp1_object.png', format='png', dpi=300, bbox_inches='tight')
#
#fig, ax = plt.subplots()
#plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
#ax.plot(time_real,goal_angle, 'g', label=r'$\theta$ goal',linewidth=2.0)
#ax.plot(time_real,tip_angle, '--g', label=r'$\theta$ tip',linewidth=2.0)
#plt.xlabel('Time (s)',fontsize=24)
#plt.ylabel('Angle ($^\circ$)',fontsize=24)
#plt.xticks(fontsize=18)
#plt.yticks(fontsize=18)
#ax.set_xlim([0,100])
#ax.set_ylim([-15,5])
#plt.legend(loc='upper right', fontsize=16)
#
#plt.show()
#fig.savefig('tip_angle_tracking1.pdf', bbox_inches='tight')
#
fig, ax = plt.subplots()
#plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_exp_real[end_time_1+100:len(time_exp_real)]-time_kinect_real[end_time_1+100],p_now[end_time_1+100:len(time_exp_real),0], 'r', label=r'$p_0$',linewidth=2.0)
ax.plot(time_exp_real[end_time_1+100:len(time_exp_real)]-time_kinect_real[end_time_1+100],p_now[end_time_1+100:len(time_exp_real),1], 'g', label=r'$p_1$',linewidth=2.0)
ax.plot(time_exp_real[end_time_1+100:len(time_exp_real)]-time_kinect_real[end_time_1+100],p_now[end_time_1+100:len(time_exp_real),2], 'b', label=r'$p_2$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Pressure (bar)',fontsize=24)
plt.xticks(np.arange(0, 201, step=40), fontsize=18)
plt.yticks(np.arange(-1.5, 3.6, step=0.5), fontsize=18)
ax.set_xlim([0,200])
ax.set_ylim([-1.5,3.5])
plt.legend(loc='upper left', fontsize=16, ncol=3)

plt.show()
fig.savefig('./wormbot/control/pressure_tracking_exp1_object.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/pressure_tracking_exp1_object.png', format='png', dpi=300, bbox_inches='tight')

################### Shape comparison

bending_kinect_init = bending_kinect[0:end_time_1]
tip_kinect_init = tip_kinect[0:end_time_1,:]
bending_exp_init  = bending_exp[0:end_time_1,:]
pressure_exp_init  = pressure_exp[0:end_time_1,:]
goal_exp_init  = goal_exp[0:end_time_1,:]
tip_exp_init  = tip_exp[0:end_time_1,:]


training_list = np.random.rand(len(bending_kinect_init))<0.6

bending_kinect_test = bending_kinect_init[~training_list]
bending_exp_test = bending_exp_init[~training_list]

################## Bending ###################################

fig, ax = plt.subplots()
#plt.axvline(x=time_kinect_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_exp_real,bending_exp[:,0], 'g', label=r'$\theta$ target',linewidth=2.0)
ax.plot(time_kinect_real,bending_kinect[:,0], '--r', label=r'$\theta$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Bending ($^\circ$)',fontsize=24)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
ax.set_xlim([0,200])
#ax.set_ylim([-0.5,1.5])
plt.legend(loc='upper right', fontsize=16)

plt.show()
fig.savefig('./wormbot/control/bending_tracking_exp1.pdf', bbox_inches='tight')
fig.savefig('./wormbot/control/bending_tracking_exp1.png', format='png', dpi=300, bbox_inches='tight')
#
################# Pose #########################################
#
