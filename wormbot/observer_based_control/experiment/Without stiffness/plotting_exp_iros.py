#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 20:09:11 2019

@author: ahmadataka
"""

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('default')
# plt.rcParams['pdf.fonttype'] = 42
# plt.rcParams['ps.fonttype'] = 42
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams['text.usetex'] = True
laptop = 'ahmadataka'
# laptop = 'arqlab'
# Get the data from CSV
file_name = '/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/experiment/With Stiffness/new/record_wormbot.csv'
df_tracking = pd.read_csv(file_name)
selected_data_list = ['field.now',
                        'field.pressure.data0','field.pressure.data1','field.pressure.data2','field.pressure.data3','field.pressure.data4','field.pressure.data5','field.pressure.data6',
                        'field.bending.data0','field.bending.data1','field.bending.data2',
                        'field.state_est.data0','field.state_est.data1','field.state_est.data2','field.state_est.data3','field.state_est.data4','field.state_est.data5',
                        'field.state.data0','field.state.data1','field.state.data2','field.state.data3','field.state.data4','field.state.data5',
                        'field.output.data0','field.output.data1','field.output.data2',
                        'field.error.data0','field.error.data1','field.error.data2','field.error.data3','field.error.data4','field.error.data5',
                        'field.goal.position.x','field.goal.position.y','field.goal.position.z','field.goal.orientation.x','field.goal.orientation.y', 'field.goal.orientation.z', 'field.goal.orientation.w',
                        'field.tip.poses2.position.x','field.tip.poses2.position.y','field.tip.poses2.position.z','field.tip.poses2.orientation.x','field.tip.poses2.orientation.y','field.tip.poses2.orientation.z', 'field.tip.poses2.orientation.w',]

# Get all variables
data_full = np.array(df_tracking[selected_data_list])
starting_index = 1150
time = data_full[starting_index:len(data_full),0]
pressure = data_full[starting_index:len(data_full),1:8]
bending = data_full[starting_index:len(data_full),8:11]
state_est = data_full[starting_index:len(data_full),11:17]
state = data_full[starting_index:len(data_full),17:23]
output = data_full[starting_index:len(data_full),23:26]
error = data_full[starting_index:len(data_full),26:32]
goal = data_full[starting_index:len(data_full),32:39]
tip = data_full[starting_index:len(data_full),39:46]


time_real = np.zeros((len(time),1))
for i in range(0,len(time)):
    time_real[i] = (time[i]-time[0])*(10**(-9))

# for i in range(0,len(tip)):
#     if(tip[i+1,0]-tip[i,0]>0.1):
#         movement_start = (i)
#         break
movement_start = 0

pressure_map = np.zeros((len(time),int((len(pressure[0,:])-1)/2+1)), float)
for j in range(0, pressure.shape[0]):
    for i in range(0,int((len(pressure[0,:])-1)/2+1)):
        if(i==0):
            pressure_map[j,i] = pressure[j,i]
        else:
            if(pressure[j,2*i-1]<=0.001 and pressure[j,2*i]>0.001):
                pressure_map[j,i] = -1.*pressure[j,2*i]
            else:
                pressure_map[j,i] = pressure[j,2*i-1]

def quaternion_to_euler(x, y, z, w):

        import math
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        X = math.degrees(math.atan2(t0, t1))

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        Y = math.degrees(math.asin(t2))

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        Z = math.degrees(math.atan2(t3, t4))

        return X, Y, Z

tip_angle = np.zeros((len(tip),1))
goal_angle = np.zeros((len(goal),1))
for i in range(0,len(tip)):
    angle = quaternion_to_euler(tip[i,3],tip[i,4],tip[i,5],tip[i,6])
    tip_angle[i] = angle[2]
    angle = quaternion_to_euler(goal[i,3],goal[i,4],goal[i,5],goal[i,6])
    goal_angle[i] = angle[2]

jacobi_error = np.zeros((len(time),1))
for i in range(0, len(time)):
    jacobi_error[i,0] = ((error[i,:]**2).mean())**0.5

b_constant = np.zeros((len(tip),3))
b_est = np.zeros((len(tip),3))
for i in range(0, len(tip)):
    for j in range(0,3):
        b_constant[i, j] = state[i,2*j+0]/(1+state[i,2*j+1]*pressure_map[i,0])
        b_est[i, j] = state_est[i,2*j+0]/(1+state_est[i,2*j+1]*pressure_map[i,0])

for i in range(0, len(tip)):
    if(goal[i,0]==0):
        goal[i,0] = save_x
    else:
        save_x = goal[i,0]
    if(goal[i,1]==0):
        goal[i,1] = save_y
    else:
        save_y = goal[i,1]

goal_median_x = np.zeros((1, 5))
output_1 = np.zeros((1, 5))
output_2 = np.zeros((1, 5))
output_3 = np.zeros((1, 5))
pressure_med_1 = np.zeros((1, 5))
pressure_med_2 = np.zeros((1, 5))
pressure_med_3 = np.zeros((1, 5))
error_med = np.zeros((1, 5))
b_med_1 = np.zeros((1, 5))
b_med_2 = np.zeros((1, 5))
b_med_3 = np.zeros((1, 5))
for i in range(0, len(tip)):
    if(i<5):
        goal_median_x[0,i] = goal[i,0]
        output_1[0,i] = output[i,0]
        output_2[0,i] = output[i,1]
        output_3[0,i] = output[i,2]
        pressure_med_1[0,i] = pressure_map[i,0]
        pressure_med_2[0,i] = pressure_map[i,1]
        pressure_med_3[0,i] = pressure_map[i,2]
        error_med[0,i] = jacobi_error[i,0]
        b_med_1[0,i] = b_est[i,0]
        b_med_2[0,i] = b_est[i,1]
        b_med_3[0,i] = b_est[i,2]
    else:
        goal_median_x[0,i % 5] = goal[i,0]
        goal[i,0] = np.median(goal_median_x[0,:])
        output_1[0,i % 5] = output[i,0]
        output[i,0] = np.median(output_1[0,:])
        output_2[0,i % 5] = output[i,1]
        output[i,1] = np.median(output_2[0,:])
        output_3[0,i % 5] = output[i,2]
        output[i,2] = np.median(output_3[0,:])
        pressure_med_1[0,i % 5] = pressure_map[i,0]
        pressure_map[i,0] = np.median(pressure_med_1[0,:])
        pressure_med_2[0,i % 5] = pressure_map[i,1]
        pressure_map[i,1] = np.median(pressure_med_2[0,:])
        pressure_med_3[0,i % 5] = pressure_map[i,2]
        pressure_map[i,2] = np.median(pressure_med_3[0,:])
        error_med[0,i % 5] = jacobi_error[i,0]
        jacobi_error[i,0] = np.median(error_med[0,:])
        b_med_1[0,i % 5] = b_est[i,0]
        b_est[i,0] = np.median(b_med_1[0,:])
        b_med_2[0,i % 5] = b_est[i,1]
        b_est[i,1] = np.median(b_med_2[0,:])
        b_med_3[0,i % 5] = b_est[i,2]
        b_est[i,2] = np.median(b_med_3[0,:])
        
        
file_name = '/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/experiment/With Stiffness/new/kinect_to_data.csv'
df_tracking = pd.read_csv(file_name, sep=',')
selected_data_list = ['field.now',
                        'field.tip.poses0.position.x','field.tip.poses0.position.y','field.tip.poses0.position.z','field.tip.poses0.orientation.x','field.tip.poses0.orientation.y','field.tip.poses0.orientation.z', 'field.tip.poses0.orientation.w',]
data_full = np.array(df_tracking[selected_data_list])
time_kinect = data_full[0:len(data_full),0]
pose_kinect = data_full[0:len(data_full),1:8]
time_kinect_real = np.zeros((len(time_kinect),1))
pose_kinect_median_x = np.zeros((1, 5))
pose_kinect_median_y = np.zeros((1, 5))
for i in range(0,len(time_kinect)):
    time_kinect_real[i] = (time_kinect[i]-time[0])*(10**(-9))
    if(i<5):
        pose_kinect_median_x[0,i] = pose_kinect[i,0]
        pose_kinect_median_y[0,i] = pose_kinect[i,1]
    else:
        pose_kinect_median_x[0,i % 5] = pose_kinect[i,0]
        pose_kinect[i,0] = np.median(pose_kinect_median_x[0,:])
        pose_kinect_median_y[0,i % 5] = pose_kinect[i,1]
        pose_kinect[i,1] = np.median(pose_kinect_median_y[0,:])


time_step = [2, 11, 22, 40.5, 51, 63, 81.5]
# fig, ax = plt.subplots()
# plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[1], color='r', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[2], color='g', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[3], color='b', linestyle='--',linewidth=1.)
# ax.plot(time_real,state[:,0], 'g', label='$c_{11}$',linewidth=2.0)
# ax.plot(time_real,state[:,1], 'r', label='$c_{21}$',linewidth=2.0)
# ax.plot(time_real,state_est[:,0], '--g', label='$\hat{c}_{11}$',linewidth=2.0)
# ax.plot(time_real,state_est[:,1], '--r', label='$\hat{c}_{21}$',linewidth=2.0)
# plt.xlabel('Time (s)',fontsize=24)
# plt.ylabel('States of Segment 1',fontsize=24)
# # plt.xticks(np.arange(0, 81, step=20), fontsize=18)
# # plt.yticks(np.arange(0, 81, step=20), fontsize=18)
# # ax.set_xlim([0,80])
# # ax.set_ylim([0,80])
# plt.legend(loc='center upper',bbox_to_anchor=(1.0, 1.2),fontsize=16,ncol=4)
# # fig.legend(handles, labels, loc='center upper',bbox_to_anchor=(1.0, 1.27),fontsize=32, facecolor='white', ncol=3)
# # axins = plt.axes([.45, .2, .4, .25]) 
# # axins.plot(time_real,state[:,0], 'g', linewidth=2.0)
# # axins.plot(time_real,state[:,1], 'r', linewidth=2.0)
# # axins.plot(time_real,state_est[:,0], '--g', linewidth=2.0)
# # axins.plot(time_real,state_est[:,1], '--r', linewidth=2.0)
# # axins.set_xlim(10, 50) # apply the x-limits
# # axins.set_ylim(-20, 80) # apply the y-limits
# # plt.yticks(np.arange(-20, 81, step=20))
# # mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
# plt.show()
# # fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/new_simulation/angle_state_est1.pdf', bbox_inches='tight')

# fig, ax = plt.subplots()
# plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[1], color='r', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[2], color='g', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[3], color='b', linestyle='--',linewidth=1.)
# ax.plot(time_real,state[:,2], 'g', label='$c_{12}$',linewidth=2.0)
# ax.plot(time_real,state[:,3], 'r', label='$c_{22}$',linewidth=2.0)
# ax.plot(time_real,state_est[:,2], '--g', label='$\hat{c}_{12}$',linewidth=2.0)
# ax.plot(time_real,state_est[:,3], '--r', label='$\hat{c}_{22}$',linewidth=2.0)
# plt.xlabel('Time (s)',fontsize=24)
# plt.ylabel('States of Segment 2',fontsize=24)
# # plt.xticks(np.arange(0, 81, step=20), fontsize=18)
# # plt.yticks(np.arange(0, 81, step=20), fontsize=18)
# # ax.set_xlim([0,80])
# # ax.set_ylim([0,80])
# plt.legend(loc='center upper',bbox_to_anchor=(1.0, 1.2),fontsize=16,ncol=4)
# # axins = plt.axes([.45, .2, .4, .25]) 
# # axins.plot(time_real,state[:,2], 'g', linewidth=2.0)
# # axins.plot(time_real,state[:,3], 'r', linewidth=2.0)
# # axins.plot(time_real,state_est[:,2], '--g', linewidth=2.0)
# # axins.plot(time_real,state_est[:,3], '--r', linewidth=2.0)
# # axins.set_xlim(10, 50) # apply the x-limits
# # axins.set_ylim(-20, 80) # apply the y-limits
# # plt.yticks(np.arange(-20, 81, step=20))
# # mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
# plt.show()
# # fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/new_simulation/angle_state_est2.pdf', bbox_inches='tight')
# # fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# # fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

# fig, ax = plt.subplots()
# plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[1], color='r', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[2], color='g', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[3], color='b', linestyle='--',linewidth=1.)
# ax.plot(time_real,state[:,4], 'r', label='$c_{13}$',linewidth=2.0)
# ax.plot(time_real,state[:,5], 'g', label='$c_{23}$',linewidth=2.0)
# ax.plot(time_real,state_est[:,4], '--r', label='$\hat{c}_{13}$',linewidth=2.0)
# ax.plot(time_real,state_est[:,5], '--g', label='$\hat{c}_{23}$',linewidth=2.0)
# plt.xlabel('Time (s)',fontsize=24)
# plt.ylabel('States of Segment 3',fontsize=24)
# # plt.xticks(np.arange(0, 81, step=20), fontsize=18)
# # plt.yticks(np.arange(0, 81, step=20), fontsize=18)
# # ax.set_xlim([0,80])
# # ax.set_ylim([0,80])
# plt.legend(loc='center upper',bbox_to_anchor=(1.0, 1.2),fontsize=16,ncol=4)
# # axins = plt.axes([.45, .2, .4, .25]) 
# # axins.plot(time_real,state[:,4], 'g', linewidth=2.0)
# # axins.plot(time_real,state[:,5], 'r', linewidth=2.0)
# # axins.plot(time_real,state_est[:,4], '--g', linewidth=2.0)
# # axins.plot(time_real,state_est[:,5], '--r', linewidth=2.0)
# # axins.set_xlim(10, 50) # apply the x-limits
# # axins.set_ylim(-20, 80) # apply the y-limits
# # plt.yticks(np.arange(-20, 81, step=20))
# # mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

# plt.show()
# # fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/new_simulation/angle_state_est3.pdf', bbox_inches='tight')
# # fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# # fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[1], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[2], color='r', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[3], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[4], color='r', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[5], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[6], color='r', linestyle='--',linewidth=1.)
ax.plot(time_real,bending[:,0], 'g', label=r'$\theta_1$',linewidth=2.0)
ax.plot(time_real,output[:,0], '--g', label=r'$\hat{\theta}_1$',linewidth=2.0)
ax.plot(time_real,bending[:,1], 'r', label=r'$\theta_2$',linewidth=2.0)
ax.plot(time_real,output[:,1], '--r', label=r'$\hat{\theta}_2$',linewidth=2.0)
ax.plot(time_real,bending[:,2], 'b', label=r'$\theta_3$',linewidth=2.0)
ax.plot(time_real,output[:,2], '--b', label=r'$\hat{\theta}_3$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Bending Angle ($^\circ$)',fontsize=24)
plt.xticks(np.arange(0, 81, step=20), fontsize=18)
plt.yticks(np.arange(-80, 81, step=40), fontsize=18)
ax.set_xlim([0,83])
ax.set_ylim([-80,80])
plt.legend(loc='center upper',bbox_to_anchor=(0.85, 1.25),fontsize=16,ncol=3)
# axins = plt.axes([.45, .45, .4, .2]) 
# axins.plot(time_real,bending[:,0], 'g', linewidth=2.0)
# axins.plot(time_real,bending[:,1], 'r', linewidth=2.0)
# axins.plot(time_real,bending[:,2], 'b', linewidth=2.0)
# axins.plot(time_real,output[:,0], '--g', linewidth=2.0)
# axins.plot(time_real,output[:,1], '--r', linewidth=2.0)
# axins.plot(time_real,output[:,2], '--b', linewidth=2.0)
# axins.set_xlim(0, 30) # apply the x-limits
# axins.set_ylim(-50, 50) # apply the y-limits
# plt.yticks(np.arange(-50, 51, step=50))
# mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

plt.show()
fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/experiment/With Stiffness/new/angle_output_est.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[1], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[2], color='r', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[3], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[4], color='r', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[5], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[6], color='r', linestyle='--',linewidth=1.)
ax.plot(time_real,goal[:,0], 'g', label='x goal',linewidth=2.0)
ax.plot(time_real,goal[:,1], 'r', label='y goal',linewidth=2.0)
ax.plot(time_kinect_real,pose_kinect[:,0], '--g', label='x tip',linewidth=2.0)
ax.plot(time_kinect_real,pose_kinect[:,1], '--r', label='y tip',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Position (pixel)',fontsize=24)
plt.xticks(np.arange(0, 81, step=20), fontsize=18)
plt.yticks(np.arange(-500, 101, step=100), fontsize=18)
ax.set_xlim([0,83])
ax.set_ylim([-500,100])
plt.legend(loc='center upper',bbox_to_anchor=(0.8, 1.25),fontsize=16,ncol=2)

plt.show()
fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/experiment/With Stiffness/new/angle_tip_tracking1.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.scatter(pose_kinect[0,0],pose_kinect[0,1], color='b', label='start',linewidth=2.0)
plt.scatter(0.,0., color='k', label='base',linewidth=2.0)
ax.plot(goal[:,0],goal[:,1], 'g', label='target',linewidth=1.0)
ax.plot(pose_kinect[:,0],pose_kinect[:,1], '--r', label='tip',linewidth=1.0)
plt.xlabel('x (m)',fontsize=24)
plt.ylabel('y (m)',fontsize=24)
plt.xticks(np.arange(-1, 1, step=0.4), fontsize=18)
plt.yticks(np.arange(0.0, 1.21, step=0.4), fontsize=18)
plt.axis('equal')
# ax.set_xlim([-0.5,0.5])
# ax.set_ylim([-0.1,1.1])
plt.legend(loc='lower right', fontsize=16)

plt.show()

fig, ax = plt.subplots()
plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[1], color='r', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[2], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[3], color='b', linestyle='--',linewidth=1.)
ax.plot(time_real,jacobi_error, 'b',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Error Jacobian',fontsize=24)
# plt.xticks(np.arange(0, 81, step=20), fontsize=18)
# plt.yticks(np.arange(-0.5, 0.51, step=0.5), fontsize=18)
# ax.set_xlim([0,80])
ax.set_ylim([-0.5,0.5])

# axins = plt.axes([.45, .45, .4, .2]) 
# axins.plot(time_real,jacobi_error, 'b', linewidth=2.0)
# axins.set_xlim(0, 30) # apply the x-limits
# axins.set_ylim(-50, 50) # apply the y-limits
# plt.yticks(np.arange(-50, 50.1, step=50))
# mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

plt.show()
# # fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/new_simulation/angle_jacobi_error.pdf', bbox_inches='tight')
# # fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# # fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

# fig, ax = plt.subplots()
# plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[1], color='r', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[2], color='g', linestyle='--',linewidth=1.)
# plt.axvline(x=time_step[3], color='b', linestyle='--',linewidth=1.)
# ax.plot(time_real,goal_angle, 'g', label=r'$\theta$ goal',linewidth=2.0)
# ax.plot(time_real,tip_angle, '--g', label=r'$\theta$ tip',linewidth=2.0)
# plt.xlabel('Time (s)',fontsize=24)
# plt.ylabel('Tip Orientation ($^\circ$)',fontsize=24)
# # plt.xticks(np.arange(0, 81, step=20), fontsize=18)
# # plt.yticks(np.arange(-10, 11, step=10), fontsize=18)
# # ax.set_xlim([0,80])
# # ax.set_ylim([-10,10])
# plt.legend(loc='center upper',bbox_to_anchor=(0.8, 1.2),fontsize=16,ncol=2)

# plt.show()
# # fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/new_simulation/angle_tip_angle_tracking1.pdf', bbox_inches='tight')
# # fig.savefig('./wormbot/observer_based_control/simulation/tip_angle_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# # fig.savefig('./wormbot/observer_based_control/simulation/tip_angle_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[1], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[2], color='r', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[3], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[4], color='r', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[5], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[6], color='r', linestyle='--',linewidth=1.)
ax.plot(time_real,pressure_map[:,0], 'r', label=r'$p_0$',linewidth=2.0)
ax.plot(time_real,pressure_map[:,1], 'g', label=r'$p_1$',linewidth=2.0)
ax.plot(time_real,pressure_map[:,2], 'b', label=r'$p_2$',linewidth=2.0)
ax.plot(time_real,pressure_map[:,3], 'y', label=r'$p_3$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Pressure (bar)',fontsize=24)
plt.xticks(np.arange(0, 81, step=20), fontsize=18)
plt.yticks(np.arange(-5, 11, step=5), fontsize=18)
ax.set_xlim([0,83])
ax.set_ylim([-5,10])
plt.legend(loc='center upper',bbox_to_anchor=(1.0, 1.2),fontsize=16,ncol=4)

plt.show()
fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/experiment/With Stiffness/new/angle_pressure_tracking1.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/LS_pressure.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/pressure_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_step[0], color='k', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[1], color='r', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[2], color='g', linestyle='--',linewidth=1.)
plt.axvline(x=time_step[3], color='b', linestyle='--',linewidth=1.)
ax.plot(time_real,b_constant[:,0], 'g', label='$b_{1}$',linewidth=2.0)
ax.plot(time_real,b_est[:,0], '--g', label='$\hat{b}_{1}$',linewidth=2.0)
ax.plot(time_real,b_constant[:,1], 'r', label='$b_{2}$',linewidth=2.0)
ax.plot(time_real,b_est[:,1], '--r', label='$\hat{b}_{2}$',linewidth=2.0)
ax.plot(time_real,b_constant[:,2], 'b', label='$b_{3}$',linewidth=2.0)
ax.plot(time_real,b_est[:,2], '--b', label='$\hat{b}_{3}$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Parameter',fontsize=24)
# plt.xticks(np.arange(0, 81, step=20), fontsize=18)
# plt.yticks(np.arange(0, 81, step=20), fontsize=18)
# ax.set_xlim([0,80])
# ax.set_ylim([0,80])
plt.legend(loc='center upper',bbox_to_anchor=(0.9, 1.3),fontsize=16,ncol=3)
# axins = plt.axes([.45, .2, .4, .25]) 
# axins.plot(time_real,state[:,0], 'g', linewidth=2.0)
# axins.plot(time_real,state[:,1], 'r', linewidth=2.0)
# axins.plot(time_real,state_est[:,0], '--g', linewidth=2.0)
# axins.plot(time_real,state_est[:,1], '--r', linewidth=2.0)
# axins.set_xlim(10, 50) # apply the x-limits
# axins.set_ylim(-20, 80) # apply the y-limits
# plt.yticks(np.arange(-20, 81, step=20))
# mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
plt.show()
# # fig.savefig('/home/'+laptop+'/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/new_simulation/param.pdf', bbox_inches='tight')
