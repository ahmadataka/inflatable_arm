#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 20:09:11 2019

@author: ahmadataka
"""

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('default')
# plt.rcParams['pdf.fonttype'] = 42
# plt.rcParams['ps.fonttype'] = 42
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams['text.usetex'] = True

# Get the data from CSV
file_name = '~/catkin_ws/src/inflatable_arm/wormbot/observer_based_control/simulation/record_angle3.csv'
# file_name = '~/catkin_ws/src/inflatable_arm/wormbot/control/tracking1.csv'
#file_name = '~/wormbot_exp_2/bending_1.csv'
df_tracking = pd.read_csv(file_name)
selected_data_list = ['field.now',
                        'field.pressure.data0','field.pressure.data1','field.pressure.data2','field.pressure.data3','field.pressure.data4','field.pressure.data5','field.pressure.data6',
                        'field.bending.data0','field.bending.data1','field.bending.data2',
                        'field.state_est.data0','field.state_est.data1','field.state_est.data2','field.state_est.data3','field.state_est.data4','field.state_est.data5',
                        'field.state.data0','field.state.data1','field.state.data2','field.state.data3','field.state.data4','field.state.data5',
                        'field.output.data0','field.output.data1','field.output.data2',
                        'field.error.data0','field.error.data1','field.error.data2','field.error.data3','field.error.data4','field.error.data5',
                        'field.goal.position.x','field.goal.position.y','field.goal.position.z','field.goal.orientation.x','field.goal.orientation.y', 'field.goal.orientation.z', 'field.goal.orientation.w',
                        'field.tip.poses2.position.x','field.tip.poses2.position.y','field.tip.poses2.position.z','field.tip.poses2.orientation.x','field.tip.poses2.orientation.y','field.tip.poses2.orientation.z', 'field.tip.poses2.orientation.w',]

# Get all variables
data_full = np.array(df_tracking[selected_data_list])
starting_index = 0
time = data_full[starting_index:len(data_full),0]
pressure = data_full[starting_index:len(data_full),1:8]
bending = data_full[starting_index:len(data_full),8:11]
state_est = data_full[starting_index:len(data_full),11:17]
state = data_full[starting_index:len(data_full),17:23]
output = data_full[starting_index:len(data_full),23:26]
error = data_full[starting_index:len(data_full),26:32]
goal = data_full[starting_index:len(data_full),32:39]
tip = data_full[starting_index:len(data_full),39:46]


time_real = np.zeros((len(time),1))
for i in range(0,len(time)):
    time_real[i] = (time[i]-time[0])*(10**(-9))

for i in range(0,len(tip)):
    if(tip[i+1,0]-tip[i,0]!=0):
        movement_start = (i)
        break

pressure_map = np.zeros((len(time),int((len(pressure[0,:])-1)/2+1)), float)
for j in range(0, pressure.shape[0]):
    for i in range(0,int((len(pressure[0,:])-1)/2+1)):
        if(i==0):
            pressure_map[j,i] = pressure[j,i]
        else:
            if(pressure[j,2*i-1]<=0.001 and pressure[j,2*i]>0.001):
                pressure_map[j,i] = -1.*pressure[j,2*i]
            else:
                pressure_map[j,i] = pressure[j,2*i-1]

def quaternion_to_euler(x, y, z, w):

        import math
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        X = math.degrees(math.atan2(t0, t1))

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        Y = math.degrees(math.asin(t2))

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        Z = math.degrees(math.atan2(t3, t4))

        return X, Y, Z

tip_angle = np.zeros((len(tip),1))
goal_angle = np.zeros((len(goal),1))
for i in range(0,len(tip)):
    angle = quaternion_to_euler(tip[i,3],tip[i,4],tip[i,5],tip[i,6])
    tip_angle[i] = angle[2]
    angle = quaternion_to_euler(goal[i,3],goal[i,4],goal[i,5],goal[i,6])
    goal_angle[i] = angle[2]

jacobi_error = np.zeros((len(time),1))
for i in range(0, len(time)):
    jacobi_error[i,0] = ((error[i,:]**2).mean())**0.5

fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,state[:,0], 'g', label='$c_{11}$',linewidth=2.0)
ax.plot(time_real,state[:,1], 'r', label='$c_{21}$',linewidth=2.0)
ax.plot(time_real,state_est[:,0], '--g', label='$\hat{c}_{11}$',linewidth=2.0)
ax.plot(time_real,state_est[:,1], '--r', label='$\hat{c}_{21}$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Parameter',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-2000, 501, step=500), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-2000,500])
plt.legend(loc='upper right', fontsize=16)
axins = plt.axes([.45, .2, .4, .25]) 
axins.plot(time_real,state[:,0], 'g', linewidth=2.0)
axins.plot(time_real,state[:,1], 'r', linewidth=2.0)
axins.plot(time_real,state_est[:,0], '--g', linewidth=2.0)
axins.plot(time_real,state_est[:,1], '--r', linewidth=2.0)
axins.set_xlim(10, 50) # apply the x-limits
axins.set_ylim(-20, 80) # apply the y-limits
plt.yticks(np.arange(-20, 81, step=20))
# mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
plt.show()
# fig.savefig('./wormbot/observer_based_control/simulation/angle_state_est1.pdf', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,state[:,2], 'g', label='$c_{12}$',linewidth=2.0)
ax.plot(time_real,state[:,3], 'r', label='$c_{22}$',linewidth=2.0)
ax.plot(time_real,state_est[:,2], '--g', label='$\hat{c}_{12}$',linewidth=2.0)
ax.plot(time_real,state_est[:,3], '--r', label='$\hat{c}_{22}$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Parameter',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-2500, 501, step=500), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-2500,500])
plt.legend(loc='upper right', fontsize=16)
axins = plt.axes([.45, .2, .4, .25]) 
axins.plot(time_real,state[:,2], 'g', linewidth=2.0)
axins.plot(time_real,state[:,3], 'r', linewidth=2.0)
axins.plot(time_real,state_est[:,2], '--g', linewidth=2.0)
axins.plot(time_real,state_est[:,3], '--r', linewidth=2.0)
axins.set_xlim(10, 50) # apply the x-limits
axins.set_ylim(-20, 80) # apply the y-limits
plt.yticks(np.arange(-20, 81, step=20))
# mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
plt.show()
# fig.savefig('./wormbot/observer_based_control/simulation/angle_state_est2.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,state[:,4], 'r', label='$c_{13}$',linewidth=2.0)
ax.plot(time_real,state[:,5], 'g', label='$c_{23}$',linewidth=2.0)
ax.plot(time_real,state_est[:,4], '--r', label='$\hat{c}_{13}$',linewidth=2.0)
ax.plot(time_real,state_est[:,5], '--g', label='$\hat{c}_{23}$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Parameter',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-1000, 201, step=400), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-1000,200])
plt.legend(loc='upper right', fontsize=16)
axins = plt.axes([.45, .2, .4, .25]) 
axins.plot(time_real,state[:,4], 'g', linewidth=2.0)
axins.plot(time_real,state[:,5], 'r', linewidth=2.0)
axins.plot(time_real,state_est[:,4], '--g', linewidth=2.0)
axins.plot(time_real,state_est[:,5], '--r', linewidth=2.0)
axins.set_xlim(10, 50) # apply the x-limits
axins.set_ylim(-20, 80) # apply the y-limits
plt.yticks(np.arange(-20, 81, step=20))
# mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

plt.show()
# fig.savefig('./wormbot/observer_based_control/simulation/angle_state_est3.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,bending[:,0], 'g', label=r'$\theta_1$',linewidth=2.0)
ax.plot(time_real,bending[:,1], 'r', label=r'$\theta_2$',linewidth=2.0)
ax.plot(time_real,bending[:,2], 'b', label=r'$\theta_3$',linewidth=2.0)
ax.plot(time_real,output[:,0], '--g', label=r'$\hat{\theta}_1$',linewidth=2.0)
ax.plot(time_real,output[:,1], '--r', label=r'$\hat{\theta}_2$',linewidth=2.0)
ax.plot(time_real,output[:,2], '--b', label=r'$\hat{\theta}_3$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Angle ($^\circ$)',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-100, 401, step=100), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-100,400])
plt.legend(loc='upper right', fontsize=16, ncol=3)
axins = plt.axes([.45, .45, .4, .2]) 
axins.plot(time_real,bending[:,0], 'g', linewidth=2.0)
axins.plot(time_real,bending[:,1], 'r', linewidth=2.0)
axins.plot(time_real,bending[:,2], 'b', linewidth=2.0)
axins.plot(time_real,output[:,0], '--g', linewidth=2.0)
axins.plot(time_real,output[:,1], '--r', linewidth=2.0)
axins.plot(time_real,output[:,2], '--b', linewidth=2.0)
axins.set_xlim(0, 30) # apply the x-limits
axins.set_ylim(-50, 50) # apply the y-limits
plt.yticks(np.arange(-50, 51, step=50))
# mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

plt.show()
# fig.savefig('./wormbot/observer_based_control/simulation/output_est.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,goal[:,0], 'g', label='x goal',linewidth=2.0)
ax.plot(time_real,goal[:,1], 'r', label='y goal',linewidth=2.0)
ax.plot(time_real,tip[:,0], '--g', label='x tip',linewidth=2.0)
ax.plot(time_real,tip[:,1], '--r', label='y tip',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Position (m)',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(0, 1.51, step=0.5), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([0,1.5])
plt.legend(loc='upper right', fontsize=16)

plt.show()
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,jacobi_error, 'b',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Error Jacobian',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-20000, 120001, step=20000), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-20000,120000])

axins = plt.axes([.45, .45, .4, .2]) 
axins.plot(time_real,jacobi_error, 'b', linewidth=2.0)
axins.set_xlim(0, 30) # apply the x-limits
axins.set_ylim(-50, 50) # apply the y-limits
plt.yticks(np.arange(-50, 50.1, step=50))
# mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

plt.show()
# fig.savefig('./wormbot/observer_based_control/simulation/jacobi_error.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,goal_angle, 'g', label=r'$\theta$ goal',linewidth=2.0)
ax.plot(time_real,tip_angle, '--g', label=r'$\theta$ tip',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Angle ($^\circ$)',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-15, 40.1, step=5), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-15,40])
plt.legend(loc='upper right', fontsize=16)

plt.show()
# fig.savefig('./wormbot/observer_based_control/simulation/tip_angle_tracking1.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_angle_tracking1.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/tip_angle_tracking1.eps', format='eps', bbox_inches='tight')

fig, ax = plt.subplots()
plt.axvline(x=time_real[movement_start], color='k', linestyle='--',linewidth=1.)
ax.plot(time_real,pressure_map[:,0], 'r', label=r'$p_0$',linewidth=2.0)
ax.plot(time_real,pressure_map[:,1], 'g', label=r'$p_1$',linewidth=2.0)
ax.plot(time_real,pressure_map[:,2], 'b', label=r'$p_2$',linewidth=2.0)
ax.plot(time_real,pressure_map[:,3], 'y', label=r'$p_3$',linewidth=2.0)
plt.xlabel('Time (s)',fontsize=24)
plt.ylabel('Pressure (bar)',fontsize=24)
plt.xticks(np.arange(0, 101, step=20), fontsize=18)
plt.yticks(np.arange(-2, 2.51, step=0.5), fontsize=18)
ax.set_xlim([0,100])
ax.set_ylim([-2,2.5])
plt.legend(loc='upper left', fontsize=16)

plt.show()
# fig.savefig('./wormbot/observer_based_control/simulation/pressure_tracking1.pdf', bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/LS_pressure.jpeg', format='jpeg', dpi=75, bbox_inches='tight')
# fig.savefig('./wormbot/observer_based_control/simulation/pressure_tracking1.eps', format='eps', bbox_inches='tight')